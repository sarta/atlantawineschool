<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart Software license agreement                                           |
| Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
| SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
| (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
| FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
| THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
| LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
| INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
| (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
| LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
| NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
| PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
| THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
| SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
| GRANTED BY THIS AGREEMENT.                                                  |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

/**
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Admin interface
 * @author     Ruslan R. Fazlyev <rrf@x-cart.com>
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    $Id: registrants_info.php,v 1 2016/05/05 kolbasa Exp $
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */
require './auth.php';
require $xcart_dir . '/include/security.php';

$location[] = array(func_get_langvar_by_name('lbl_registrants_info'), '');

if ($REQUEST_METHOD == 'POST' && $mode == 'make_report') {
    $data = array();
    $data = func_query("SELECT a.amount, a.extra_data, b.firstname, b.lastname, b.email FROM $sql_tbl[order_details] as a LEFT JOIN $sql_tbl[orders] as b ON (a.orderid = b.orderid) WHERE a.productcode = '$productcode' AND b.status IN ('C', 'P')");

    if (!empty($data)) {
        foreach ($data as $k => $v) {
            $data[$k]['extra_data'] = unserialize($v['extra_data']);
            $varid = func_get_variantid($data[$k]['extra_data']['product_options']);
            if (!$varid) {
                unset($data[$k]);
                continue;
            }
        }
    }

    if (!empty($data)) {
        $date = date('d-m-Y', XC_TIME);
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="registrants_' . $date . '.csv"');

        $smarty->assign('data', $data);
        func_display('main/registrants_info.tpl', $smarty);
        exit;
    } else {
        $top_message['content'] = func_get_langvar_by_name('msg_no_registrants');
        func_header_location($HTTP_REFERER);
    }
}

$smarty->assign('main', 'registrants_info');

// Assign the current location line
$smarty->assign('location', $location);

if (
    file_exists($xcart_dir . '/modules/gold_display.php')
    && is_readable($xcart_dir . '/modules/gold_display.php')
) {
    include $xcart_dir . '/modules/gold_display.php';
}
func_display('provider/home.tpl', $smarty);
?>
