<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2010 Ruslan R. Fazlyev <rrf@x-cart.com>                  |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLYEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazlyev             |
| Portions created by Ruslan R. Fazlyev are Copyright (C) 2001-2010           |
| Ruslan R. Fazlyev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: multilevel_menu.php,v 1.0 lehach Exp $
#
require "./auth.php";
require $xcart_dir."/include/security.php";

$location[] = array(func_get_langvar_by_name("lbl_multilevel_menu"), "multilevel_menu.php");

if (empty($mode)) $mode = "";
$linkid = isset($linkid) ? intval($linkid) : 0;

if ($linkid > 0) {
# Get the current link data
	$current_link = func_get_link_data($linkid);
	$smarty->assign("current_link",$current_link);
}

require $xcart_dir."/admin/multilevel_menu_location.php";

if ($REQUEST_METHOD == "POST") {

	if ($mode == "update") {
		# update links list

		if (is_array($posted_data)) {
			foreach ($posted_data as $link_id=>$v) {
				$query_data = array(
					"order_by" => $v["order_by"],
					"url" => $v["url"],
					"avail" => ($v["avail"] == "Y" ? "Y" : "N")
				);

                if (($shop_language == $config['default_admin_language'])) {
   					$query_data["link"] =  $v["link"];
                }
                func_languages_alt_insert('multilevel_menu_'.$link_id, $v["link"], $shop_language);

				func_array2update("multilevel_menu", $query_data, "linkid='$link_id'");		
			}

		}

        $top_message["content"] = func_get_langvar_by_name("msg_adm_links_updated");
        $top_message["type"] = "I";

    } elseif ($mode == "delete" && is_array($to_delete)) {
		# delete links (with sublinks)
		
		foreach ($to_delete as $link_id=>$v) 
			$parent_linkid = func_delete_link($link_id);	

		$top_message["content"] = func_get_langvar_by_name("msg_adm_links_del");
		$top_message["type"] = "I";

		func_header_location("multilevel_menu.php?linkid=$parent_linkid");

	} elseif ($mode == "add" && is_array($new_link)) {
		# add new link to store menu

		$new_linkid = func_add_new_link($new_link,$linkid);

		if (intval($new_linkid) > 0 ) {
			$top_message = array(
				"content" => func_get_langvar_by_name("msg_adm_link_added"),
				"type" => "I"
			);
		}
		
	}

	func_header_location("multilevel_menu.php?linkid=$linkid");
}

$multilevel_menu = func_get_links_list($linkid);

if (!empty($multilevel_menu)) {
    $new_order_by = 0;
    foreach ($multilevel_menu as $link_id=>$v)
        $new_order_by = max($new_order_by, $v['order_by']);
}

$smarty->assign("new_order_by",$new_order_by+10);
$smarty->assign("multilevel_menu",$multilevel_menu);
$smarty->assign("linkid",$linkid);
$smarty->assign("main","multilevel_menu");

# Assign the current location line
$smarty->assign("location", $location);

@include $xcart_dir."/modules/gold_display.php";
func_display("admin/home.tpl",$smarty);
?>