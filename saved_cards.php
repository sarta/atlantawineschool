<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart Software license agreement                                           |
| Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
| SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
| (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
| FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
| THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
| LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
| INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
| (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
| LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
| NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
| PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
| THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
| SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
| GRANTED BY THIS AGREEMENT.                                                  |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

/**
 * Customer's address book interface
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Customer interface
 * @author     Ruslan R. Fazlyev <rrf@x-cart.com>
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v14 (xcart_4_7_5), 2016-02-18 13:44:28, saved_cards.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

require __DIR__.'/auth.php';

if (empty($active_modules['XPayments_Connector'])) {
    func_header_location('home.php');
    exit;
}

func_xpay_func_load();

if (!xpc_use_recharges()) {
    func_header_location('home.php');
    exit;
}

require $xcart_dir . '/include/remember_user.php';
require $xcart_dir . '/include/security.php';

include $xcart_dir . '/include/common.php';

x_load('user');

if (
    in_array($mode, array('delete', 'set_default'))
    && !empty($id)
) {

    if ('delete' == $mode) {
        $res = xpc_delete_saved_card($logged_userid, $id);
        $msg = func_get_langvar_by_name('txt_saved_card_removed');
    } elseif ('set_default' == $mode) {
        $res = xpc_set_default_card($logged_userid, $id);
        $msg = func_get_langvar_by_name('txt_saved_card_updated');
    }

    if ($res) {
        $top_message = array(
            'type'    => 'I',
            'content' => $msg, 
        );
    }

    func_header_location('saved_cards.php');
}

$saved_cards = xpc_get_saved_cards();
$default_card_id = xpc_get_default_card();
$allow_save_cards = xpc_get_allow_save_cards();

$smarty->assign('saved_cards', $saved_cards);
$smarty->assign('allow_save_cards', $allow_save_cards);
$smarty->assign('default_card_id', $default_card_id);

$xpc_save_cc_paymentid = xpc_get_save_cc_paymentid();
$allow_add_new_card =
                $config['XPayments_Connector']['xpc_save_cc_enable'] == 'Y'
                && $xpc_save_cc_paymentid > 0
                && !func_is_address_book_empty($logged_userid);

if (
    $xpc_save_cc_paymentid
    && $allow_add_new_card
) {
    x_load('cart');
    $allow_add_new_card = false;
    $payment_methods = check_payment_methods(($user_account['membershipid']) ? $user_account['membershipid'] : 0);
    foreach ($payment_methods as $payment) {
        if ($payment['paymentid'] == $xpc_save_cc_paymentid) {
            $allow_add_new_card = true;
            break;
        }
    }
}

$smarty->assign('xpc_save_cc_paymentid', $xpc_save_cc_paymentid);
$smarty->assign('allow_add_new_card', $allow_add_new_card);

if ($allow_add_new_card) {

    x_session_register('xpc_save_card_address', array());

    if (empty($xpc_save_card_address)) {
        $addresses = func_get_address_book($logged_userid);
        foreach ($addresses as $address) {
            if ('Y' == $address['default_b']) {
                $xpc_save_card_address = $address;
                break;
            }
        }
    }

    // Additional address fields are skipped here since they're not used by X-Payments
    $smarty->assign('address', $xpc_save_card_address);

    $address_fields = func_get_default_fields('C', 'address_book', true, true);
    $smarty->assign('default_fields', $address_fields);

    x_session_register('xpc_save_card_show_form', false);
    $smarty->assign('auto_open_save_card', $xpc_save_card_show_form);
    $xpc_save_card_show_form = false;

}

$smarty->assign('main', 'saved_cards');

// Assign the current location line
$location[] = array(func_get_langvar_by_name('lbl_saved_cards'), '');
$smarty->assign('location', $location);

func_display('customer/home.tpl', $smarty);
?>
