<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart Software license agreement                                           |
| Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
| SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
| (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
| FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
| THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
| LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
| INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
| (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
| LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
| NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
| PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
| THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
| SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
| GRANTED BY THIS AGREEMENT.                                                  |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

/**
 * Pitney Bowes shipping library
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Lib
 * @author     Michael Bugrov
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v6 (xcart_4_7_5), 2016-02-18 13:44:28, mod_PB.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

if ( !defined('XCART_SESSION_START') ) { header("Location: ../"); die("Access denied"); }

x_load('order');

function func_shipper_PB($items, $userinfo, $orig_address, $debug, $cart)
{ // {{{
    global $config;
    global $allowed_shipping_methods;
    global $intershipper_rates;

    if (
        empty($config['Pitney_Bowes']['pb_api_username'])
        || empty($config['Pitney_Bowes']['pb_api_password'])
    ) {
        return;
    }

    $shipping_data = $pb_methods = $rates = array();
    foreach ($allowed_shipping_methods as $v) {
        if ($v['code'] == 'PB') {
            $pb_methods[] = $v;
        }
    }

    if (empty($pb_methods)) {
        return;
    }

    // Get package limits
    $package_limits = func_get_package_limits_PB();

    // Get packages
    $packages = func_get_packages(
        $items, $package_limits,
        (XCPitneyBowesConfig::getInstance()->useMultiplePackages() == 'Y') ? 100 : 1
    );

    if (empty($packages) || !is_array($packages)) {
        return;
    }

    $objPitneyBowes = func_pitney_bowes_get_processor($debug);

    // Billing address
    $shipping_data['billAddress'] = func_pitney_bowes_get_billing_address($userinfo);
    // Destination address info
    $shipping_data['destAddress'] = func_pitney_bowes_get_destination_address($userinfo);
    // Source address info
    $shipping_data['origAddress'] = func_pitney_bowes_get_original_address($orig_address);
    // Cart total
    $shipping_data['cartSubTotal'] = $cart['subtotal'];

    // Pitney Bowes API filter (quote / order)
    $selectedOnCheckout = func_pitney_bowes_is_selected_on_checkout($userinfo);

    if ($selectedOnCheckout) {
        $shipping_data[XCPitneyBowesDefs::CART_TRANSACTIONID] = func_pitney_bowes_get_cart_transactionid();
    }

    // Used packages
    $used_packs = array();

    foreach ($packages as $pack_num => $pack) { // {{{ foreach $packages
        $_pack = $pack;

        $pack_key = md5(serialize($_pack));

        if (isset($used_packs[$pack_key])) {
            $pb_rates[$pack_num] = $used_packs[$pack_key];
            continue;
        }

        $shipping_data['package'] = $pack;

        $parsed_rates = func_PB_find_methods($objPitneyBowes->getRates($shipping_data), $pb_methods);

        if (empty($parsed_rates)) {
            // Do not calculate all other packs from pack_set if any Pack from the pack_set cannot be calculated
            $pb_rates = array();
            break;
        }

        if ($selectedOnCheckout) {
            func_pitney_bowes_add_cart_transaction_orders($objPitneyBowes->getOrders($shipping_data));
        }

        $pb_rates[$pack_num] = $parsed_rates;
        $pb_rates[$pack_num] = func_normalize_shipping_rates($pb_rates[$pack_num], 'PB');

        $used_packs[$pack_key] = $pb_rates[$pack_num];
    } // }}} foreach $packages
 
    $_rates = func_array_merge($rates, func_intersect_rates($pb_rates));
    $rates = func_shipping_min_rates($_rates);

    $intershipper_rates = func_array_merge($intershipper_rates, $rates);
} // }}}

/**
 * Return package limits for PB package
 */
function func_get_package_limits_PB()
{ // {{{
    global $config;

    // Enable caching for func_get_package_limits_PB function
    func_register_cache_function(
        'func_get_package_limits_PB',
        array (
            'dir'   => 'shipping_PB_cache',
            'hashedDirectoryLevel' => 2,
        )
    );

    $save_result_in_cache = true;

    $oPBOptions = XCPitneyBowesConfig::getInstance();

    $md5_args = md5(serialize(array(
        $oPBOptions->getConfigAsArray(),
        $config['General']['dimensions_symbol_cm'], // From func_correct_dimensions
        $config['General']['weight_symbol_grams'], // From func_correct_dimensions
    )));

    if (($cacheData = func_get_cache_func($md5_args, 'func_get_package_limits_PB'))) {
        return $cacheData['data'];
    }

    $dim = array();
    list($dim['length'], $dim['width'], $dim['height'], $dim['girth']) = $oPBOptions->getDims();

    $dimensions_array = array();
    foreach (array('width', 'height', 'length', 'girth') as $_dim) {
        if (!empty($dim[$_dim])) {
            // Must be in inch to work with func_correct_dimensions
            $dimensions_array[$_dim] = func_units_convert(func_dim_in_centimeters($dim[$_dim]), 'cm', 'in', 64);
        }
    }

    $max_weight = $oPBOptions->getMaxWeight();
    if ($max_weight > 0) {
        // Must be lbs to work with func_correct_dimensions
        $dimensions_array['weight'] = func_units_convert(func_weight_in_grams($max_weight), 'g', 'lbs', 64);
    }

    $package_limits = $dimensions_array;

    foreach ($package_limits as $k => $v) {
        $package_limits[$k] = func_correct_dimensions($v);
    }

    if ($save_result_in_cache) {
        func_save_cache_func($package_limits, $md5_args, 'func_get_package_limits_PB');
    }

    return $package_limits;
} // }}}

/**
 * Check if PB allows box
 */
function func_check_limits_PB($box)
{ // {{{
    $box['weight'] = isset($box['weight']) ? $box['weight'] : 0;

    $pack_limit = func_get_package_limits_PB();
    $avail = (func_check_box_dimensions($box, $pack_limit) && $pack_limit['weight'] > $box['weight']);

    return $avail;
} // }}}

/**
 * Find shipping methods
 *
 * @param array $rates
 * @param array $pb_methods
 *
 * @return array
 */
function func_PB_find_methods($rates, $pb_methods)
{ // {{{

    if (empty($rates) || empty($pb_methods)) {
        // Display message to customer
        global $top_message;
        x_session_register('top_message');
        $top_message = array (
            'type' => 'E',
            'message' => func_get_langvar_by_name('msg_err_pitney_bowes_api_call_failed'),
        );
        // Return empty array
        return array();
    }

    $founded_rates = array();

    foreach ($rates as $rate) {

        // Try to find known method
        foreach ($pb_methods as $sm) {
            if ($rate->methodSubCode == $sm['subcode']) {

                $founded_rate = array(
                    'methodid'           => $sm['subcode'],
                    'rate'               => $rate->getRate(),
                );

                if (
                    ($trTime = $rate->getTransitTime())
                    && !empty($trTime)
                ) {
                    $founded_rate['shipping_time'] = $trTime;
                }

                $founded_rates[] = $founded_rate;

                break;
            }
        }
    }

    return $founded_rates;
} // }}}
