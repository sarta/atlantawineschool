<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/* * ***************************************************************************\
  +-----------------------------------------------------------------------------+
  | X-Cart Software license agreement                                           |
  | Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
  | All rights reserved.                                                        |
  +-----------------------------------------------------------------------------+
  | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
  | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
  | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
  |                                                                             |
  | THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
  | SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
  | (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
  | FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
  | THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
  | LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
  | INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
  | (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
  | LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
  | NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
  | PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
  | THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
  | SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
  | GRANTED BY THIS AGREEMENT.                                                  |
  +-----------------------------------------------------------------------------+
  \**************************************************************************** */

/**
 * HTTP - Curl
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Qualiteam software Ltd <info@x-cart.com>
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v2 (xcart_4_7_5), 2016-02-18 13:44:28, Curl.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace XCart\Modules\PitneyBowes\HTTP\Adapter;

/**
 * Custom Curl adapter for HTTP\Request
 */
class Curl extends \PEAR2\HTTP\Request\Adapter\Curl
{
    /**
     * The number of seconds to wait while trying to connect
     *
     * @var integer
     */
    protected $connectTimeout = 15;

    /**
     * Additional cURL options
     *
     * @var array
     */
    protected $additionalOptions = array();

    /**
     * Set additional cURL option
     *
     * @param string $name  Option name
     * @param mixed  $value Option value
     *
     * @return void
     */
    public function setAdditionalOption($name, $value)
    {
        $this->additionalOptions[$name] = $value;
    }

    /**
     * Add curl options
     *
     * @return void
     */
    protected function _setupRequest()
    {
        parent::_setupRequest();

        // The number of seconds to wait while trying to connect
        curl_setopt($this->curl, \CURLOPT_CONNECTTIMEOUT, $this->connectTimeout);

        // The maximum number of seconds to allow cURL functions to execute
        curl_setopt($this->curl, \CURLOPT_TIMEOUT, $this->connectTimeout + $this->requestTimeout);

        foreach ($this->additionalOptions as $name => $value) {
            curl_setopt($this->curl, $name, $value);
        }
    }

    /**
     * Send cURL request
     *
     * @return \PEAR2\HTTP\Request\Response
     * @throws \PEAR2\HTTP\Request\Exception
     */
    protected function _sendRequest()
    {
        $body = curl_exec($this->curl);
        $this->_notify('disconnect');

        if (false === $body) {
            x_log_add(
                PITNEY_BOWES,
                curl_error($this->curl)
            );

            throw new \PEAR2\HTTP\Request\Exception(
                'Curl ' . curl_error($this->curl) . ' (' . curl_errno($this->curl) . ')'
            );
        }

        $this->sentFilesize = false;

        if ($this->fp !== false) {
            fclose($this->fp);
        }

        $details = $this->uri->toArray();
        $details['code'] = curl_getinfo($this->curl, \CURLINFO_HTTP_CODE);

        $headers = new \PEAR2\HTTP\Request\Headers($this->headers);
        $cookies = array();

        return new \PEAR2\HTTP\Request\Response($details, $body, $headers, $cookies);
    }
}
