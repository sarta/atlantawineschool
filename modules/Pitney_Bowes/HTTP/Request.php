<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/* * ***************************************************************************\
  +-----------------------------------------------------------------------------+
  | X-Cart Software license agreement                                           |
  | Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
  | All rights reserved.                                                        |
  +-----------------------------------------------------------------------------+
  | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
  | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
  | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
  |                                                                             |
  | THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
  | SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
  | (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
  | FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
  | THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
  | LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
  | INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
  | (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
  | LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
  | NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
  | PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
  | THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
  | SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
  | GRANTED BY THIS AGREEMENT.                                                  |
  +-----------------------------------------------------------------------------+
  \**************************************************************************** */

/**
 * HTTP - Request
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Qualiteam software Ltd <info@x-cart.com>
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v2 (xcart_4_7_5), 2016-02-18 13:44:28, Request.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace XCart\Modules\PitneyBowes\HTTP;

/**
 * Request
 */
class Request extends \PEAR2\HTTP\Request
{
    /**
     * Error message
     *
     * @var string
     */
    protected $errorMsg = null;

    /**
     * Sets up the adapter
     *
     * @param string                      $url      URL for this request OPTIONAL
     * @param \PEAR2\HTTP\Request\Adapter $instance The adapter to use OPTIONAL
     *
     * @return void
     */
    public function __construct($url = null, $instance = null)
    {
        if (!$instance && extension_loaded('curl')) {
            $instance = new \XCart\Modules\PitneyBowes\HTTP\Adapter\Curl;
        }

        try {
            parent::__construct($url, $instance);

        } catch (\Exception $exception) {
            $this->errorMsg = $exception->getMessage();
            $this->logBouncerError($exception);
        }
    }

    /**
     * Asks for a response class from the adapter
     *
     * @return \PEAR2\HTTP\Request\Response
     */
    public function sendRequest()
    {
        try {
            $result = parent::sendRequest();

        } catch (\Exception $exception) {
            $result = null;
            $this->errorMsg = $exception->getMessage();
            $this->logBouncerError($exception);
        }

        return $result;
    }

    /**
     * Sends a request storing the output to a file
     *
     * @param string $file File to store to
     *
     * @return \PEAR2\HTTP\Request\Response
     */
    public function requestToFile($file)
    {
        try {
            $result = parent::sendRequest();

        } catch (\Exception $exception) {
            $result = null;
            $this->errorMsg = $exception->getMessage();
            $this->logBouncerError($exception);
        }

        return $result;
    }

    /**
     * Get last error message
     *
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMsg;
    }

    /**
     * Set additional cURL option
     *
     * @param string $name  Option name
     * @param mixed  $value Option value
     *
     * @return void
     */
    public function setAdditionalOption($name, $value)
    {
        if ($this->adapter instanceof \XCart\Modules\PitneyBowes\HTTP\Adapter\Curl) {
            $this->adapter->setAdditionalOption($name, $value);
        }
    }

    /**
     * Logging
     *
     * @param \Exception $exception Thrown exception
     *
     * @return void
     */
    protected function logBouncerError(\Exception $exception)
    {
        x_log_add(
            PITNEY_BOWES,
            $exception->getMessage()
        );
    }

    /**
     * Return type of log messages
     *
     * @return integer
     */
    protected function getLogLevel()
    {
        return LOG_WARNING;
    }
}
