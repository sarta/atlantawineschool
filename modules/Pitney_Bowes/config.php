<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart Software license agreement                                           |
| Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
| SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
| (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
| FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
| THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
| LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
| INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
| (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
| LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
| NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
| PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
| THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
| SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
| GRANTED BY THIS AGREEMENT.                                                  |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

/**
 * Module configuration
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Michael Bugrov
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v5 (xcart_4_7_5), 2016-02-18 13:44:28, config.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

if ( !defined('XCART_START') ) { header('Location: ../../'); die('Access denied'); }

define('PITNEY_BOWES', 'Pitney_Bowes');

// Add module specific tables
$sql_tbl['product_pb_exports']        = XC_TBL_PREFIX . 'product_pb_exports';
$sql_tbl['product_pb_restrictions']   = XC_TBL_PREFIX . 'product_pb_restrictions';

$sql_tbl['pb_exports']        = XC_TBL_PREFIX . 'pb_exports';
$sql_tbl['pb_orders']         = XC_TBL_PREFIX . 'pb_orders';
$sql_tbl['pb_parcels']        = XC_TBL_PREFIX . 'pb_parcels';
$sql_tbl['pb_parcel_items']   = XC_TBL_PREFIX . 'pb_parcel_items';

// Shipping options processor
$shipping_options_processors[PITNEY_BOWES]['PB'] = $xcart_dir . XC_DS . 'modules' . XC_DS . PITNEY_BOWES . XC_DS . 'shipping_options.php';

// Load module specific CSS files
$css_files[PITNEY_BOWES][] = array(); // main.css
$css_files[PITNEY_BOWES][] = array('admin' => true); // admin.css

// Enable debugging:
//define('XC_PITNEY_BOWES_DEBUG', true);

// Load module functions
if (!empty($include_func)) {

    require_once $xcart_dir . XC_DS . 'modules' . XC_DS . PITNEY_BOWES . XC_DS . 'func.php';

    // Initialize module
    if (!empty($include_init)) {
        func_pitney_bowes_init();
    }
}
