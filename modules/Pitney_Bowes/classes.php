<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/* * ***************************************************************************\
  +-----------------------------------------------------------------------------+
  | X-Cart Software license agreement                                           |
  | Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
  | All rights reserved.                                                        |
  +-----------------------------------------------------------------------------+
  | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
  | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
  | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
  |                                                                             |
  | THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
  | SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
  | (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
  | FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
  | THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
  | LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
  | INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
  | (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
  | LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
  | NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
  | PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
  | THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
  | SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
  | GRANTED BY THIS AGREEMENT.                                                  |
  +-----------------------------------------------------------------------------+
  \**************************************************************************** */

/**
 * Classes
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Michael Bugrov
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v10 (xcart_4_7_5), 2016-02-18 13:44:28, classes.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

abstract class XCPitneyBowesDefs { //{{{

    const ASN_STATUS_PENDING    = 0;
    const ASN_STATUS_SENT       = 1;

    const CLASS_PREFIX = 'XCPitneyBowesController';

    const CART_TRANSACTIONID = 'pitney_bowes_cart_transaction_id';
    const CART_ORDERS = 'pitney_bowes_cart_orders';

    const DISPLAY_TYPE_SELF = 'self';
    const DISPLAY_TYPE_CUST = 'custom_template';

    const SHIPPING_CODE = 'PB';
    const SHIPPING_SUBCODE_PB_INTERNATIONAL = 'PB_INTERNATIONAL';

    const YES = 'Y';
    const NO  = 'N';

    const CONFIG_VAR_API_USERNAME       = 'api_username';
    const CONFIG_VAR_API_PASSWORD       = 'api_password';
    const CONFIG_VAR_SFTP_USERNAME      = 'sftp_username';
    const CONFIG_VAR_SFTP_PASSWORD      = 'sftp_password';
    const CONFIG_VAR_SFTP_CATALOG_DIR   = 'sftp_catalog_directory';
    const CONFIG_VAR_MERCHANT_CODE      = 'merchant_code';
    const CONFIG_VAR_SENDER_ID          = 'sender_id';

    const CONTROLLER = 'controller';

    const CONTROLLER_SETTINGS   = 'Settings';
    const CONTROLLER_CATALOG    = 'Catalog';
    const CONTROLLER_PARCELS    = 'Parcels';
    const CONTROLLER_CREDENTIALS    = 'Credentials';
    const CONTROLLER_PARCEL_ITEMS   = 'ParcelItems';
    const CONTROLLER_LABEL_PRINTER  = 'LabelPrinter';

    const EXPORT_TYPE       = 'export_type';
    const EXPORT_TYPE_FULL  = 'full';
    const EXPORT_TYPE_DIFF  = 'diff';

    const PARCEL_NUMBER_LENGTH = 22;

    const PARCEL_ORDERID = 'orderid';
    const PARCEL_PARCELID = 'parcelid';
    const PARCEL_PARCELS = 'parcels';
    const PARCEL_ITEMS = 'parcel_items';
    const PARCEL_HUB_ADDRESS = 'hub_address';
    const PARCEL_ORDER_NUMBER = 'order_number';

    const ORDERS_LIST = 'orders_hubs_list';

    const PARCEL_ACTION_CREATE_PARCEL = 'create_parcel';
    const PARCEL_ACTION_DELETE_PARCEL = 'delete_parcel';
    const PARCEL_ACTION_EDIT_ITEMS = 'edit_items';
    const PARCEL_ACTION_VIEW_ITEMS = 'view_items';
    const PARCEL_ACTION_SEND_ASN = 'send_asn';
    const PARCEL_ACTION_PRINT_LABEL = 'print_label';
    const PARCEL_ACTION_ADD_PARCEL_ITEMS = 'add_parcel_items';
    const PARCEL_ACTION_UPDATE_PARCEL_ITEM = 'update_parcel_item';
    const PARCEL_ACTION_DELETE_PARCEL_ITEM = 'delete_parcel_item';

    const EXPORT_STATUS_APPROVED    = 1;
    const EXPORT_STATUS_FAILED      = 2;
    const EXPORT_STATUS_PENDING     = 0;

    const HISTORY_CODE = 'PB';

    const RESTRICTED_PRODUCTS_SHOW = 'Show';
    const RESTRICTED_PRODUCTS_HIDE = 'Hide';

    const DATASET_STATUS_PENDING    = 0;
    const DATASET_STATUS_EXPORTED   = 1;

    const ORDER_PARCELS = 'pitney_bowes_order_parcels';

    const SHIPPING_MARKUP_BASIS_ITEMS   = 'Items';
    const SHIPPING_MARKUP_BASIS_CART    = 'Cart';

    const UNITS_WEIGHT = 'g';
    const UNITS_DISTANCE = 'cm';

    const EVENT_EXPORT_CATALOG_FULL = 'pitney_bowes_ecf_ttl';
    const EVENT_EXPORT_CATALOG_DIFF = 'pitney_bowes_ecd_ttl';

} //}}}

abstract class XCPitneyBowesAdminRequestProcessor { // {{{

    protected $displayTemplate = '';
    protected $displayType = XCPitneyBowesDefs::DISPLAY_TYPE_CUST;

    protected $sessionVars = array();
    protected $globalVars = array();
    protected $smartyVars = array();

    protected $requiredConfigVars = array();

    public static function processRequest()
    { //{{{
        global $smarty, $REQUEST_METHOD, ${XCPitneyBowesDefs::CONTROLLER};

        $controllerClass = XCPitneyBowesDefs::CLASS_PREFIX . XCStringUtils::convertToCamelCase(${XCPitneyBowesDefs::CONTROLLER});

        if (class_exists($controllerClass)) {

            $controllerInstance = new $controllerClass();

            $controllerInstance->registerGlobalVars();
            $controllerInstance->registerSessionVars();

            $smarty->assign(XCPitneyBowesDefs::CONTROLLER, ${XCPitneyBowesDefs::CONTROLLER});

            switch ($REQUEST_METHOD) {
                case 'GET':
                    $controllerInstance->display();
                    break;
                case 'POST':
                    if (($opt_code = $controllerInstance->isConfigured()) !== true) {
                        // not all required options were configured
                        global $top_message;
                        x_session_register('top_message');

                        $top_message['content']
                            = func_get_langvar_by_name (
                                'msg_err_pitney_bowes_catalog_not_configured',
                                array(
                                    'oname' => func_get_langvar_by_name('opt_pb_' . $opt_code),
                                    'ocode' => $opt_code
                                )
                            );
                        $top_message['type'] = 'E';

                    } else {
                        // process request
                        $controllerInstance->process();
                    }
                    // redirect to controller page in case of post request
                    func_header_location($controllerInstance->getNavigationScript());
                    break;
            }
        }

    } //}}}

    public static function registerSessionVar($varName, $className = null)
    { // {{{
        if (is_null($className)) {
            $className = get_called_class();
        }

        $classVarName = $className . "_$varName";
        global ${$classVarName};
        x_session_register($classVarName);

        return $classVarName;
    } // }}}

    public static function setSessionVar($varName, $varValue, $className = null)
    { // {{{
        $classVarName = static::registerSessionVar($varName, $className);
        global ${$classVarName};
        ${$classVarName} = $varValue;
    } // }}}

    protected function getControllerName()
    { // {{{
        return str_replace(XCPitneyBowesDefs::CLASS_PREFIX, '', get_class($this));
    } // }}}

    protected function getNavigationScript()
    { // {{{
        $option = PITNEY_BOWES;
        $controller = $this->getControllerName();

        return "configuration.php?option=$option&controller=$controller";
    } // }}}

    protected function registerGlobalVars()
    { // {{{
        if (!empty($this->globalVars)) {
            foreach ($this->globalVars as $varName) {
                global ${$varName};
                $this->$varName = ${$varName};
            }
        }
    } // }}}

    protected function registerSessionVars()
    { // {{{
        if (!empty($this->sessionVars)) {
            foreach ($this->sessionVars as $varName) {
                $classVarName = static::registerSessionVar($varName, get_class($this));

                global ${$varName}, ${$classVarName};

                if (isset(${$varName})) {
                    ${$classVarName} = ${$varName};
                }

                $this->$varName = ${$classVarName};
            }
        }
    } // }}}

    protected function registerSmartyVars()
    { // {{{
        global $smarty;

        if (!empty($this->smartyVars)) {
            foreach ($this->smartyVars as $varName) {
                if (property_exists($this, $varName)) {
                    $smarty->assign($varName, $this->$varName);
                }
            }
        }
    } // }}}

    /**
     * Check if controller is configured
     *
     * @return mixed true on success and option code on fail
     */
    protected function isConfigured()
    { // {{{
        $result = true;

        $config = \XCPitneyBowesConfig::getInstance()->getConfigAsObject();

        foreach ($this->requiredConfigVars as $varname) {
            if (
                !property_exists($config, $varname)
                || empty($config->$varname)
            ) {
                $result = $varname;
            }
        }

        return $result;
    } // }}}

    protected function display()
    { // {{{
        global $smarty;

        $this->registerSmartyVars();

        $smarty->assign('navigation_script', $this->getNavigationScript());

        if ($this->displayType === XCPitneyBowesDefs::DISPLAY_TYPE_SELF) {

            func_display($this->displayTemplate, $smarty);

            exit();
        }
        else {
            $smarty->assign('custom_admin_module_config_tpl_file', $this->displayTemplate);
        }
    } // }}}

    abstract protected function process();

} // }}}

class XCPitneyBowesControllerCatalog extends XCPitneyBowesAdminRequestProcessor { // {{{

    protected $displayTemplate = 'modules/Pitney_Bowes/catalog.tpl';

    protected $exportProcessor; // Export processor

    protected $requiredConfigVars = array (
        XCPitneyBowesDefs::CONFIG_VAR_SFTP_USERNAME,
        XCPitneyBowesDefs::CONFIG_VAR_SFTP_PASSWORD,
        XCPitneyBowesDefs::CONFIG_VAR_SFTP_CATALOG_DIR,
        XCPitneyBowesDefs::CONFIG_VAR_MERCHANT_CODE,
        XCPitneyBowesDefs::CONFIG_VAR_SENDER_ID
    );

    protected $sessionVars = array (
        XCPitneyBowesDefs::EXPORT_TYPE
    );
    protected $smartyVars = array (
        XCPitneyBowesDefs::EXPORT_TYPE
    );

    protected function __construct()
    { // {{{
        $this->exportProcessor = new XCPitneyBowesExporter();
    } // }}}

    protected function display()
    { // {{{
        global $xcart_dir, $sql_tbl, $config, $smarty;
        global $page, $total_items, $objects_per_page, $first_page; // Navigation globals

        $total_items = func_query_first_cell ("SELECT COUNT(*) FROM $sql_tbl[pb_exports]");

        if ($total_items > 0) {

            include $xcart_dir . '/include/navigation.php';

            // Get export items list
            $export_items = func_query("SELECT * FROM $sql_tbl[pb_exports]".($objects_per_page > 0 ? " LIMIT $first_page, $objects_per_page" : ""));

            // Pre-process display values
            foreach ($export_items as $idx => $item) {
                // type
                $parts = explode('_', $item['filename']);
                $export_items[$idx]['type'] = ucfirst($parts[1]) ?: func_get_langvar_by_name('lbl_unknown');
                // status
                $status_text = func_get_langvar_by_name('lbl_unknown');
                switch ($export_items[$idx]['status']) {
                    case XCPitneyBowesDefs::EXPORT_STATUS_PENDING:
                        $status_text = func_get_langvar_by_name('lbl_pending');
                        break;
                    case XCPitneyBowesDefs::EXPORT_STATUS_APPROVED:
                        $status_text = func_get_langvar_by_name('lbl_approved');
                        break;
                    case XCPitneyBowesDefs::EXPORT_STATUS_FAILED:
                        $status_text = func_get_langvar_by_name('lbl_failed');
                        break;
                }
                $export_items[$idx]['status'] = $status_text;
            }

            $smarty->assign('export_items', $export_items);

            $smarty->assign('navigation_script', $this->getNavigationScript());
            $smarty->assign('first_item',        $first_page + 1);
            $smarty->assign('last_item',         min($first_page + $objects_per_page, $total_items));
        }

        parent::display();
    } // }}}

    protected function process()
    { // {{{

        $extract_method = 'extract' . ucfirst($this->{XCPitneyBowesDefs::EXPORT_TYPE});

        if (method_exists($this, $extract_method)) {
            $this->$extract_method();
        }
    } // }}}

    protected function extractFull()
    { // {{{

        func_pitney_bowes_mark_all_products_pending();

        $this->exportProcessor->export();

    } // }}}

    protected function extractDiff()
    { // {{{
        $this->exportProcessor->export();
    } // }}}

} //}}}

class XCPitneyBowesControllerCheck extends XCPitneyBowesControllerCatalog { // {{{

    protected function process()
    {
        $fileExchange = new XCart\Modules\PitneyBowes\Catalog\FileExchange\Processor();
        $fileExchange->checkNotifications();

        $option = PITNEY_BOWES;
        $controller = XCPitneyBowesDefs::CONTROLLER_CATALOG;

        func_header_location("configuration.php?option=$option&controller=$controller");
    }
}

class XCPitneyBowesAdminOrderRequestProcessor extends XCPitneyBowesAdminRequestProcessor { // {{{

    protected $parcels = array();

    protected function __construct()
    { // {{{
        $this->loadOrderParcels();
        $this->loadParcelItems();
    } // }}}

    protected function loadOrderParcels()
    { // {{{
        global $sql_tbl, $orderid;

        $orderid = intval($orderid);

        $parcels = func_query(
            "SELECT $sql_tbl[pb_parcels].*"
            . " FROM $sql_tbl[pb_parcels]"
            . " WHERE $sql_tbl[pb_parcels].orderid = '$orderid'"
            . " ORDER BY $sql_tbl[pb_parcels].id"
        );

        if (!empty($parcels)) {
            foreach ($parcels as $parcel) {
                $this->parcels[$parcel['id']] = $parcel;
            }
        }
    } // }}}

    protected function loadParcelItems()
    { // {{{
        global $sql_tbl, $orderid;

        $orderid = intval($orderid);

        $parcel_items = func_query(
            "SELECT $sql_tbl[pb_parcel_items].*"
            . " FROM $sql_tbl[pb_parcels]"
            . " INNER JOIN $sql_tbl[pb_parcel_items]"
                . " ON $sql_tbl[pb_parcels].id = $sql_tbl[pb_parcel_items].parcelid"
                . " AND $sql_tbl[pb_parcels].orderid = $sql_tbl[pb_parcel_items].orderid"
                . " AND $sql_tbl[pb_parcels].orderid = '$orderid'"
            . " ORDER BY $sql_tbl[pb_parcels].id"
        );

        if (!empty($parcel_items)) {
            foreach ($parcel_items as $parcel_item) {
                $this->parcels[$parcel_item['parcelid']]['items'][$parcel_item['itemid']] = $parcel_item;
            }
        }

    } // }}}

    protected function getAmountInAllParcels($orderItem)
    { // {{{
        $parcels = $this->parcels;

        return array_reduce($parcels, function($carry, $parcel) use ($orderItem) {
            return $carry
                + (!empty($parcel['items'][$orderItem['itemid']]['quantity'])
                    ? $parcel['items'][$orderItem['itemid']]['quantity']
                    : 0);
        });
    } // }}}

    protected function getAvailableAmount($orderItem, $parcel = null)
    { // {{{
        $getAmountInParcels = $this->getAmountInAllParcels($orderItem);

        if (
            null !== $parcel
            && !empty($parcel['items'][$orderItem['itemid']]['quantity'])
        ) {
            $getAmountInParcels -= $parcel['items'][$orderItem['itemid']]['quantity'];
        }

        return $orderItem['amount'] - $getAmountInParcels;
    } // }}}

    protected function isNewParcelAllowed()
    { // {{{
        global $orderid;

        $orderData  = func_order_data($orderid);

        $orderItems = !empty($orderData['products']) ? $orderData['products'] : array();

        $tempThis = $this;

        $hasOrderItems = array_reduce($orderItems, function($carry, $orderItem) use ($tempThis) {
            return $carry ?: 0 < $tempThis->getAvailableAmount($orderItem);
        });

        $hasEmptyParcels = array_reduce($this->parcels, function($carry, $parcel){
            return $carry ?: (!empty($parcel['items']) && count($parcel['items']) === 0);
        });

        return $hasOrderItems && !$hasEmptyParcels;
    } // }}}

    protected function getOrderItemInfo($orderid, $itemid)
    { // {{{
        $result = false;

        $orderData = func_order_data($orderid);

        if (!empty($orderData)) {
            foreach ($orderData['products'] as $itemInfo) {
                if ($itemInfo['itemid'] === $itemid) {
                    return $itemInfo;
                }
            }
        }

        return $result;
    } // }}}

    protected function getNavigationScript()
    { // {{{
        global $orderid;

        $orderid = intval($orderid);
        $controller = $this->getControllerName();

        return "order.php?orderid=$orderid&controller=$controller";
    } // }}}

    protected function display()
    { // {{{
        global $location, $orderid;

        $last_location = array_pop($location);
        $last_location[1] = 'order.php?orderid=' . intval($orderid);

        $location[] = $last_location;
        $location[] = array(func_get_langvar_by_name('lbl_pitney_bowes_' . XCStringUtils::convertFromCamelCase($this->getControllerName())), '');

        parent::display();
    } // }}}

    protected function process()
    { // {{{
        global $action;

        $action = 'action' . XCStringUtils::convertToCamelCase($action);

        if (method_exists($this, $action)) {
            $this->$action();
        }
    } // }}}

} // }}}

class XCPitneyBowesControllerParcels extends XCPitneyBowesAdminOrderRequestProcessor { // {{{

    protected $smartyVars = array(
        'main',
        XCPitneyBowesDefs::PARCEL_ORDERID,
        XCPitneyBowesDefs::PARCEL_PARCELS,
        XCPitneyBowesDefs::PARCEL_HUB_ADDRESS,
        XCPitneyBowesDefs::PARCEL_ORDER_NUMBER,
        XCPitneyBowesDefs::ORDERS_LIST,
    );

    protected $main = XCPitneyBowesDefs::PARCEL_PARCELS;

    protected function __construct()
    { // {{{
        $this->loadHubAddress();

        parent::__construct();
    } // }}}

    protected function display()
    {
        global $smarty;

        $smarty->assign('createParcelAllowed', $this->isNewParcelAllowed());

        parent::display();
    }

    protected function loadHubAddress()
    { // {{{
        global $sql_tbl, $orderid;

        $orderid = intval($orderid);

        $ordersRecord = func_query_first("SELECT * FROM $sql_tbl[pb_orders] WHERE orderid = '$orderid'");
        $ordersInfo = unserialize($ordersRecord['createOrderResponse']);

        if (!empty($ordersInfo)) {
            $this->{XCPitneyBowesDefs::ORDERS_LIST} = array();

            foreach ($ordersInfo as $orderInfo) {
                $this->{XCPitneyBowesDefs::ORDERS_LIST}[$orderInfo->orderId] = array (
                    XCPitneyBowesDefs::PARCEL_ORDER_NUMBER => $orderInfo->orderId,
                    XCPitneyBowesDefs::PARCEL_HUB_ADDRESS =>
                        "{$orderInfo->shipToHub->hubAddress->street1}"
                        . ", {$orderInfo->shipToHub->hubAddress->city}"
                        . ", {$orderInfo->shipToHub->hubAddress->provinceOrState}"
                        . ", {$orderInfo->shipToHub->hubAddress->postalOrZipCode}"
                        . ", {$orderInfo->shipToHub->hubAddress->country}"
                );
            }

            // Use first order from the list
            $shipToHub = reset($this->{XCPitneyBowesDefs::ORDERS_LIST});

            $this->{XCPitneyBowesDefs::PARCEL_HUB_ADDRESS} = $shipToHub[XCPitneyBowesDefs::PARCEL_HUB_ADDRESS];
            $this->{XCPitneyBowesDefs::PARCEL_ORDER_NUMBER} = $shipToHub[XCPitneyBowesDefs::PARCEL_ORDER_NUMBER];
        }
    } // }}}

    protected function actionCreateParcel()
    { // {{{
        global $orderid;

        $orderid = intval($orderid);

        $parcel = array (
            'orderid' => $orderid,
            'parcel_number' => func_pitney_bowes_generate_token(),
            'createAsnCalled' => XCPitneyBowesDefs::ASN_STATUS_PENDING,
        );

        $result = func_array2insert('pb_parcels', $parcel);

        if ($result !== false) {
            global $top_message;
            x_session_register('top_message');

            $top_message = array (
                'type' => 'I',
                'content' => func_get_langvar_by_name('msg_pitney_bowes_parcel_created')
            );
        }

    } // }}}

    protected function actionEditItems()
    { // {{{
        global $orderid, $parcelid;

        $orderid = intval($orderid);
        $parcelid = intval($parcelid);

        $controller = XCPitneyBowesDefs::CONTROLLER_PARCEL_ITEMS;

        func_header_location("order.php?orderid=$orderid&controller={$controller}&parcelid={$parcelid}");
    } // }}}

    protected function actionViewItems()
    { // {{{
        global $orderid, $parcelid;

        $orderid = intval($orderid);
        $parcelid = intval($parcelid);

        $controller = XCPitneyBowesDefs::CONTROLLER_PARCEL_ITEMS;

        func_header_location("order.php?orderid=$orderid&controller={$controller}&parcelid={$parcelid}");
    } // }}}

    protected function actionDeleteParcel()
    { // {{{
        global $sql_tbl, $orderid, $parcelid;

        $orderid = intval($orderid);
        $parcelid = intval($parcelid);

        $createAsnCalled = func_query_first_cell(
            "SELECT createAsnCalled"
            . " FROM $sql_tbl[pb_parcels]"
            . " WHERE orderid='$orderid' AND id='$parcelid'"
        );

        if ($createAsnCalled == XCPitneyBowesDefs::ASN_STATUS_PENDING) {
            db_query(
                "DELETE FROM $sql_tbl[pb_parcels]"
                . " WHERE orderid='$orderid' AND id='$parcelid'"
            );
            db_query(
                "DELETE FROM $sql_tbl[pb_parcel_items]"
                . " WHERE orderid='$orderid' AND parcelid='$parcelid'"
            );

            global $top_message;
            x_session_register('top_message');

            $top_message = array (
                'type' => 'I',
                'content' => func_get_langvar_by_name('msg_pitney_bowes_parcel_deleted')
            );
        }
    } // }}}

    protected function actionPrintLabel()
    { // {{{
        global $orderid, $parcelid, ${XCPitneyBowesDefs::PARCEL_ORDER_NUMBER}, ${XCPitneyBowesDefs::PARCEL_HUB_ADDRESS};

        $orderid = intval($orderid);
        $parcelid = intval($parcelid);

        XCPitneyBowesControllerLabelPrinter::setSessionVar(
            XCPitneyBowesDefs::PARCEL_ORDER_NUMBER,
            ${XCPitneyBowesDefs::PARCEL_ORDER_NUMBER}
        );

        XCPitneyBowesControllerLabelPrinter::setSessionVar(
            XCPitneyBowesDefs::PARCEL_HUB_ADDRESS,
            ${XCPitneyBowesDefs::PARCEL_HUB_ADDRESS}
        );

        $controller = XCPitneyBowesDefs::CONTROLLER_LABEL_PRINTER;

        func_header_location("order.php?orderid=$orderid&controller={$controller}&parcelid={$parcelid}");
    } // }}}

    protected function actionSendAsn()
    { // {{{
        global $sql_tbl, $config, $orderid, $parcelid, ${XCPitneyBowesDefs::PARCEL_ORDER_NUMBER};

        $orderid = intval($orderid);
        $parcelid = intval($parcelid);

        if (empty($this->parcels[$parcelid]['items'])) {
            return false;
        }

        $orderData = func_order_data($orderid);

        $ormus_number = ${XCPitneyBowesDefs::PARCEL_ORDER_NUMBER};

        if (
            empty($orderData['order'][XCPitneyBowesDefs::CART_ORDERS])
            || !in_array($ormus_number, array_keys($orderData['order'][XCPitneyBowesDefs::CART_ORDERS]))
        ) {
            return false;
        }

        $orderItems = !empty($orderData['products']) ? $orderData['products'] : array();

        foreach ($orderItems as $orderItem) {

            // Fill in required items info in current parcel
            if (!empty($this->parcels[$parcelid]['items'][$orderItem['itemid']])) {

                $this->parcels[$parcelid]['items'][$orderItem['itemid']]['provider'] = $orderItem['provider'];

                $this->parcels[$parcelid]['items'][$orderItem['itemid']]['productid'] = $orderItem['productid'];
                $this->parcels[$parcelid]['items'][$orderItem['itemid']]['weight'] = $orderItem['weight'];

            }

        }

        $providers = func_get_products_providers($this->parcels[$parcelid]['items']);

        $provider = !empty($providers) ? reset($providers) : 0;

        $default_seller_address = XCShipFrom::getAddressArray();

        $is_empty_seller_address =  (
            func_is_seller_address_empty($provider)
            || !func_is_seller_address_applicable($provider, 'N')
        );

        $orig_address = $is_empty_seller_address
            ? $default_seller_address
            : func_query_first("SELECT * FROM $sql_tbl[seller_addresses] WHERE userid='$provider'");

        $this->parcels[$parcelid]['ormus_number'] = $ormus_number;

        $api = new \XCart\Modules\PitneyBowes\Shipping\API\PitneyBowesAPI(XCPitneyBowesConfig::getInstance()->getConfigAsObject());

        $pbRequestData = array(
            'pbParcel' => $this->parcels[$parcelid],
            'origAddress' => func_pitney_bowes_get_original_address($orig_address),
        );

        $asn_number = $api->createInboundParcelsRequest($pbRequestData);

        global $top_message;
        x_session_register('top_message');

        $top_message = array (
            'type' => 'E',
            'content' => func_get_langvar_by_name('err_pitney_bowes_asn_request_failed')
        );

        if ($asn_number) {

            $asnStatus = XCPitneyBowesDefs::ASN_STATUS_SENT;

            func_query(
                "UPDATE $sql_tbl[pb_parcels]"
                . " SET ormus_number = '$ormus_number',"
                    . " asn_number = '$asn_number',"
                    . " createAsnCalled = '$asnStatus'"
                . " WHERE orderid = '$orderid'"
                    . " AND id = '$parcelid'"
            );

            $top_message = array (
                'type' => 'I',
                'content' => func_get_langvar_by_name('msg_pitney_bowes_asn_sent')
            );
        }

    } // }}}

} // }}}

class XCPitneyBowesControllerParcelItems extends XCPitneyBowesAdminOrderRequestProcessor { // {{{

    protected $globalVars = array(
        XCPitneyBowesDefs::PARCEL_PARCELID,
    );

    protected $smartyVars = array(
        'main',
        XCPitneyBowesDefs::PARCEL_ORDERID,
        XCPitneyBowesDefs::PARCEL_PARCELID,
        XCPitneyBowesDefs::PARCEL_PARCELS,
    );

    protected $main = XCPitneyBowesDefs::PARCEL_ITEMS;

    protected function getNavigationScript()
    { // {{{
        return parent::getNavigationScript() . "&parcelid={$this->{XCPitneyBowesDefs::PARCEL_PARCELID}}";
    } // }}}

    protected function getAvailableItems()
    { // {{{
        global $orderid, $parcelid;

        $availableItems = array();

        $orderid = intval($orderid);
        $parcelid = intval($parcelid);

        $orderData = func_order_data($orderid);

        $orderItems = !empty($orderData['products']) ? $orderData['products'] : array();

        foreach ($orderItems as $orderItem) {

            // Check availibility of each item
            if (($max = $this->getAvailableAmount($orderItem)) > 0) {

                $availableItems[] = array (
                    'itemid' => $orderItem['itemid'],
                    'sku' => $orderItem['productcode'],
                    'name' => $orderItem['product'],
                    'max' => $max,
                );

            }

            // Fill in items info in current parcel
            if (!empty($this->parcels[$parcelid]['items'][$orderItem['itemid']])) {
                $this->parcels[$parcelid]['items'][$orderItem['itemid']]['sku'] = $orderItem['productcode'];
                $this->parcels[$parcelid]['items'][$orderItem['itemid']]['name'] = $orderItem['product'];
                $this->parcels[$parcelid]['items'][$orderItem['itemid']]['max'] = $this->getAvailableAmount($orderItem, $this->parcels[$parcelid]);
            }

        }

        return $availableItems;
    } // }}}

    protected function display()
    { // {{{
        global $smarty;

        $smarty->assign('available_items', $this->getAvailableItems());

        parent::display();
    } // }}}

    protected function actionAddParcelItems()
    { // {{{
        global $orderid, $parcelid, $itemsToAdd;

        $orderid = intval($orderid);
        $parcelid = intval($parcelid);

        if (!empty($itemsToAdd)) {

            foreach ($itemsToAdd as $itemToAdd) {

                $parcelItem = func_array_map('intval',
                    array (
                        'orderid' => $orderid,
                        'parcelid' => $parcelid,
                        'itemid' => $itemToAdd['itemid'],
                        'quantity' => $itemToAdd['quantity'],
                    )
                );

                $orderItem = $this->getOrderItemInfo($orderid, $itemToAdd['itemid']);

                if (($max = $this->getAvailableAmount($orderItem, $this->parcels[$parcelid])) > 0) {

                    if (
                        !empty($this->parcels[$parcelid]['items'][$itemToAdd['itemid']])
                        && $this->parcels[$parcelid]['items'][$itemToAdd['itemid']]['quantity'] + $parcelItem['quantity'] <= $max
                    ) {
                        $parcelItem['quantity'] += $this->parcels[$parcelid]['items'][$itemToAdd['itemid']]['quantity'];
                    }

                    $result = func_array2insert('pb_parcel_items', $parcelItem, true);

                    if ($result !== false) {
                        global $top_message;
                        x_session_register('top_message');

                        $top_message = array (
                            'type' => 'I',
                            'content' => func_get_langvar_by_name('msg_pitney_bowes_parcel_items_added')
                        );
                    }
                }
            }
        }

    } // }}}

    protected function actionUpdateParcelItem()
    { // {{{
        global $sql_tbl, $orderid, $parcelid, $itemid, $quantity;

        $orderid = intval($orderid);
        $parcelid = intval($parcelid);
        $itemid = intval($itemid);

        $quantity = intval($quantity);

        func_query(
            "UPDATE $sql_tbl[pb_parcel_items] SET quantity = '$quantity'"
            . " WHERE orderid='$orderid' AND parcelid='$parcelid' AND itemid='$itemid'"
        );

        global $top_message;
        x_session_register('top_message');

        $top_message = array (
            'type' => 'I',
            'content' => func_get_langvar_by_name('msg_pitney_bowes_parcel_item_updated')
        );
    } // }}}

    protected function actionDeleteParcelItem()
    { // {{{
        global $sql_tbl, $orderid, $parcelid, $itemid;

        $orderid = intval($orderid);
        $parcelid = intval($parcelid);
        $itemid = intval($itemid);

        db_query(
            "DELETE FROM $sql_tbl[pb_parcel_items]"
            . " WHERE orderid='$orderid' AND parcelid='$parcelid' AND itemid='$itemid'"
        );

        global $top_message;
        x_session_register('top_message');

        $top_message = array (
            'type' => 'I',
            'content' => func_get_langvar_by_name('msg_pitney_bowes_parcel_item_deleted')
        );
    } // }}}

} // }}}

class XCPitneyBowesControllerLabelPrinter extends XCPitneyBowesAdminOrderRequestProcessor { // {{{

    const className = __CLASS__;

    protected $sessionVars = array (
        XCPitneyBowesDefs::PARCEL_HUB_ADDRESS,
        XCPitneyBowesDefs::PARCEL_ORDER_NUMBER,
    );
    protected $smartyVars = array (
        XCPitneyBowesDefs::PARCEL_HUB_ADDRESS,
        XCPitneyBowesDefs::PARCEL_ORDER_NUMBER,
    );

    protected $displayTemplate = 'modules/Pitney_Bowes/shipping_label.tpl';
    protected $displayType = XCPitneyBowesDefs::DISPLAY_TYPE_SELF;

    protected function display()
    {
        global $sql_tbl, $smarty, $orderid, $parcelid;

        $orderid = intval($orderid);
        $parcelid = intval($parcelid);

        $printer = new \XCart\Modules\PitneyBowes\Barcode\Printer();

        $parcelInfo = func_query_first(
            "SELECT *"
            . " FROM $sql_tbl[pb_parcels]"
            . " WHERE orderid='$orderid'"
                . " AND id='$parcelid'");

        if (empty($parcelInfo)) {
            return;
        }

        $order = func_select_order($orderid);

        if (empty($order)) {
            return;
        }

        $barcode_img = $printer->getBarcode(
            array(
                'text' => $parcelInfo['parcel_number']
            )
        );

        $recipient = "$order[b_firstname] $order[b_lastname]";
        $barcode_text = $parcelInfo['parcel_number'];

        $smarty->assign('recipient', $recipient);
        $smarty->assign('barcode_text', $barcode_text);

        $smarty->assign('barcode_img', base64_encode($barcode_img));

        parent::display();
    }

} // }}}

class XCPitneyBowesControllerCredentials extends XCPitneyBowesAdminRequestProcessor { // {{{

    const CREDENTIALS_REQUEST_EMAIL_ENPOINT = 'ecomm_bd@pb.com';

    protected $displayTemplate = 'modules/Pitney_Bowes/request_credentials.tpl';

    protected $sessionVars = array (
        'pb_cr_name', 'pb_cr_company', 'pb_cr_address', 'pb_cr_email', 'pb_cr_phone', 'pb_cr_version', 'fill_errors'
    );
    protected $smartyVars = array (
        'pb_cr_name', 'pb_cr_company', 'pb_cr_address', 'pb_cr_email', 'pb_cr_phone', 'pb_cr_version', 'fill_errors'
    );

    protected function display()
    {
        global $sql_tbl, $smarty, $logged_userid;

        $xcart_version = func_query_first_cell("SELECT value FROM $sql_tbl[config] WHERE name='version'");

        $smarty->assign('xcart_version', $xcart_version);
        $smarty->assign('userinfo', func_userinfo($logged_userid));

        parent::display();
    }

    protected function process()
    { // {{{

        $this->fill_errors = array();

        $fields_to_check = $this->smartyVars;
        array_pop($fields_to_check);

        // Check required fields
        foreach ($fields_to_check as $field) {
            if (empty($this->$field)) {
                $this->fill_errors[$field] = true;
            }
        }

        // Additional check for email
        if (!func_check_email($this->pb_cr_email)) {
            $this->fill_errors['pb_cr_email'] = true;
        }

        if (empty($this->fill_errors)) {

            $request_msg = <<<RQM
Your Name: {$this->pb_cr_name}

Company Name: {$this->pb_cr_company}

Email: {$this->pb_cr_email}

Phone Number: {$this->pb_cr_phone}

Business Address: {$this->pb_cr_address}

Current X-Cart Version: {$this->pb_cr_version}
RQM;

            $result = func_send_simple_mail(
                self::CREDENTIALS_REQUEST_EMAIL_ENPOINT,
                func_get_langvar_by_name('lbl_pitney_bowes_credentials_request', null, false, true),
                $request_msg,
                "{$this->pb_cr_name} <{$this->pb_cr_email}>"
            );

            global $top_message;

            x_load('ajax');
            x_session_register('top_message');

            if ($result) {

                global $sql_tbl;

                db_query("UPDATE $sql_tbl[config] SET value = '1' WHERE name = 'pb_credentials_request'");

                $top_message = array (
                    'type' => 'I',
                    'content' => func_get_langvar_by_name('msg_pitney_bowes_crt_sent_short', null, false, true),
                );

                func_send_simple_mail(
                    $this->pb_cr_email,
                    func_get_langvar_by_name('lbl_pitney_bowes_credentials_request', null, false, true),
                    func_get_langvar_by_name('msg_pitney_bowes_crt_sent_long', null, false, true),
                    'Pitney Bowes <' . self::CREDENTIALS_REQUEST_EMAIL_ENPOINT . '>'
                );

            } else {

                $top_message = array (
                    'type' => 'E',
                    'content' => func_get_langvar_by_name('err_pitney_bowes_credential_request_fail', null, false, true),
                );
            }

            x_session_save('top_message');

            func_reload_parent_window();

        } else {

            self::setSessionVar('fill_errors', $this->fill_errors);

            if (func_is_ajax_request()) {

                func_register_ajax_message(
                    'popupDialogCall', array(
                    'action' => 'load',
                    'src' => $this->getNavigationScript()
                    )
                );
            }
        }
    } // }}}

} // }}}

abstract class XCPitneyBowesExportImportProcessor { // {{{

    const EXPORT_DIR_NAME = 'export';
    const IMPORT_DIR_NAME = 'import';

    protected $_config;

    protected $_workingDir;
    protected $_workingFiles;

    /**
     * @var XCart\Modules\PitneyBowes\Catalog\File
     */
    protected $_currentFile;

    protected abstract function defineDir();
    protected abstract function defineFiles();

    public function __construct()
    { // {{{
        set_time_limit(86400);

        func_set_memory_limit('96M');

        x_load('category','product','files','export');

        $this->_config = \XCPitneyBowesConfig::getInstance()->getConfigAsObject();

        $this->defineDir();
        $this->defineFiles();
    } // }}}

    public function getConfig()
    { // {{{
        return $this->_config;
    } // }}}

    public function getWorkingDir()
    { // {{{
        return $this->_workingDir;
    } // }}}

    /**
     * Delete all files in working dir
     *
     * @return void
     */
    public function deleteAllFiles()
    { // {{{
        func_rm_dir($this->_workingDir, true);
    } // }}}

    /**
     * Check working dir
     *
     * @global type $top_message
     * @global type $export_dir
     *
     * @return boolean
     */
    protected function checkDir()
    { // {{{
        global $top_message, $export_dir;

        $export_dir = $this->_workingDir;

        // Create directory if it does not exist
        if (!is_dir($export_dir)) {
            func_mkdir($export_dir);
        }

        // Check if it is writable
        if (!func_export_dir_is_writable()) {

            x_session_register('top_message');

            $top_message['content'] = func_get_langvar_by_name('err_pitney_bowes_dir_permission_denied',  array('path' => $export_dir));
            $top_message['type'] = 'E';

            return false;
        }

        return true;
    } // }}}

} // }}}

class XCPitneyBowesExporter extends XCPitneyBowesExportImportProcessor { // {{{

    protected function defineDir()
    { // {{{
        global $var_dirs;

        $this->_workingDir = $var_dirs['var'] . XC_DS . self::EXPORT_DIR_NAME;

        if (!$this->checkDir()) {
            func_pitney_bowes_debug_log(func_get_langvar_by_name('msg_adm_incorrect_store_perms', array('path' => $this->_workingDir)));
            return false;
        }
    } // }}}

    protected function defineFiles()
    { // {{{
        $this->_workingFiles = array (
            XCart\Modules\PitneyBowes\Catalog\Export\Categories::className,
            XCart\Modules\PitneyBowes\Catalog\Export\Products::className
        );
    } // }}}

    public function export()
    { // {{{

        $readyFiles = array();

        foreach ($this->_workingFiles as $object) {

            $this->_currentFile = new $object($this);

            if (!$this->_currentFile->isWorking()) {
                $this->_currentFile->save();
            }

            if (
                $this->_currentFile->isFinished()
                && !$this->_currentFile->isEmpty()
            ) {
                $readyFiles[$this->_currentFile->getFilename()] = $this->_currentFile->getRealPath();
            }
        }

        $fileExchange = new XCart\Modules\PitneyBowes\Catalog\FileExchange\Processor();

        if (!empty($readyFiles)) {
            $fileExchange->submitCatalog($readyFiles);
        }

        $fileExchange->checkNotifications();
    } // }}}

} // }}}

class XCPitneyBowesImporter extends XCPitneyBowesExportImportProcessor { // {{{

    protected function defineDir()
    { // {{{
        global $var_dirs;

        $this->_workingDir = $var_dirs['var'] . XC_DS . self::IMPORT_DIR_NAME;

        if (!$this->checkDir()) {
            func_pitney_bowes_debug_log(func_get_langvar_by_name('msg_adm_incorrect_store_perms', array('path' => $this->_workingDir)));
            return false;
        }
    } // }}}

    protected function defineFiles()
    { // {{{
        $this->_workingFiles = array (
            XCart\Modules\PitneyBowes\Catalog\Import\Eligibility::className,
        );
    } // }}}

    public function getImportDir()
    { // {{{
        return $this->_workingDir;
    } // }}}

    public function import()
    { // {{{

        foreach ($this->_workingFiles as $object) {

            $this->_currentFile = new $object($this);

            if (!$this->_currentFile->isWorking()) {
                $this->_currentFile->load();
            }

            if ($this->_currentFile->isFinished()) {
                //$this->_currentFile->delete();
            }

        }

    } // }}}

} // }}}

class XCPitneyBowesConfig extends XC_Singleton { // {{{

    private $_params = array();

    private $_shippingCode = XCPitneyBowesDefs::SHIPPING_CODE;

    public static function getInstance()
    { // {{{
        // Call parent getter
        return parent::getClassInstance(__CLASS__);
    } // }}}

    public function getCurrency()
    { // {{{
        return $this->_params['currency'];
    } // }}}

    public function getDims()
    { // {{{

        $tmp_dim = array();
        @list($tmp_dim['length'], $tmp_dim['width'], $tmp_dim['height']) = explode(':', $this->_params['param06']);
        $dim = array_map('doubleval', $tmp_dim);

        if (count(array_filter($dim)) == 3) {
            $dim['girth'] = func_girth($dim);
        } else {
            $dim['girth'] = 0;
        }

        return array($dim['length'], $dim['width'], $dim['height'], $dim['girth']); //common param06
    } // }}}

    public function getHash()
    { // {{{
        return md5(serialize($this->getConfigAsArray()));
    } // }}}

    public function getMaxWeight()
    { // {{{
        return doubleval($this->_params['param08']); // common param08
    } // }}}

    public function getSpecifiedDims()
    { // {{{
        $specified_dims = array();
        list($specified_dims['length'], $specified_dims['width'], $specified_dims['height'], $specified_dims['girth']) = $this->getDims();

        return array_filter($specified_dims);
    } // }}}

    public function useMaximumDimensions()
    { // {{{
        return $this->_params['param09']; // common param09
    } // }}}

    public function useMultiplePackages()
    { // {{{
        return $this->_params['param11']; // common param11
    } // }}}

    public function getConfigAsArray($debugPrint = XCPitneyBowesDefs::NO)
    { // {{{
        $this->_params['print_debug'] = ($debugPrint == XCPitneyBowesDefs::YES);

        return $this->_params;
    } // }}}

    public function getConfigAsObject($debugPrint = XCPitneyBowesDefs::NO)
    { // {{{
        static $configObject = null;

        if (is_null($configObject)) {
            $configObject = new stdClass();

            foreach ($this->_params as $key => $value) {
                $configObject->$key = $value;
            }
        }

        $configObject->print_debug = ($debugPrint == XCPitneyBowesDefs::YES);

        return $configObject;
    } // }}}

    public function getShippingParamsArray()
    { // {{{
        global $sql_tbl;
        // Get all shipping params
        $params = func_query_first("SELECT * FROM $sql_tbl[shipping_options] WHERE carrier='$this->_shippingCode'");
        // Complex params
        $_params = @unserialize($params['param00']);
        if (!empty($_params)) {
            unset($params['param00']);
            $params = array_merge($params, $_params);
        }
        return $params;
    } // }}}

    public function getShippingParamsObject()
    { // {{{
        $configParams = new stdClass();

        foreach ($this->getShippingParamsArray() as $key => $value) {
            $configParams->$key = $value;
        }

        return $configParams;
    } // }}}

    protected function __construct()
    { // {{{
        // Call parent constructor
        parent::__construct();

        // Globals
        global $config;

        // Get all shipping params
        $this->_params = $this->getShippingParamsArray();

        // Get module config params
        if (!empty($config[PITNEY_BOWES])) {
            foreach ($config[PITNEY_BOWES] as $p_key => $_p_value) {
                if (strrpos($p_key, 'pb_', -strlen($p_key)) !== FALSE) {
                    $this->_params[substr($p_key, 3)] = $_p_value;
                }
            }
        }

        // Get extra config params
        $this->_params['token_endpoint']       = $config['pb_token_endpoint'];
        $this->_params['checkout_endpoint']    = $config['pb_checkout_endpoint'];
        $this->_params['asn_endpoint']         = $config['pb_asn_endpoint'];

        $this->_params['sftp_endpoint']        = $config['pb_sftp_endpoint'];
        $this->_params['sftp_port']            = $config['pb_sftp_port'];

        $this->_params['records_per_file']     = $config['pb_records_per_file'];
        $this->_params['credentials_request']  = $config['pb_credentials_request'];

        $this->_params['print_debug']          = false;
    } // }}}

} // }}}

class XCPitneyBowesRate { // {{{

    protected $_baseRate;
    protected $_baseRateParts;
    protected $_markupRate;
    protected $_extraData;

    public $methodSubCode = XCPitneyBowesDefs::SHIPPING_SUBCODE_PB_INTERNATIONAL;

    public function setBaseRate($baseRate)
    { // {{{
        $this->_baseRate = $baseRate;
    } // }}}

    public function setBaseRateParts($baseRateParts)
    { // {{{
        $this->_baseRateParts = $baseRateParts;
    } // }}}

    public function setMarkupRate($markupRate)
    { // {{{
        $this->_markupRate = $markupRate;
    } // }}}

    public function setExtraData($extraData)
    { // {{{
        $this->_extraData = $extraData;
    } // }}}

    public function getRate()
    { // {{{
        return $this->_baseRate + $this->_markupRate;
    } // }}}

    public function getBaseRateParts()
    { // {{{
        return $this->_baseRateParts;
    } // }}}

    public function getTransitTime()
    {
        $transitTime = '';
        if (
            !empty($this->_extraData)
            && property_exists($this->_extraData, 'deliveryMinDays')
            && property_exists($this->_extraData, 'deliveryMaxDays')
        ) {
            $transitTime = $this->_extraData->deliveryMinDays
                    . '-'
                    . $this->_extraData->deliveryMaxDays
                    . ' days';
        }
        return $transitTime;
    }

} // }}}
