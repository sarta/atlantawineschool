<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/* * ***************************************************************************\
  +-----------------------------------------------------------------------------+
  | X-Cart Software license agreement                                           |
  | Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
  | All rights reserved.                                                        |
  +-----------------------------------------------------------------------------+
  | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
  | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
  | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
  |                                                                             |
  | THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
  | SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
  | (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
  | FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
  | THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
  | LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
  | INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
  | (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
  | LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
  | NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
  | PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
  | THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
  | SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
  | GRANTED BY THIS AGREEMENT.                                                  |
  +-----------------------------------------------------------------------------+
  \**************************************************************************** */

/**
 * Classes
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Michael Bugrov
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v3 (xcart_4_7_5), 2016-02-18 13:44:28, Eligibility.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace XCart\Modules\PitneyBowes\Catalog\Import;

/**
 * @see https://wiki.ecommerce.pb.com/display/TECH4/Catalog
 */
class Eligibility extends File { // {{{

    const className = __CLASS__;

    protected function defineFeedName()
    { // {{{
        return self::PITNEY_BOWES_FEED_NAME_ELIGIBILITY;
    } // }}}

    protected function defineOperation()
    { // {{{
        return self::PITNEY_BOWES_OPERATION_UPDATE;
    } // }}}

    protected function defineColumns()
    { // {{{
        $columns = array(
            'MERCHANT_COMMODITY_REF_ID' => array(),
            'COUNTRY_CODE'              => array(),
            'RESTRICTION_CODE'          => array(),
        );

        return $columns;
    } // }}}

    protected function defineDataset()
    { // {{{
        return array (
            self::DATA_SOURCE =>
                'product_pb_restrictions',
        );
    } // }}}

    // {{{ Setters and formatters

    /**
     * Set column value for 'MERCHANT_COMMODITY_REF_ID' column
     *
     * @param array   $record  Record
     * @param array   $line    Line
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function setMERCHANT_COMMODITY_REF_IDColumnValue(array &$record, array $line, $name, $info)
    { // {{{
        global $sql_tbl;

        $record['productid'] = $productid = intval($line[$name]);
        $product_exists = func_query_first_cell("SELECT COUNT(productid) FROM $sql_tbl[products] WHERE productid = '$productid'");

        return $product_exists > 0 ? $record['productid'] : false;
    } // }}}

    /**
     * Set column value for 'COUNTRY_CODE' column
     *
     * @param array   $record  Record
     * @param array   $line    Line
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function setCOUNTRY_CODEColumnValue(array &$record, array $line, $name, $info)
    { // {{{
        return $record['country'] = func_pitney_bowes_get_country($line[$name]);
    } // }}}

    /**
     * Set column value for 'RESTRICTION_CODE' column
     *
     * @param array   $record  Record
     * @param array   $line    Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function setRESTRICTION_CODEColumnValue(array &$record, array $line, $name, $info)
    { // {{{
        return $record['restriction_code'] = $line[$name];
    } // }}}

    // }}} Getters and formatters

} // }}}
