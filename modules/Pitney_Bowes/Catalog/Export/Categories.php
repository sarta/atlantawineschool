<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/* * ***************************************************************************\
  +-----------------------------------------------------------------------------+
  | X-Cart Software license agreement                                           |
  | Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
  | All rights reserved.                                                        |
  +-----------------------------------------------------------------------------+
  | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
  | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
  | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
  |                                                                             |
  | THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
  | SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
  | (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
  | FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
  | THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
  | LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
  | INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
  | (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
  | LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
  | NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
  | PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
  | THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
  | SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
  | GRANTED BY THIS AGREEMENT.                                                  |
  +-----------------------------------------------------------------------------+
  \**************************************************************************** */

/**
 * Classes
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Michael Bugrov
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v3 (xcart_4_7_5), 2016-02-18 13:44:28, Categories.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace XCart\Modules\PitneyBowes\Catalog\Export;

/**
 * @see https://wiki.ecommerce.pb.com/display/TECH4/Catalog
 */
class Categories extends \XCart\Modules\PitneyBowes\Catalog\File { // {{{

    const className = __CLASS__;

    protected function defineFeedName()
    { // {{{
        return self::PITNEY_BOWES_FEED_NAME_CATEGORIES;
    } // }}}

    protected function defineOperation()
    { // {{{
        return self::PITNEY_BOWES_OPERATION_UPDATE;
    } // }}}

    protected function defineColumns()
    { // {{{
        $columns = array(
            'CATEGORY_ID'           => array(),
            'PARENT_CATEGORY_ID'    => array(),
            'NAME'                  => array(),
            'ID_PATH'               => array(),
            'URL'                   => array(),
        );

        return $columns;
    } // }}}

    protected function defineDataset()
    { // {{{
        global $sql_tbl;

        return array (
            self::DATA_SOURCE =>
                "$sql_tbl[categories] as CATEGORY",
            self::DATA_FILTER =>
                "CATEGORY.avail = '" . \XCPitneyBowesDefs::YES . "'"
        );
    } // }}}

    /**
     * Get records from dataset
     *
     * @return integer
     */
    protected function getRecords()
    { // {{{
        $result = parent::getRecords();

        if ($this->_position == 0) {
            // Prepend catalog root
            array_unshift($result, array(
                'categoryid' => '0',
                'category' => 'Catalog',
                'parentid' => '',
            ));
        }

        return $result;
    } // }}}

    // {{{ Getters and formatters

    /**
     * Get column value for 'CATEGORY_ID' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getCATEGORY_IDColumnValue(array $dataset, $name, $info)
    { // {{{
        return $dataset['categoryid'];
    } // }}}

    /**
     * Get column value for 'PARENT_CATEGORY_ID' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getPARENT_CATEGORY_IDColumnValue(array $dataset, $name, $info)
    { // {{{
        return $dataset['parentid'];
    } // }}}

    /**
     * Get column value for 'NAME' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getNAMEColumnValue(array $dataset, $name, $info)
    { // {{{
        return $dataset['category'];
    } // }}}

    /**
     * Get column value for 'ID_PATH' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getID_PATHColumnValue(array $dataset, $name, $info)
    { // {{{
        return func_get_category_path($dataset['categoryid'], 'categoryid', true, ':');
    } // }}}

    /**
     * Get column value for 'URL' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getURLColumnValue(array $dataset, $name, $info)
    { // {{{
        return func_get_resource_url('category', $dataset['categoryid']);
    } // }}}

    // }}} Getters and formatters

} // }}}
