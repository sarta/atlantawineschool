<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/* * ***************************************************************************\
  +-----------------------------------------------------------------------------+
  | X-Cart Software license agreement                                           |
  | Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
  | All rights reserved.                                                        |
  +-----------------------------------------------------------------------------+
  | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
  | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
  | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
  |                                                                             |
  | THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
  | SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
  | (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
  | FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
  | THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
  | LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
  | INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
  | (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
  | LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
  | NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
  | PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
  | THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
  | SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
  | GRANTED BY THIS AGREEMENT.                                                  |
  +-----------------------------------------------------------------------------+
  \**************************************************************************** */

/**
 * Classes
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Michael Bugrov
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v6 (xcart_4_7_5), 2016-02-18 13:44:28, File.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace XCart\Modules\PitneyBowes\Catalog;

/**
 * @see https://wiki.ecommerce.pb.com/display/TECH4/Catalog
 */
abstract class File { // {{{

    const PITNEY_BOWES_RECIPIENT_ID = '16061';

    const PITNEY_BOWES_FEED_NAME_PRODUCTS       = 'catalog';
    const PITNEY_BOWES_FEED_NAME_CATEGORIES     = 'category-tree';
    const PITNEY_BOWES_FEED_NAME_ELIGIBILITY    = 'commodity-eligibility';
    const PITNEY_BOWES_FEED_NAME_SHIPPING       = 'shipping-details';

    const PITNEY_BOWES_OPERATION_UPDATE = 'update';
    const PITNEY_BOWES_OPERATION_DELETE = 'delete';

    const CSV_DELIMITER = ',';
    const CSV_ENCLOSURE = '"';

    const DATA_SOURCE = 'Source';
    const DATA_FILTER = 'Filter';

    const FILE_STATUS_NEW = 'New';
    const FILE_STATUS_WORKING = 'Working';
    const FILE_STATUS_FINISHED = 'Finished';

    const STEP_NAME_EXPORT = 'Export';
    const STEP_NAME_IMPORT = 'Import';
    const STEP_NAME_FINALIZE = 'Finalize';

    const RECORDS_PER_STEP = 10;

    protected $_config;
    /**
     * @var \XCPitneyBowesExportImportProcessor
     */
    protected $_processor;

    protected $_feedname;
    protected $_operation;
    protected $_columns;

    protected $_filename;
    protected $_filepath;
    protected $_filePointer;

    protected $_state = self::FILE_STATUS_NEW;

    protected $_count = 0;
    protected $_position = 0;
    protected $_step = self::RECORDS_PER_STEP;

    protected $_start_time = XC_TIME;
    protected $_step_time = 0;

    protected $_pid;

    protected $_step_name = self::STEP_NAME_EXPORT;
    protected $_step_progress = 0;

    protected $_dataset = array (
        self::DATA_SOURCE => false,
        self::DATA_FILTER => false
    );

    protected $_fieldset = '*';

    public function __construct(\XCPitneyBowesExportImportProcessor $processor)
    { // {{{
        $this->_processor   = $processor;
        $this->_config      = $processor->getConfig();

        $this->_filepath    = $processor->getWorkingDir();

        $this->_feedname    = $this->defineFeedName();
        $this->_operation   = $this->defineOperation();
        $this->_columns     = $this->defineColumns();

        $this->_dataset     = $this->defineDataset();
        $this->_fieldset    = $this->defineFieldset();

        // Register cache function
        func_register_cache_function(
            'getPitneyBowesFileCacheClassState',
            array(
                'class' => get_class($this),
            )
        );

        // Get cache data
        $this->getPitneyBowesFileCacheClassState();
    } // }}}

    public function save()
    { // {{{

        $this->_filePointer = fopen($this->getRealPath(), 'a');

        if (!$this->_filePointer) {
            func_pitney_bowes_debug_log(func_get_langvar_by_name('err_pitney_bowes_cannot_open_export_file', array('file' => $this->getRealPath())));
            return false;
        }

        if ($this->_state == self::FILE_STATUS_NEW) {
            $this->_count = $this->getRecordsCount();
            $this->_pid = $this->getPid();
        }

        $loop_guard = 0;
        while (
            $this->_position < $this->_count
            && $loop_guard < $this->_count
        ) {

            if ($this->_position == 0) {
                $this->_state = self::FILE_STATUS_WORKING;
                $this->wrightExportHeader();
            }

            $records = $this->getRecords();

            if (!empty($records)) {
                foreach ($records as $record) {
                    $this->wrightExportLine($record);
                    $this->_position++;
                }
            }
            $loop_guard += max(1, count($records));

            func_flush('.');

            if ($this->_step_time === 0) {
                $this->_step_time = time() - $this->_start_time;
            }

            $this->setCacheClassState();

            $this->_step_progress = ($this->_position / $this->_count) * 100;
        }

        fclose($this->_filePointer);

        if ($this->_position == $this->_count) {
            $this->_state = self::FILE_STATUS_FINISHED;
            $this->setCacheClassState();

            if (method_exists($this, 'onFinished')) {
                $this->onFinished();
            }
        }

    } // }}}

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    { // {{{

        if (empty($this->_filename)) {
            $parts = array(
                'Sender_ID'         => $this->_config->sender_id,
                'Data_Feed_Name'    => $this->_feedname,
                'Operation'         => $this->_operation,
                'Recipient_ID'      => static::PITNEY_BOWES_RECIPIENT_ID,
                'UTC_Date_Time'     => date('Ymd_His'),
                'Random_6_Digits'   => str_pad(rand(0, pow(10, 6)), 6, '0', STR_PAD_LEFT),
            );
            $this->_filename = implode('_', $parts) . '.csv';
        }

        return $this->_filename;
    } // }}}

    public function getRealPath()
    { // {{{
        return $this->_filepath . XC_DS . $this->getFilename();
    } // }}}

    protected function getLinesCount($filepath)
    { // {{{
        $count = 0;

        $filePointer = fopen($filepath, 'r');
        while (($data = fgetcsv($filePointer, 0, self::CSV_DELIMITER, self::CSV_ENCLOSURE)) !== FALSE) {
            $count++;
        }
        fclose($filePointer);

        return $count;
    } // }}}

    public function isWorking()
    { // {{{
        return $this->_state == self::FILE_STATUS_WORKING
                && (function_exists('posix_getpgid') ? posix_getpgid($this->_pid) : true);
    } // }}}

    public function isFinished()
    { // {{{
        return $this->_state == self::FILE_STATUS_FINISHED;
    } // }}}

    public function isEmpty()
    { // {{{
        return !file_exists($this->getRealPath()) || filesize($this->getRealPath()) == 0;
    } // }}}

    protected function getPid()
    { // {{{
        return function_exists('posix_getpid') ? posix_getpid() : null;
    } // }}}

    protected function getCacheKey()
    { // {{{
        return get_class($this);
    } // }}}

    /**
     * Get cache class state
     */
    protected function getPitneyBowesFileCacheClassState()
    { // {{{
        $cacheKey = $this->getCacheKey();

        if (($cacheData = func_get_cache_func($cacheKey, 'getPitneyBowesFileCacheClassState'))) {
            if ($this->_state == self::FILE_STATUS_WORKING) {
                // restore object properties
                foreach ($cacheData['data'] as $property => $value) {
                    $this->$property = $value;
                }
            }
            return;
        }

        $this->setCacheClassState();
    } // }}}

    protected function setCacheClassState()
    { // {{{
        $cacheKey = $this->getCacheKey();

        $classState = array (
            '_filename'         => $this->_filename,
            '_filepath'         => $this->_filepath,
            '_count'            => $this->_count,
            '_position'         => $this->_position,
            '_step'             => $this->_step,
            '_state'            => $this->_state,
            '_start_time'       => $this->_start_time,
            '_step_time'        => $this->_step_time,
            '_pid'              => $this->_pid,
            '_step_name'        => $this->_step_name,
            '_step_progress'    => $this->_step_progress,
        );

        func_save_cache_func($classState, $cacheKey, 'getPitneyBowesFileCacheClassState');
    } // }}}

    /**
     * Define feed name
     *
     * @return string
     */
    abstract protected function defineFeedName();

    /**
     * Define operation
     *
     * @return string
     */
    abstract protected function defineOperation();

    /**
     * Define columns
     *
     * @return array
     *
     * @see https://wiki.ecommerce.pb.com/display/TECH4/Catalog+Specifications
     */
    abstract protected function defineColumns();

    /**
     * Define dataset
     *
     * @return string
     */
    abstract protected function defineDataset();

    /**
     * Define fieldset
     *
     * @return string
     */
    protected function defineFieldset()
    { // {{{
        return '*';
    } // }}}

    /**
     * Get from SQL query
     *
     * @return string
     */
    protected function getFromQuery()
    { // {{{
        $query = false;

        if (!empty($this->_dataset[self::DATA_SOURCE])) {
            $query = ' FROM ' . $this->_dataset[self::DATA_SOURCE];
        }

        if (!empty($this->_dataset[self::DATA_FILTER])) {
            $query .= ' WHERE ' . $this->_dataset[self::DATA_FILTER];
        }

        return $query;
    } // }}}

    /**
     * Get records from dataset
     *
     * @return integer
     */
    protected function getRecords()
    { // {{{
        $result = array();

        if (($fromQuery = $this->getFromQuery())) {

            $SQL = "SELECT {$this->_fieldset} {$fromQuery} LIMIT {$this->_position}, {$this->_step}";

            $result = func_query($SQL);
        }

        return $result;
    } // }}}

    /**
     * Get records count in dataset
     *
     * @return integer
     */
    protected function getRecordsCount()
    { // {{{
        $result = 0;

        if (($fromQuery = $this->getFromQuery())) {

            $SQL = "SELECT COUNT(*) {$fromQuery}";

            $result = func_query_first_cell($SQL);
        }

        return $result;
    } // }}}

    /**
     * Wright export HEADER to file
     *
     * @return mixed
     */
    protected function wrightExportHeader()
    { // {{{
        $result = false;

        $columns = array_keys($this->_columns);
        if (!empty($columns)) {
            $result = fputcsv($this->_filePointer, array_keys($this->defineColumns()), self::CSV_DELIMITER, self::CSV_ENCLOSURE);
        }

        return $result;
    } // }}}

    /**
     * Wright export LINE to file
     *
     * @return mixed
     */
    protected function wrightExportLine(array $dataset)
    { // {{{
        $result = false;

        $line = array();

        if (!empty($this->_columns)) {
            // Process columns
            foreach ($this->_columns as $column => $info) {
                $method_name = "get{$column}ColumnValue";

                if (method_exists($this, $method_name)) {
                    $result = $this->{$method_name}($dataset, $column, $info);
                }

                $line[$column] = !is_null($result) ? $result : '';
            }
            // Put CSV line
            $result = fputcsv($this->_filePointer, $line, self::CSV_DELIMITER, self::CSV_ENCLOSURE) !== false;
        }

        return $result;
    } // }}}

} // }}}
