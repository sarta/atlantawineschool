<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart Software license agreement                                           |
| Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
| SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
| (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
| FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
| THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
| LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
| INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
| (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
| LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
| NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
| PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
| THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
| SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
| GRANTED BY THIS AGREEMENT.                                                  |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

/**
 * Shipping options
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Michael Bugrov
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v3 (xcart_4_7_5), 2016-02-18 13:44:28, shipping_options.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

if ($REQUEST_METHOD == 'POST') {

    // PB options update

    settype($currency, 'string');

    $free_transportation_countries_store = array_filter(explode(';', $free_transportation_countries_store));
    if (!empty($free_transportation_countries_store)) {
        $free_transportation_countries_store = array_fill_keys($free_transportation_countries_store, XCPitneyBowesDefs::YES);
    }

    $free_importation_countries_store = array_filter(explode(';', $free_importation_countries_store));
    if (!empty($free_importation_countries_store)) {
        $free_importation_countries_store = array_fill_keys($free_importation_countries_store, XCPitneyBowesDefs::YES);
    }

    $_params = addslashes(
        serialize(
            array (
                'currency' => $currency,

                'free_transportation' => !empty($free_transportation) ? $free_transportation : '',
                'free_transportation_threshold' => $free_transportation_threshold,
                'free_transportation_countries' => $free_transportation_countries_store,

                'free_importation' => !empty($free_importation) ? $free_importation : '',
                'free_importation_threshold' => $free_importation_threshold,
                'free_importation_countries' => $free_importation_countries_store,

                'shipping_fee_markup' => $shipping_fee_markup,
                'shipping_fee_markup_basis' => $shipping_fee_markup_basis,

                'handling_fee_markup' => $handling_fee_markup,
                'handling_fee_markup_basis' => $handling_fee_markup_basis,

                'min_delivery_adjustment' => $min_delivery_adjustment,
                'max_delivery_adjustment' => $max_delivery_adjustment,

                'suppress_leg_tracking' => !empty($suppress_leg_tracking) ? $suppress_leg_tracking : '',
            )
        )
    );

    settype($param01, 'string'); //param01

    $param06 = abs(doubleval($dim_length)) . ':' . abs(doubleval($dim_width)) . ':' . abs(doubleval($dim_height)); // param06

    settype($use_maximum_dimensions, 'string'); // param09
    settype($param11, 'string'); //param11

    $shippingOptions = array(
        'param00' => $_params,
        'param01' => $param01, //common
        'param02' => '',
        'param03' => '',
        'param04' => '',
        'param05' => '',
        'param06' => $param06, //common
        'param07' => '',
        'param08' => abs(doubleval($max_weight)), // common
        'param09' => ('Y' !== $use_maximum_dimensions) ? 'N' : $use_maximum_dimensions, //common
        'param10' => '',
        'param11' => empty($param11) ? 'N' : $param11, //common: Split the shipment into multiple packages if...
    );

} else {

    // PB options display

    $_dim = explode(':', $shipping_options[$lower_carrier]['param06']);

    if (func_array_empty($_dim)) {
        $_dim = array_fill(0, 3, 0);
    }

    $shipping_options[$lower_carrier]['dim_length'] = $_dim[0];
    $shipping_options[$lower_carrier]['dim_width'] = $_dim[1];
    $shipping_options[$lower_carrier]['dim_height'] = $_dim[2];

    $_params = @unserialize($shipping_options[$lower_carrier]['param00']);

    if (!empty($_params)) {
        foreach ($_params as $key => $param) {
            $shipping_options[$lower_carrier][$key] = $param;
        }
    }

    $smarty->assign('pb_currencies', func_pitney_bowes_get_all_currencies());
    $smarty->assign('pb_countries', func_pitney_bowes_get_all_countries());
}
