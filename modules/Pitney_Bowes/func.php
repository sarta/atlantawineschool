<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart Software license agreement                                           |
| Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
| SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
| (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
| FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
| THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
| LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
| INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
| (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
| LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
| NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
| PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
| THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
| SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
| GRANTED BY THIS AGREEMENT.                                                  |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

/**
 * Functions
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Michael Bugrov
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v17 (xcart_4_7_5), 2016-02-18 13:44:28, func.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

if ( !defined('XCART_START') ) { header("Location: ../../"); die("Access denied"); }

/**
 * Initialize module
 */
function func_pitney_bowes_init()
{ // {{{
    global $smarty, $active_modules;

    // check configuration compability issues
    if (
        func_constant('AREA_TYPE') == 'C'
        && ($pitney_bowes_message = func_pitney_bowes_has_compability_issues())
        && !empty($pitney_bowes_message)
    ) {
        // disable module for customers
        unset($active_modules[PITNEY_BOWES]);
        $smarty->assign('active_modules', $active_modules);

        return;
    }

    // load classes
    func_pitney_bowes_load_classes();

    // enable cron tasks
    func_pitney_bowes_add_cron_task();

    if (defined('ADMIN_MODULES_CONTROLLER')) {
        // Register module toggle handler
        if (function_exists('func_add_event_listener')) {
            func_add_event_listener('module.ajax.toggle', 'func_pitney_bowes_on_module_toggle');
        }
    }

    if (defined('ADMIN_CONFIGURATION_CONTROLLER')) {
        // Register module config update handler
        if (function_exists('func_add_event_listener')) {
            func_add_event_listener('module.config.update', 'XCPitneyBowesAdminRequestProcessor::processRequest');
        }
    }

    // register functions
    $smarty->registerPlugin('function', 'pb_transportation_part', 'func_pitney_bowes_spbtp');
    $smarty->registerPlugin('function', 'pb_importation_part', 'func_pitney_bowes_spbip');
    $smarty->registerPlugin('function', 'pb_track_url', 'func_pitney_bowes_gptru');

    // allow static class const access in smarty
    $smarty->changeSecurity(array('static_classes' => array('XCPitneyBowesDefs')));
} // }}}

function func_pitney_bowes_load_classes()
{ // {{{
    global $xcart_dir;

    if (!function_exists('func_pitney_bowes_load_class')) {
        function func_pitney_bowes_load_class($class_name) {
            global $xcart_dir;

            static $pbClasses = array(
                'XCStringUtils' => '../../include/classes/class.XCStringUtils.php',
                'PEAR2\HTTP\Request' => '../../include/lib/PEAR2/Autoload.php',

                'XCart\Modules\PitneyBowes\Barcode\Printer'  => 'Barcode/Printer.php',

                'XCart\Modules\PitneyBowes\Catalog\File'  => 'Catalog/File.php',
                'XCart\Modules\PitneyBowes\Catalog\Export\Categories' => 'Catalog/Export/Categories.php',
                'XCart\Modules\PitneyBowes\Catalog\Export\Products'   => 'Catalog/Export/Products.php',
                'XCart\Modules\PitneyBowes\Catalog\FileExchange\Processor'    => 'Catalog/FileExchange/Processor.php',
                'XCart\Modules\PitneyBowes\Catalog\Import\File'   => 'Catalog/Import/File.php',
                'XCart\Modules\PitneyBowes\Catalog\Import\Eligibility'    => 'Catalog/Import/Eligibility.php',

                'XCart\Modules\PitneyBowes\HTTP\Request'  => 'HTTP/Request.php',
                'XCart\Modules\PitneyBowes\HTTP\Adapter\Curl' => 'HTTP/Adapter/Curl.php',

                'XCart\Modules\PitneyBowes\Shipping\API\DataTypes\AResponse'  => 'Shipping/API/DataTypes/AResponse.php',
                'XCart\Modules\PitneyBowes\Shipping\API\DataTypes\Basket' => 'Shipping/API/DataTypes/Basket.php',
                'XCart\Modules\PitneyBowes\Shipping\API\DataTypes\QuoteSet'   => 'Shipping/API/DataTypes/QuoteSet.php',
                'XCart\Modules\PitneyBowes\Shipping\API\DataTypes\QuoteSet'   => 'Shipping/API/Mapper/IMapper.php',

                'XCart\Modules\PitneyBowes\Shipping\API\Mapper\AMapper'   => 'Shipping/API/Mapper/AMapper.php',
                'XCart\Modules\PitneyBowes\Shipping\API\Mapper\IMapper'   => 'Shipping/API/Mapper/IMapper.php',
                'XCart\Modules\PitneyBowes\Shipping\API\Mapper\AuthTokenMapper'   => 'Shipping/API/Mapper/AuthTokenMapper.php',
                'XCart\Modules\PitneyBowes\Shipping\API\Mapper\JsonPostProcessedMapper'   => 'Shipping/API/Mapper/JsonPostProcessedMapper.php',

                'XCart\Modules\PitneyBowes\Shipping\API\Request\ARequest' => 'Shipping/API/Request/ARequest.php',
                'XCart\Modules\PitneyBowes\Shipping\API\Request\AuthTokenRequest' => 'Shipping/API/Request/AuthTokenRequest.php',
                'XCart\Modules\PitneyBowes\Shipping\API\Request\ConfirmOrderRequest'  => 'Shipping/API/Request/ConfirmOrderRequest.php',
                'XCart\Modules\PitneyBowes\Shipping\API\Request\CreateInboundParcelsRequest'  => 'Shipping/API/Request/CreateInboundParcelsRequest.php',
                'XCart\Modules\PitneyBowes\Shipping\API\Request\CreateOrderRequest'   => 'Shipping/API/Request/CreateOrderRequest.php',
                'XCart\Modules\PitneyBowes\Shipping\API\Request\GetQuoteRequest'  => 'Shipping/API/Request/GetQuoteRequest.php',

                'XCart\Modules\PitneyBowes\Shipping\API\PitneyBowesAPI' => 'Shipping/API/PitneyBowesAPI.php',
                'XCart\Modules\PitneyBowes\Shipping\Processor\PitneyBowes' => 'Shipping/Processor/PitneyBowes.php',

                'XCart\Modules\PitneyBowes\Shipping\Mapper\GetQuote\OrderInputMapper'  => 'Shipping/Mapper/GetQuote/OrderInputMapper.php',
                'XCart\Modules\PitneyBowes\Shipping\Mapper\GetQuote\QuoteInputMapper' => 'Shipping/Mapper/GetQuote/QuoteInputMapper.php',
                'XCart\Modules\PitneyBowes\Shipping\Mapper\GetQuote\OutputMapper' => 'Shipping/Mapper/GetQuote/OutputMapper.php',
                'XCart\Modules\PitneyBowes\Shipping\Mapper\ConfirmOrder\InputMapper'  => 'Shipping/Mapper/ConfirmOrder/InputMapper.php',
                'XCart\Modules\PitneyBowes\Shipping\Mapper\ConfirmOrder\OutputMapper' => 'Shipping/Mapper/ConfirmOrder/OutputMapper.php',
                'XCart\Modules\PitneyBowes\Shipping\Mapper\CreateInboundParcels\InputMapper'  => 'Shipping/Mapper/CreateInboundParcels/InputMapper.php',
                'XCart\Modules\PitneyBowes\Shipping\Mapper\CreateInboundParcels\OutputMapper' => 'Shipping/Mapper/CreateInboundParcels/OutputMapper.php',
                'XCart\Modules\PitneyBowes\Shipping\Mapper\CreateOrder\OrderInputMapper'   => 'Shipping/Mapper/CreateOrder/OrderInputMapper.php',
                'XCart\Modules\PitneyBowes\Shipping\Mapper\CreateOrder\OutputMapper'  => 'Shipping/Mapper/CreateOrder/OutputMapper.php',
            );

            if (
                isset($pbClasses[$class_name])
            ) {
                include $xcart_dir . XC_DS . 'modules' . XC_DS . PITNEY_BOWES . XC_DS . $pbClasses[$class_name];
            }
        }
        // register class loader
        spl_autoload_register('func_pitney_bowes_load_class');
    }

    // load basic classes
    require_once $xcart_dir . XC_DS . 'modules' . XC_DS . PITNEY_BOWES . XC_DS . 'classes.php';
} // }}}

function func_pitney_bowes_add_cron_task()
{ // {{{
    global $cron_tasks;

    $cron_tasks[] = array(
        'function' => 'func_pitney_bowes_periodic_catalog_update'
    );
} // }}}

function func_pitney_bowes_has_compability_issues()
{ // {{{

    if (!function_exists('func_get_default_fields')) {
        x_load('user');
    }

    if (
        // Customer
        (
            ($c_area_fields = func_get_default_fields('C', 'address_book'))
            && empty($c_area_fields['phone']['required'])
        )
        // Customer at checkout
        || (
            ($h_area_fields = func_get_default_fields('H', 'address_book'))
            && empty($h_area_fields['phone']['required'])
        )
    ) {
        return func_get_langvar_by_name('msg_adm_pitney_bowes_require_phone_number');
    }

    return false;
} // }}}

function func_pitney_bowes_add_avail_sections($avail_sections) { // {{{

    $avail_sections[] = 'product_restrictions';

    return $avail_sections;
} //}}}

function func_pitney_bowes_add_dialog_tools_data($dialog_tools_data, $pm_link) { // {{{

    $dialog_tools_data['left'][] = array(
        'link'  => $pm_link . '&section=product_restrictions',
        'title' => func_get_langvar_by_name('lbl_pitney_bowes_product_restrictions', null, false, true, false)
    );

    return $dialog_tools_data;
} //}}}

function func_pitney_bowes_get_product_restrictions($productid) { // {{{
    global $sql_tbl;

    $result = func_query("SELECT * FROM $sql_tbl[product_pb_restrictions] WHERE productid='$productid'");

    return !empty($result) ? $result : array();
} //}}}

function func_pitney_bowes_add_items_info_to_package(&$package_ref, $item)
{ // {{{
    // Add current item to package box items list
    if (
        !empty($package_ref['items_list'])
        && !empty($package_ref['items_count'])
    ) {
        // Append item to list
        $package_ref['items_list'][] = array (
            // Required info (cartid, productid, productcode, amount, price)
            'cartid'    => $item['cartid'],
            'productid' => $item['productid'],
            'productcode'   => $item['productcode'],
            'amount'    => $item['amount'],
            'price'     => $item['price'],
        );
        // Increase counter
        $package_ref['items_count'] += $item['amount'];
    } else {
        // The list is empty, create it
        $package_ref['items_list'] = array (
            // Required info (cartid, productid, productcode, amount, price)
            array (
                'cartid'    => $item['cartid'],
                'productid' => $item['productid'],
                'productcode' => $item['productcode'],
                'amount'    => $item['amount'],
                'price'     => $item['price'],
            )
        );
        // Collect items amount
        $package_ref['items_count'] = $item['amount'];
    }
} // }}}

function func_pitney_bowes_on_module_toggle($module_name, $module_new_state)
{ // {{{
    if ($module_name == PITNEY_BOWES) {
        switch ($module_new_state) {
            case true:
                func_pitney_bowes_prepare_records();
        }
    }
} // }}}

function func_pitney_bowes_spbtp($param)
{ // {{{
    $transportation = 0;

    $orders = func_pitney_bowes_get_cart_transaction_orders($param['cart']);

    $config = $param['cart']['orderid'] ? $param['cart']['extra'][PITNEY_BOWES] : false;

    foreach ($orders as $order) {
        $transportation += XCart\Modules\PitneyBowes\Shipping\Processor\PitneyBowes::getTransportationPart(
            $param['cart']['subtotal'],
            $param['country'],
            $order->order,
            $config
        );
    }

    return $transportation;
} // }}}

function func_pitney_bowes_spbip($param)
{ // {{{
    $importation = 0;

    $orders = func_pitney_bowes_get_cart_transaction_orders($param['cart']);

    $config = $param['cart']['orderid'] ? $param['cart']['extra'][PITNEY_BOWES] : false;

    foreach ($orders as $order) {
        $importation += XCart\Modules\PitneyBowes\Shipping\Processor\PitneyBowes::getImportationPart(
            $param['cart']['subtotal'],
            $param['country'],
            $order->order,
            $config
        );
    }

    return $importation;
} // }}}

function func_pitney_bowes_gptru($param)
{
    return \XCart\Modules\PitneyBowes\Shipping\Processor\PitneyBowes::getTrackingInformationURL($param['tracking_number']);
}

function func_pitney_bowes_get_cart_transactionid()
{
    global $cart;

    return !empty($cart[\XCPitneyBowesDefs::CART_TRANSACTIONID])
        ? $cart[\XCPitneyBowesDefs::CART_TRANSACTIONID]
        : false;
}

function func_pitney_bowes_is_selected_on_checkout($userinfo)
{ // {{{
    global $sql_tbl, $cart;

    static $pb_shippingid = null;

    $code = XCPitneyBowesDefs::SHIPPING_CODE;
    $subcode = XCPitneyBowesDefs::SHIPPING_SUBCODE_PB_INTERNATIONAL;

    if (is_null($pb_shippingid)) {
        $pb_shippingid = func_query_first_cell(
            "SELECT shippingid"
            . " FROM $sql_tbl[shipping]"
            . " WHERE code='$code'"
                . " AND subcode='$subcode'"
        );
    }

    if (
        !empty($cart['shippingid'])
        && $pb_shippingid == $cart['shippingid']
    ) {
        $cart[\XCPitneyBowesDefs::CART_TRANSACTIONID] = func_pitney_bowes_create_transaction_id($cart, $userinfo);
        $cart[\XCPitneyBowesDefs::CART_ORDERS] = array();
    } else {
        if (is_array($cart)) {
            unset($cart[\XCPitneyBowesDefs::CART_TRANSACTIONID]);
            unset($cart[\XCPitneyBowesDefs::CART_ORDERS]);
        }
    }

    return !empty($cart[\XCPitneyBowesDefs::CART_TRANSACTIONID]);
} // }}}

function func_pitney_bowes_create_transaction_id($cart, $userinfo)
{ // {{{
    global $XCARTSESSID;

    // Config hash
    $configHash = XCPitneyBowesConfig::getInstance()->getHash();
    // Serialize required cart info
    $cartSerial = serialize(
        array(
            $cart['products'],
            $cart['shippingid'],
            $userinfo
        )
    );

    return md5($XCARTSESSID . $configHash . $cartSerial);
} // }}}

function func_pitney_bowes_compact_cart_transaction_orders(&$cart_ref)
{ // {{{
    // Check registered orders
    if (!empty($cart_ref[\XCPitneyBowesDefs::CART_ORDERS])) {
        // Loop through all registered orders
        foreach ($cart_ref[\XCPitneyBowesDefs::CART_ORDERS] as $orderid => $order) {
            // Keep only orders related to current cart transaction ID
            if ($order->order->transactionId !== $cart_ref[\XCPitneyBowesDefs::CART_TRANSACTIONID]) {
                unset($cart_ref[\XCPitneyBowesDefs::CART_ORDERS][$orderid]);
            }
        }
    }
} // }}}

function func_pitney_bowes_add_cart_transaction_orders($orders)
{ // {{{
    global $cart;

    if (!empty($cart[\XCPitneyBowesDefs::CART_TRANSACTIONID])) {

        func_pitney_bowes_compact_cart_transaction_orders($cart);

        // Register new orders
        foreach ($orders as $order) {
            // Check if it has already been registered
            if (!isset($cart[\XCPitneyBowesDefs::CART_ORDERS][$order->orderId])) {
                // Register cart order
                $cart[\XCPitneyBowesDefs::CART_ORDERS][$order->orderId] = $order;
            }
        }
    }

} // }}}

function func_pitney_bowes_get_cart_transaction_orders($cart)
{ // {{{
    if (!empty($cart[\XCPitneyBowesDefs::CART_TRANSACTIONID])) {

        func_pitney_bowes_compact_cart_transaction_orders($cart);

        return $cart[\XCPitneyBowesDefs::CART_ORDERS];
    }
} // }}}

function func_pitney_bowes_load_pb_orders_data(&$order_ref)
{ // {{{
    global $sql_tbl;
    $pbOrder = func_query_first(
        "SELECT ormus_transid, createOrderResponse"
        . " FROM $sql_tbl[pb_orders]"
        . " WHERE orderid='$order_ref[orderid]'"
    );

    if (!empty($pbOrder)) {

        $asnStatus = \XCPitneyBowesDefs::ASN_STATUS_SENT;
        $pbParcels = func_query(
            "SELECT parcel_number, ormus_number, asn_number"
            . " FROM $sql_tbl[pb_parcels]"
            . " WHERE orderid='$order_ref[orderid]'"
                . " AND createAsnCalled = '$asnStatus'"
        );

        $order_ref[\XCPitneyBowesDefs::CART_TRANSACTIONID] = $pbOrder['ormus_transid'];
        $order_ref[\XCPitneyBowesDefs::CART_ORDERS] = unserialize($pbOrder['createOrderResponse']);
        $order_ref[\XCPitneyBowesDefs::ORDER_PARCELS] = $pbParcels;
    }
} // }}}

function func_pitney_bowes_save_pb_orders_data($orderid, $order_status, $cart)
{ // {{{
    if (
        ($pbOrders = func_pitney_bowes_get_cart_transaction_orders($cart))
        && !empty($pbOrders) && is_array($pbOrders)
    ) {
        $pbOrdersData = array (
            'orderid' => $orderid,
            'ormus_transid' => $cart[\XCPitneyBowesDefs::CART_TRANSACTIONID],
            'createOrderResponse' => func_addslashes(serialize($pbOrders))
        );

        $result = func_array2insert('pb_orders', $pbOrdersData);

        if ($result !== false) {
            func_pitney_bowes_create_order_history_event($orderid, $order_status, $pbOrders);
        }
    }
} // }}}

function func_pitney_bowes_set_order_extra(&$extra_ref)
{ // {{{
    $extra_ref[PITNEY_BOWES] = \XCPitneyBowesConfig::getInstance()->getShippingParamsObject();
} // }}}

/**
 * https://wiki.ecommerce.pb.com/display/TECH4/Data+Types+-+InboundParcelRequest
 * parcelIdentificationNumber - maxLength = 50
 */
function func_pitney_bowes_generate_token()
{ // {{{

    $length = XCPitneyBowesDefs::PARCEL_NUMBER_LENGTH;
    $chars = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');

    $limit = count($chars) - 1;
    $x = explode('.', uniqid('', true));
    mt_srand(microtime(true) + intval(hexdec($x[0])) + $x[1]);

    $password = '';
    for ($i = 0; $length > $i; $i++) {
        $password .= $chars[mt_rand(0, $limit)];
    }

    return $password;
} // }}}

function func_pitney_bowes_prepare_records()
{ // {{{
    global $sql_tbl;

    $count_products = func_query_first_cell(
        "SELECT COUNT(*)"
        . " FROM $sql_tbl[products]"
    );

    $count_flags = func_query_first_cell(
        "SELECT COUNT(*)"
        . " FROM $sql_tbl[product_pb_exports] AS pbe"
        . " INNER JOIN $sql_tbl[products] prd"
            . " ON pbe.productid = pbe.productid"
    );

    // make sure tables are sync
    if (intval($count_products) != intval($count_flags)) {
        func_pitney_bowes_mark_all_products_pending();
    }

} // }}}

function func_pitney_bowes_mark_all_products_pending()
{ // {{{
    global $sql_tbl;

    // Clear exports table
    db_query("DELETE FROM $sql_tbl[product_pb_exports]");

    // Mark all products
    db_query("INSERT INTO $sql_tbl[product_pb_exports]"
            . " ("
                . "SELECT "
                    . "productid, "
                    . XCPitneyBowesDefs::DATASET_STATUS_PENDING
                . " FROM $sql_tbl[products]"
            . ") "
    );
} // }}}

function func_pitney_bowes_confirm_order($order_data)
{ // {{{
    if (
        !empty($order_data['userinfo'])
        && !empty($order_data['order'][XCPitneyBowesDefs::CART_TRANSACTIONID])
        && !empty($order_data['order'][XCPitneyBowesDefs::CART_ORDERS])
    ) {

        $api = new \XCart\Modules\PitneyBowes\Shipping\API\PitneyBowesAPI(XCPitneyBowesConfig::getInstance()->getConfigAsObject());

        $pbOrders = $order_data['order'][XCPitneyBowesDefs::CART_ORDERS];

        foreach ($pbOrders as $pbOrder) {

            $pbOrderData = array();

            // Merchant order ID
            $pbOrderData['orderid'] = $order_data['order']['orderid'];
            // Merchant order status
            $pbOrderData['status'] = $order_data['order']['status'];

            // Pitney Bowes order ID
            $pbOrderData['ormus_orderid'] = $pbOrder->orderId;
            $pbOrderData['ormus_transid'] = $pbOrder->order->transactionId;

            // Billing address
            $pbOrderData['billAddress'] = func_pitney_bowes_get_billing_address($order_data['userinfo']);
            // Destination address info
            $pbOrderData['destAddress'] = func_pitney_bowes_get_destination_address($order_data['userinfo']);

            $confirmOrderResult = $api->confirmOrder($pbOrderData);

            if ($confirmOrderResult) {
                func_pitney_bowes_confirm_order_history_event($pbOrderData['orderid'], $pbOrderData['status'], $confirmOrderResult);
            }
        }
    }
} // }}}

function func_pitney_bowes_cancel_order($orderid)
{ // {{{
    // TODO implement if necessary
} // }}}

function func_pitney_bowes_create_order_history_event($orderid, $order_status, $pbOrders)
{ // {{{
    global $active_modules;
    // Store in the order history
    if (!empty($active_modules['Advanced_Order_Management'])) {

        foreach ($pbOrders as $pbOrder) {
            $details = array(
                'new_status' => $order_status,
                'comment' =>
                    "Pitney Bowes Order 'Creation':\n"
                    . "Grand total: {$pbOrder->order->total->value}\n"
                    . "Transaction ID: {$pbOrder->order->transactionId}\n"
                    . "Order ID: {$pbOrder->orderId}\n"
                    . "Total transportation: {$pbOrder->order->totalTransportation->total->value}\n"
                    . "Total importation: {$pbOrder->order->totalImportation->total->value}\n"
                    . "Expire date: {$pbOrder->expireDate}\n"
                    . "Hub address: " . func_pitney_bowes_hub_address_as_string($pbOrder->shipToHub) ."\n"
                    . "Delivery time: " . func_pitney_bowes_delivery_time_as_string($pbOrder->order) . "\n"
            );

            func_aom_save_history($orderid, XCPitneyBowesDefs::HISTORY_CODE, $details);
        }
    }
} // }}}

function func_pitney_bowes_confirm_order_history_event($orderid, $order_status, $confirmOrderResult)
{ // {{{
    global $active_modules;
    // Store in the order history
    if (!empty($active_modules['Advanced_Order_Management'])) {

        $details = array(
            'new_status' => $order_status,
            'comment' =>
                "Pitney Bowes Order 'Confirmation':\n"
                . "Transaction ID: {$confirmOrderResult->transactionId}\n"
                . "Order ID: {$confirmOrderResult->orderId}\n"
                . "Result: {$confirmOrderResult->result}\n"
        );

        func_aom_save_history($orderid, XCPitneyBowesDefs::HISTORY_CODE, $details);
    }
} // }}}

function func_pitney_bowes_hub_address_as_string($shipToHub)
{ // {{{
    $addressParts = (array) $shipToHub->hubAddress;
    $addressString = implode(' ', $addressParts);

    return $shipToHub->hubId . ' : ' . $addressString;
} // }}}

function func_pitney_bowes_delivery_time_as_string($order)
{ // {{{
    $min = $order->totalTransportation->minDays ?: '&infin;';
    $max = $order->totalTransportation->maxDays ?: '&infin;';

    return "$min - $max days";
} // }}}

function func_pitney_bowes_get_country($three_letters_code)
{ // {{{
    global $sql_tbl;

    return func_query_first_cell("SELECT code FROM $sql_tbl[countries] WHERE code_A3 = '$three_letters_code'", true);
} // }}}

function func_pitney_bowes_debug_log($log_message)
{ // {{{
    if (
        (defined('XC_PITNEY_BOWES_DEBUG') || defined('DEVELOPMENT_MODE'))
        && !empty($log_message)
    ) {
        x_log_add(PITNEY_BOWES, $log_message);
    }
} // }}}

function func_pitney_bowes_get_all_currencies()
{ // {{{
    global $sql_tbl;

    static $currencies = null;

    if (is_null($currencies)) {

        // https://wiki.ecommerce.pb.com/display/TECH4/Data+Types+-+Currency
        $supported_currencies = implode("','", array('AED','AFN','ALL','AMD','ANG','AOA','ARS','AUD','AWG','AZN','BAM','BBD','BDT','BGN','BHD','BIF','BMD','BND','BOB','BOV','BRL','BSD','BTN','BWP','BYR','BZD','CAD','CDF','CHE','CHF','CHW','CLF','CLP','CNY','COP','COU','CRC','CUC','CUP','CVE','CZK','DJF','DKK','DOP','DZD','EGP','ERN','ETB','EUR','FJD','FKP','GBP','GEL','GHS','GIP','GMD','GNF','GTQ','GYD','HKD','HNL','HRK','HTG','HUF','IDR','ILS','INR','IQD','IRR','ISK','JMD','JOD','JPY','KES','KGS','KHR','KMF','KPW','KRW','KWD','KYD','KZT','LAK','LBP','LKR','LRD','LSL','LTL','LYD','MAD','MDL','MGA','MKD','MMK','MNT','MOP','MRO','MUR','MVR','MWK','MXN','MXV','MYR','MZN','NAD','NGN','NIO','NOK','NPR','NZD','OMR','PAB','PEN','PGK','PHP','PKR','PLN','PYG','QAR','RON','RSD','RUB','RWF','SAR','SBD','SCR','SDG','SEK','SGD','SHP','SLL','SOS','SRD','SSP','STD','SVC','SYP','SZL','THB','TJS','TMT','TND','TOP','TRY','TTD','TWD','TZS','UAH','UGX','USD','USN','USS','UYI','UYU','UZS','VEF','VND','VUV','WST','XAF','XAG','XAU','XBA','XBB','XBC','XBD','XCD','XDR','XOF','XPD','XPF','XPT','XSU','XTS','XUA','XXX','YER','ZAR','ZMW','ZWL'));

        $currencies = array();

        $currencies_list = func_query("SELECT code, name FROM $sql_tbl[currencies] WHERE code IN ('{$supported_currencies}')");

        if (!empty($currencies_list)) {
            foreach ($currencies_list as $currency_item) {
                $currencies[$currency_item['code']] = $currency_item['name'];
            }
        }
    }

    return $currencies;
} // }}}

function func_pitney_bowes_get_all_countries()
{ // {{{
    global $sql_tbl;

    static $countries = null;

    if (is_null($countries)) {

        // https://wiki.ecommerce.pb.com/display/TECH4/Data+Types+-+Country
        $supported_countries = implode("','", array('AD','AE','AF','AG','AI','AL','AM','AO','AQ','AR','AS','AT','AU','AW','AX','AZ','BA','BB','BD','BE','BF','BG','BH','BI','BJ','BL','BM','BN','BO','BQ','BR','BS','BT','BV','BW','BY','BZ','CA','CC','CD','CF','CG','CH','CI','CK','CL','CM','CN','CO','CR','CU','CV','CW','CX','CY','CZ','DE','DJ','DK','DM','DO','DZ','EC','EE','EG','EH','ER','ES','ET','FI','FJ','FK','FM','FO','FR','GA','GB','GD','GE','GF','GG','GH','GI','GL','GM','GN','GP','GQ','GR','GS','GT','GU','GW','GY','HK','HM','HN','HR','HT','HU','ID','IE','IL','IM','IN','IO','IQ','IR','IS','IT','JE','JM','JO','JP','KE','KG','KH','KI','KM','KN','KP','KR','KW','KY','KZ','LA','LB','LC','LI','LK','LR','LS','LT','LU','LV','LY','MA','MC','MD','ME','MF','MG','MH','MK','ML','MM','MN','MO','MP','MQ','MR','MS','MT','MU','MV','MW','MX','MY','MZ','NA','NC','NE','NF','NG','NI','NL','NO','NP','NR','NU','NZ','OM','PA','PE','PF','PG','PH','PK','PL','PM','PN','PR','PS','PT','PW','PY','QA','RE','RO','RS','RU','RW','SA','SB','SC','SD','SE','SG','SH','SI','SJ','SK','SL','SM','SN','SO','SR','SS','ST','SV','SX','SY','SZ','TC','TD','TF','TG','TH','TJ','TK','TL','TM','TN','TO','TR','TT','TV','TW','TZ','UA','UG','UM','US','UY','UZ','VA','VC','VE','VG','VI','VN','VU','WF','WS','YE','YT','ZA','ZM','ZW'));

        $countries = array();

        $countries_list = func_query("SELECT code, code_A3 FROM $sql_tbl[countries] WHERE code IN ('{$supported_countries}')");

        if (!empty($countries_list)) {
            foreach ($countries_list as $country_item) {
                $countries[$country_item['code']] = $country_item['code_A3'];
            }
        }
    }

    return $countries;
} // }}}

function func_pitney_bowes_get_extract_address($userinfo, $type)
{
    global $config;

    $address = array();

    if (!empty($userinfo['email'])) {
        $address['email'] = $userinfo['email'];
    }

    $fields = array('firstname', 'lastname', 'phone', 'country', 'state', 'city', 'zipcode', 'address');

    foreach ($fields as $field) {
        if (isset($userinfo[$type . '_' . $field])) {
            $address[$field] = $userinfo[$type . '_' . $field];
        }
    }

    if (!empty($userinfo[$type . '_address_2'])) {
        $address['address'] .= ' ' . $userinfo[$type . '_address_2'];
    }

    if (
        $config['General']['zip4_support'] == 'Y'
        && !empty($userinfo[$type . '_zip4'])
    ) {
        $address['zip4'] = $userinfo[$type . '_zip4'];
    }

    return $address;
}

function func_pitney_bowes_get_billing_address($userinfo)
{ // {{{
    return func_pitney_bowes_get_extract_address($userinfo, 'b');
} // }}}

function func_pitney_bowes_get_destination_address($userinfo)
{ // {{{
    return func_pitney_bowes_get_extract_address($userinfo, 's');
} // }}}

function func_pitney_bowes_get_original_address($orig_address)
{ // {{{
    global $config, $single_mode;

    if (!function_exists('func_userinfo')) { x_load('user'); }

    $address = array(
        'firstname' => 'Store',
        'lastname'  => 'Owner',
        'email'   => $config['Company']['orders_department'],

        'phone'   => $config['Company']['company_phone'],

        'country' => $orig_address['country'],
        'state'   => $orig_address['state'],
        'city'    => $orig_address['city'],
        'zipcode' => $orig_address['zipcode'],
        'address' => $orig_address['address'],
    );

    if (
        !$single_mode
        && !empty($orig_address['userid'])
        && ($userinfo = func_userinfo($orig_address['userid'], 'P'))
        && !empty($userinfo)
    ) {
        $address['firstname'] = !empty($userinfo['firstname'])
                ? $userinfo['firstname']
                : '';
        $address['lastname'] = !empty($userinfo['lastname'])
                ? $userinfo['lastname']
                : '';
        $address['email'] = !empty($userinfo['email'])
                ? $userinfo['email']
                : '';
    }

    return $address;
} // }}}

function func_pitney_bowes_get_shipable_items($items)
{ // {{{
    $result = array();

    if (!empty($items)) {
        foreach ($items as $key => $item) {
            if ($item['free_shipping'] != 'Y') {
                $result[$key] = $item;
            }
        }
    }

    return $result;
} // }}}

function func_pitney_bowes_reset_exported_flag($productid)
{ // {{{
    $data = array(
        'productid' => $productid,
        'exported' => XCPitneyBowesDefs::DATASET_STATUS_PENDING,
    );

    func_array2insert('product_pb_exports', $data, true);
} // }}}

function func_pitney_bowes_get_customer_country()
{ // {{{
    global $config, $logged_userid, $login, $user_account, $CLIENT_IP;

    $country_code = '';

    if (!function_exists('func_userinfo')) { x_load('user'); }

    $userinfo = func_userinfo($logged_userid, !empty($login) ? $user_account['usertype'] : '', false, false, 'H');

    if (!empty($userinfo['s_country'])) {
        $country_code = $userinfo['s_country'];
    }

    if (empty($country_code)) {

        $geo_addr_info = func_get_address_by_ip($CLIENT_IP);

        if (!empty($geo_addr_info['country_code'])) {

            $country = func_get_country($geo_addr_info['country_code'], 'Y');

            if (!empty($country)) {

                $country_code = $country;

            }
        }
    }

    if (
        empty($country_code)
        && $config['General']['apply_default_country'] == 'Y'
        && !empty($config['General']['default_country'])
    ) {
        $country_code = $config['General']['default_country'];
    }

    return $country_code;
} // }}}

function func_pitney_bowes_add_search_conditions($data, &$fields, &$inner_joins, &$left_joins, &$where)
{ //{{{
    global $sql_tbl, $config;

    // Search conditions
    if (AREA_TYPE == 'C') {

        $customer_country_slashed = addslashes(func_pitney_bowes_get_customer_country());

        $fields[] = 'PB_RESTRICTIONS.country';
        $fields[] = 'PB_RESTRICTIONS.restriction_code';

        $left_joins['PB_RESTRICTIONS'] = array(
            'tblname' => 'product_pb_restrictions',
            'on' => $sql_tbl['products'] . '.productid = PB_RESTRICTIONS.productid'
            . " AND PB_RESTRICTIONS.country = '$customer_country_slashed'"
        );

        switch ($config[PITNEY_BOWES]['pb_restricted_products']) {

            case XCPitneyBowesDefs::RESTRICTED_PRODUCTS_SHOW:
                break;

            case XCPitneyBowesDefs::RESTRICTED_PRODUCTS_HIDE:
                $where[] = "( PB_RESTRICTIONS.restriction_code = ''"
                            . " OR "
                            . "PB_RESTRICTIONS.restriction_code IS NULL )";
                break;
        }

    } else {

        $fields[] = 'PB_EXPORTS.exported';

        $inner_joins['PB_EXPORTS'] = array(
            'tblname' => 'product_pb_exports',
            'on' => $sql_tbl['products'] . '.productid = PB_EXPORTS.productid'
        );

        $fields[] = 'PB_RESTRICTIONS.country';
        $fields[] = 'PB_RESTRICTIONS.restriction_code';

        $left_joins['PB_RESTRICTIONS'] = array(
            'tblname' => 'product_pb_restrictions',
            'on' => $sql_tbl['products'] . '.productid = PB_RESTRICTIONS.productid'
        );
    }
} //}}}

 function func_pitney_bowes_get_processor($printDebug)
{ // {{{
    return new XCart\Modules\PitneyBowes\Shipping\Processor\PitneyBowes($printDebug);
} // }}}

/**
 * jsonpp - Pretty print JSON data
 *
 * In versions of PHP < 5.4.x, the json_encode() function does not yet provide a
 * pretty-print option. In lieu of forgoing the feature, an additional call can
 * be made to this function, passing in JSON text, and (optionally) a string to
 * be used for indentation.
 *
 * @param string $json  The JSON data, pre-encoded
 * @param string $istr  The indentation string
 *
 * @link https://github.com/ryanuber/projects/blob/master/PHP/JSON/jsonpp.php
 *
 * @return string
 */
function func_pitney_bowes_jsonpp($json, $istr='  ')
{ // {{{
    $result = '';
    for($p=$q=$i=0; isset($json[$p]); $p++)
    {
        $json[$p] == '"' && ($p>0?$json[$p-1]:'') != '\\' && $q=!$q;
        if(!$q && strchr(" \t\n", $json[$p])){continue;}
        if(strchr('}]', $json[$p]) && !$q && $i--)
        {
            strchr('{[', $json[$p-1]) || $result .= "\n".str_repeat($istr, $i);
        }
        $result .= $json[$p];
        if(strchr(',{[', $json[$p]) && !$q)
        {
            $i += strchr('{[', $json[$p])===FALSE?0:1;
            strchr('}]', $json[$p+1]) || $result .= "\n".str_repeat($istr, $i);
        }
    }
    return $result;
} // }}}

function func_pitney_bowes_format_json_output($config, $json)
{ // {{{
    if (
        defined('XC_PITNEY_BOWES_DEBUG')
        || $config->print_debug
    ) {
        return func_pitney_bowes_jsonpp($json);
    }
    return $json;
} // }}}

function func_pitney_bowes_periodic_catalog_update()
{ // {{{
    global $config;

    $return = '';

    $export_routines = array (
        XCPitneyBowesDefs::EXPORT_TYPE_DIFF => true,
        XCPitneyBowesDefs::EXPORT_TYPE_FULL => true,
    );

    // DIFF export {{{
    $prev_devent = func_get_event(XCPitneyBowesDefs::EVENT_EXPORT_CATALOG_DIFF);

    if ((XC_TIME - $prev_devent) <= (constant('SECONDS_PER_HOUR') * $config[PITNEY_BOWES]['pb_diff_catalog_extraction'])) {
        $export_routines[XCPitneyBowesDefs::EXPORT_TYPE_DIFF] = false;
    }

    if ($export_routines[XCPitneyBowesDefs::EXPORT_TYPE_DIFF]) {
        // Export catalog
        $exporter = new XCPitneyBowesExporter();
        $exporter->export();

        echo "\n"; // echo new line

        $return .= func_get_langvar_by_name('msg_pitney_bowes_export_diff_completed', null, false, true);

        func_set_event(XCPitneyBowesDefs::EVENT_EXPORT_CATALOG_DIFF);
    }
    // DIFF export }}}

    // FULL export {{{
    $prev_fevent = func_get_event(XCPitneyBowesDefs::EVENT_EXPORT_CATALOG_FULL);

    if ((XC_TIME - $prev_fevent) <= (constant('SECONDS_PER_DAY') * $config[PITNEY_BOWES]['pb_full_catalog_extraction'])) {
        $export_routines[XCPitneyBowesDefs::EXPORT_TYPE_FULL] = false;
    }

    if ($export_routines[XCPitneyBowesDefs::EXPORT_TYPE_FULL]) {
        // Mark all products pending export
        func_pitney_bowes_mark_all_products_pending();
        // Export catalog
        $exporter = new XCPitneyBowesExporter();
        $exporter->export();

        echo "\n"; // echo new line

        $return .= func_get_langvar_by_name('msg_pitney_bowes_export_full_completed', null, false, true);

        func_set_event(XCPitneyBowesDefs::EVENT_EXPORT_CATALOG_FULL);
    }
    // FULL export }}}

    if (empty($return)) {
        $return = func_get_langvar_by_name('msg_pitney_bowes_export_malapropos_start', null, false, true);
    }

    return $return;
} // }}}
