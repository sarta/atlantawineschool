<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/* * ***************************************************************************\
  +-----------------------------------------------------------------------------+
  | X-Cart Software license agreement                                           |
  | Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
  | All rights reserved.                                                        |
  +-----------------------------------------------------------------------------+
  | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
  | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
  | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
  |                                                                             |
  | THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
  | SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
  | (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
  | FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
  | THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
  | LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
  | INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
  | (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
  | LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
  | NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
  | PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
  | THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
  | SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
  | GRANTED BY THIS AGREEMENT.                                                  |
  +-----------------------------------------------------------------------------+
  \**************************************************************************** */

/**
 * API - DEV TOOL 4 TESTS
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Qualiteam software Ltd <info@x-cart.com>
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v2 (xcart_4_7_5), 2016-02-18 13:44:28, CreateOrderRequest.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace XCart\Modules\PitneyBowes\Shipping\API\Request\Stub;

use \XCart\Modules\PitneyBowes\Shipping\API;

/**
 * https://wiki.ecommerce.pb.com/display/TECH4/Create+Order
 */
class CreateOrderRequest extends API\Request\CreateOrderRequest
{
    protected $body = '{"order": [{
       "orderId": "ORMUSP11120562GB",
       "order":    {
          "quoteCurrency": "USD",
          "transactionId": "22",
          "quoteLines": [      {
             "lineId": "65",
             "merchantComRefId": "0-12345600",
             "quoteLineId": "1",
             "quantity": 1,
             "unitPrice":          {
                "value": 600,
                "currency": "USD"
             },
             "unitImportation":          {
                "importationCurrency": "USD",
                "approximateDuty":             {
                   "value": 60.04,
                   "currency": "USD"
                },
                "approximateTax":             {
                   "value": 135.8,
                   "currency": "USD"
                },
                "approximateBrokerage":             {
                   "value": 1.51,
                   "currency": "USD"
                },
                "total":             {
                   "value": 197.35,
                   "currency": "USD"
                }
             },
             "unitTransportation":          {
                "currency": "USD",
                "merchantShippingIdentifier": "STANDARD",
                "speed": "STANDARD",
                "shipping":             {
                   "value": 13.85,
                   "currency": "USD"
                },
                "handling":             {
                   "value": 2.01,
                   "currency": "USD"
                },
                "total":             {
                   "value": 15.86,
                   "currency": "USD"
                },
                "minDays": 4,
                "maxDays": 7
             },
             "unitTotal":          {
                "value": 213.21,
                "currency": "USD"
             },
             "linePrice":          {
                "value": 600,
                "currency": "USD"
             },
             "lineImportation":          {
                "importationCurrency": "USD",
                "approximateDuty":             {
                   "value": 60.04,
                   "currency": "USD"
                },
                "approximateTax":             {
                   "value": 135.8,
                   "currency": "USD"
                },
                "approximateBrokerage":             {
                   "value": 1.51,
                   "currency": "USD"
                },
                "total":             {
                   "value": 197.35,
                   "currency": "USD"
                }
             },
             "lineTransportation":          {
                "currency": "USD",
                "merchantShippingIdentifier": "STANDARD",
                "speed": "STANDARD",
                "shipping":             {
                   "value": 13.85,
                   "currency": "USD"
                },
                "handling":             {
                   "value": 2.01,
                   "currency": "USD"
                },
                "total":             {
                   "value": 15.86,
                   "currency": "USD"
                },
                "minDays": 4,
                "maxDays": 7
             },
             "lineTotal":          {
                "value": 213.21,
                "currency": "USD"
             }
          }],
          "totalCommodity":       {
             "value": 600,
             "currency": "USD"
          },
          "totalTransportation":       {
             "currency": "USD",
             "merchantShippingIdentifier": "STANDARD",
             "speed": "STANDARD",
             "shipping":          {
                "value": 13.85,
                "currency": "USD"
             },
             "handling":          {
                "value": 2.01,
                "currency": "USD"
             },
             "total":          {
                "value": 15.86,
                "currency": "USD"
             },
             "minDays": 4,
             "maxDays": 7
          },
          "totalImportation":       {
             "importationCurrency": "USD",
             "approximateDuty":          {
                "value": 60.04,
                "currency": "USD"
             },
             "approximateTax":          {
                "value": 135.8,
                "currency": "USD"
             },
             "approximateBrokerage":          {
                "value": 1.51,
                "currency": "USD"
             },
             "total":          {
                "value": 197.35,
                "currency": "USD"
             }
          },
          "total":       {
             "value": 213.21,
             "currency": "USD"
          }
       },
       "status":    {
          "confirmed": false,
          "canceled": false,
          "held": false,
          "itemsShipped": 0
       },
       "expireDate": "2015-08-12T14:52:35-04:00",
       "shipToHub":    {
          "hubId": "US_ELOVATIONS_KY",
          "hubAddress":       {
             "street1": "1850 Airport Exchange Boulevard",
             "street2": "Order#: ORMUSP11120562GB",
             "city": "Erlanger",
             "provinceOrState": "Kentucky",
             "postalOrZipCode": "41025-2501",
             "country": "US"
          }
       }
    }]}';

    /**
     * 
     */
    public function getResponse()
    {
        $this->response = new \PEAR2\HTTP\Request\Response(
            array(),
            $this->body,
            array(),
            array()
        );

        return parent::getResponse();
    }
}
