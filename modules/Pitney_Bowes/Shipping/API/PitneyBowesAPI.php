<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/* * ***************************************************************************\
  +-----------------------------------------------------------------------------+
  | X-Cart Software license agreement                                           |
  | Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
  | All rights reserved.                                                        |
  +-----------------------------------------------------------------------------+
  | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
  | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
  | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
  |                                                                             |
  | THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
  | SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
  | (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
  | FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
  | THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
  | LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
  | INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
  | (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
  | LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
  | NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
  | PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
  | THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
  | SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
  | GRANTED BY THIS AGREEMENT.                                                  |
  +-----------------------------------------------------------------------------+
  \**************************************************************************** */

/**
 * API
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Michael Bugrov
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v4 (xcart_4_7_5), 2016-02-18 13:44:28, PitneyBowesAPI.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace XCart\Modules\PitneyBowes\Shipping\API;

use XCart\Modules\PitneyBowes\Shipping\API;
use XCart\Modules\PitneyBowes\Shipping\Mapper;

/**
 * @see: https://wiki.ecommerce.pb.com/display/TECH4/Pitney+Bowes+Ecommerce+-+Technical+Wiki
 */
class PitneyBowesAPI { // {{{

    /**
     * @var object API config
     */
    protected $config;

    /**
     * @var string Auth token
     */
    protected $auth;

    /**
     * Constructor
     *
     * @param object $config
     */
    public function __construct($config)
    { // {{{
        $this->config = $config;
    } // }}}

    /**
     * @return string
     */
    public function getQuoteCacheKey($inputData)
    { // {{{
        $inputMapper = new Mapper\GetQuote\QuoteInputMapper($this->config);
        $inputMapper->setInputData($inputData);

        $prefix = !empty($inputData['orderid'])
            ? $inputData['orderid']
            : (
                !empty($inputData[\XCPitneyBowesDefs::CART_TRANSACTIONID])
                ? $inputData[\XCPitneyBowesDefs::CART_TRANSACTIONID]
                : ''
            );

        return (!empty($prefix) ? $prefix .'-' : '') . md5($inputMapper->getMapped());
    } // }}}

    /**
     * @return string
     */
    public function getOrderCacheKey($inputData)
    { // {{{
        $inputMapper = new Mapper\CreateOrder\OrderInputMapper($this->config);
        $inputMapper->setInputData($inputData);

        $prefix = !empty($inputData['orderid'])
            ? $inputData['orderid']
            : (
                !empty($inputData[\XCPitneyBowesDefs::CART_TRANSACTIONID])
                ? $inputData[\XCPitneyBowesDefs::CART_TRANSACTIONID]
                : ''
            );

        return (!empty($prefix) ? $prefix .'-' : '') . md5($inputMapper->getMapped());
    } // }}}

    /**
     * https://wiki.ecommerce.pb.com/display/TECH4/Get+Quote
     * 
     * @return QuoteSet
     */
    public function getQuote($inputData)
    { // {{{
        $url = $this->config->checkout_endpoint;

        $request = new API\Request\GetQuoteRequest($url, $inputData);

        $inputMapper = new Mapper\GetQuote\QuoteInputMapper($this->config);
        $request->setInputMapper($inputMapper);

        $request->setOutputMapper(new Mapper\GetQuote\OutputMapper($this->config));

        $auth = $this->getAuth();
        $request->setAuth($auth['type'], $auth['value']);

        $request->sendRequest();

        $this->logCommunication(__FUNCTION__, $request, $this->getQuoteCacheKey($inputData));

        return $request->getResponse();
    } // }}}

    /**
     * https://wiki.ecommerce.pb.com/display/TECH4/Create+Order
     * 
     * @return OrderSet
     */
    public function createOrder($inputData)
    { // {{{
        $url = $this->config->checkout_endpoint;

        $request = new API\Request\CreateOrderRequest($url, $inputData);

        $inputMapper = new Mapper\CreateOrder\OrderInputMapper($this->config);
        $request->setInputMapper($inputMapper);

        $request->setOutputMapper(new Mapper\CreateOrder\OutputMapper($this->config));

        $auth = $this->getAuth();
        $request->setAuth($auth['type'], $auth['value']);

        $request->sendRequest();

        $this->logCommunication(__FUNCTION__, $request, $this->getOrderCacheKey($inputData));

        return $request->getResponse();
    } // }}}

    /**
     * https://wiki.ecommerce.pb.com/display/TECH4/Confirm+Order
     * 
     * @param array $inputData
     * 
     * @return mixed
     */
    public function confirmOrder($inputData)
    { // {{{
        $url = $this->config->checkout_endpoint;

        $request = new API\Request\ConfirmOrderRequest($url, $inputData);

        $inputMapper = new Mapper\ConfirmOrder\InputMapper($this->config);
        $request->setInputMapper($inputMapper);

        $request->setOutputMapper(new Mapper\ConfirmOrder\OutputMapper($this->config));

        $auth = $this->getAuth();
        $request->setAuth($auth['type'], $auth['value']);

        $request->sendRequest();

        $this->logCommunication(__FUNCTION__, $request);

        return $request->getResponse();
    } // }}}

    /**
     * https://wiki.ecommerce.pb.com/display/TECH4/Create+Inbound+Parcels
     * 
     * @param array $inputData
     * 
     * @return mixed
     */
    public function createInboundParcelsRequest(array $inputData)
    { // {{{
        $url = $this->config->asn_endpoint;

        $request = new API\Request\CreateInboundParcelsRequest($url, $inputData);

        $inputMapper = new Mapper\CreateInboundParcels\InputMapper($this->config);
        $request->setInputMapper($inputMapper);

        $request->setOutputMapper(new Mapper\CreateInboundParcels\OutputMapper($this->config));

        $auth = $this->getAuth();
        $request->setAuth($auth['type'], $auth['value']);

        $request->sendRequest();

        $this->logCommunication(__FUNCTION__, $request);

        return $request->getResponse();
    } // }}}

    protected function getAuth()
    { // {{{
        if (null === $this->auth) {
            $this->auth = $this->authToken();
        }
        return $this->auth;
    } // }}}

    protected function logCommunication($apiFunction, $request, $hash = false)
    { // {{{
        $logRecord =
            "The $apiFunction API called with: " . PHP_EOL
                . ($hash ? 'Hash: '. $hash . PHP_EOL : '')
                . 'Request: ' . func_pitney_bowes_format_json_output($this->config, $request->getRawRequest()) . PHP_EOL
                . 'Response: '. func_pitney_bowes_format_json_output($this->config, $request->getRawResponse()) . PHP_EOL;

        if ($this->config->print_debug) {
            echo "<pre>$logRecord</pre>";
        }

        func_pitney_bowes_debug_log($logRecord);
    } // }}}

    /**
     * https://wiki.ecommerce.pb.com/display/TECH4/Auth+Token
     *
     * @return AuthToken
     */
    protected function authToken()
    { // {{{
        $url = $this->config->token_endpoint;

        $request = new API\Request\AuthTokenRequest($url, array());
        $request->setOutputMapper(new API\Mapper\AuthTokenMapper($this->config));

        $authBase64 = base64_encode(
            sprintf('%s:%s', $this->config->api_username, $this->config->api_password)
        );

        $request->setAuth('Basic', $authBase64);
        $request->sendRequest();

        return $request->getResponse();
    } // }}}

} // }}}
