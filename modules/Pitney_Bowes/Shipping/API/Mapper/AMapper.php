<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/* * ***************************************************************************\
  +-----------------------------------------------------------------------------+
  | X-Cart Software license agreement                                           |
  | Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
  | All rights reserved.                                                        |
  +-----------------------------------------------------------------------------+
  | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
  | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
  | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
  |                                                                             |
  | THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
  | SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
  | (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
  | FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
  | THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
  | LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
  | INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
  | (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
  | LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
  | NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
  | PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
  | THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
  | SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
  | GRANTED BY THIS AGREEMENT.                                                  |
  +-----------------------------------------------------------------------------+
  \**************************************************************************** */

/**
 * API
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Qualiteam software Ltd <info@x-cart.com>
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v2 (xcart_4_7_5), 2016-02-18 13:44:28, AMapper.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace XCart\Modules\PitneyBowes\Shipping\API\Mapper;

abstract class AMapper implements IMapper
{
    /**
     * @var mixed
     */
    protected $inputData;

    /**
     * @var mixed
     */
    protected $outputData;

    /**
     * @var mixed
     */
    protected $config;

    /**
     * @var IMapper
     */
    protected $nextMapper;

    /**
     * @var array
     */
    protected $additionalData;

    /**
     * @param mixed     $config      Config
     */
    function __construct($config)
    {
        $this->config = $config;
        $this->additionalData = array();
    }

    /**
     * Set input data
     * 
     * @param mixed     $inputData  Input data
     * @param string    $key        Additional data key     OPTIONAL
     * 
     * @return void
     */
    public function setInputData($inputData, $key = 'default')
    {
        if ('default' == $key) {
            $this->inputData = $inputData;
        } else {
            $this->additionalData[$key] = $inputData;
        }
    }

    /**
     * Get additional data by key
     * 
     * @param string    $key        Additional data key
     * 
     * @return mixed
     */
    protected function getAdditionalData($key)
    {
        return isset($this->additionalData[$key])
            ? $this->additionalData[$key]
            : null;
    }

    /**
     * Set next mapper if current will not succeed
     * 
     * @param IMapper $nextMapper Next mapper
     * 
     * @return void
     */
    public function setNextMapper(IMapper $nextMapper)
    {
        $this->nextMapper = $nextMapper;
    }

    /**
     * Is mapper able to map?
     * 
     * @return boolean
     */
    abstract protected function isApplicable();

    /**
     * Perform actual mapping
     * 
     * @return mixed
     */
    abstract protected function performMap();

    /**
     * Postprocess mapped data
     * 
     * @param mixed $mapped mapped data to postprocess
     * 
     * @return mixed
     */
    abstract protected function postProcessMapped($mapped);

    /**
     * @return mixed
     */
    public function getMapped()
    {
        $result = null;

        if ($this->isApplicable()) {
            $result = $this->postProcessMapped($this->performMap());
        } elseif ($this->nextMapper) {
            $this->nextMapper->setInputData($this->inputData);
            $result = $this->nextMapper->getMapped();
        } else {
            x_log_add(
                PITNEY_BOWES,
                'Internal error in mapper ' . get_class($this)
            );
        }

        return $result;
    }
}
