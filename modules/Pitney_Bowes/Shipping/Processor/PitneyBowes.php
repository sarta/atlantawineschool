<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/* * ***************************************************************************\
  +-----------------------------------------------------------------------------+
  | X-Cart Software license agreement                                           |
  | Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
  | All rights reserved.                                                        |
  +-----------------------------------------------------------------------------+
  | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
  | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
  | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
  |                                                                             |
  | THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
  | SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
  | (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
  | FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
  | THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
  | LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
  | INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
  | (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
  | LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
  | NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
  | PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
  | THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
  | SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
  | GRANTED BY THIS AGREEMENT.                                                  |
  +-----------------------------------------------------------------------------+
  \**************************************************************************** */

/**
 * Processor
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Michael Bugrov
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v7 (xcart_4_7_5), 2016-02-18 13:44:28, PitneyBowes.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace XCart\Modules\PitneyBowes\Shipping\Processor;

/**
 * Shipping processor model
 * API documentation: https://wiki.ecommerce.pb.com/display/TECH4/Pitney+Bowes+Ecommerce+-+Technical+Wiki
 */
class PitneyBowes { // {{{

    const PROCESSOR_ID = \XCPitneyBowesDefs::SHIPPING_CODE;

    /**
     * Create order API call TTL
     */
    const CREATE_ORDER_TTL = 3600;

    /**
     * @var mixed
     */
    protected static $config;

    /**
     * @var mixed
     */
    protected $api;

    /**
     * Construct
     */
    public function __construct($printDebug)
    { // {{{
        static::$config = \XCPitneyBowesConfig::getInstance()->getConfigAsObject($printDebug);

        $this->api = new \XCart\Modules\PitneyBowes\Shipping\API\PitneyBowesAPI(static::$config);
        // Register cache function
        func_register_cache_function(
            'getPitneyBowesProcessorDataFromCache',
            array(
                'class' => get_class($this),
                'ttl' => self::CREATE_ORDER_TTL,
                'dir' => 'pitney_bowes_cache',
                'hashedDirectoryLevel' => 2,
            )
        );
    } // }}}

    /**
     * Returns processor Id
     *
     * @return string
     */
    public function getProcessorId()
    { // {{{
        return self::PROCESSOR_ID;
    } // }}}

    /**
     * Returns shipping rates
     *
     * @param array $inputData      Shipping array data for request
     * @param boolean $ignoreCache  Flag: if true then do not get rates from cache OPTIONAL
     *
     * @return array
     */
    public function getRates($inputData, $ignoreCache = false)
    { // {{{
        $this->errorMsg = null;
        $rates = array();

        if ($this->isConfigured()) {
            if (
                !empty($inputData[\XCPitneyBowesDefs::CART_TRANSACTIONID])
            ) {
                $key = $inputData[\XCPitneyBowesDefs::CART_TRANSACTIONID];

                $rates = $this->getGuaranteedRates($inputData, $ignoreCache);

            } else {
                $rates = $this->getNonGuaranteedRates($inputData, $ignoreCache);
            }
        }

        return $rates;
    } // }}}

    /**
     * Returns shipping orders
     *
     * @param array $inputData      Shipping array data for request
     * @param boolean $ignoreCache  Flag: if true then do not get rates from cache OPTIONAL
     *
     * @return array
     */
    public function getOrders($inputData, $ignoreCache = false)
    { // {{{
        $this->errorMsg = null;
        $orders = array();

        if ($this->isConfigured()) {
            if (
                !empty($inputData[\XCPitneyBowesDefs::CART_TRANSACTIONID])
            ) {
                // No lock required, since rates were already requested
                $orders = $this->getGuaranteedRates($inputData, $ignoreCache, 'orders');
            }
        }

        return $orders;
    } // }}}

    // {{{ Static methods for calculations

    /**
     * @param integer $itemsCount   Count items in order
     * 
     * @return decimal
     */
    public static function getShippingFeeMarkup($itemsCount)
    { // {{{
        $total = floatval(static::$config->shipping_fee_markup);

        if (\XCPitneyBowesDefs::SHIPPING_MARKUP_BASIS_ITEMS == static::$config->shipping_fee_markup_basis) {
            $total *= $itemsCount;
        }

        return $total;
    } // }}}

    /**
     * @param integer $itemsCount   Count items in order
     * 
     * @return decimal
     */
    public static function getHandlingFeeMarkup($itemsCount)
    { // {{{
        $total = floatval(static::$config->handling_fee_markup);

        if (\XCPitneyBowesDefs::SHIPPING_MARKUP_BASIS_ITEMS == static::$config->handling_fee_markup_basis) {
            $total *= $itemsCount;
        }

        return $total;
    } // }}}

    /**
     * Get shipping method for rate
     * 
     * @param string $methodCode
     * 
     * @return array
     */
    public static function getMethod($methodCode)
    { // {{{
        global $sql_tbl;

        $code = self::PROCESSOR_ID;

        $method = func_query_first_cell(
            "SELECT *"
            . " FROM $sql_tbl[shipping]"
            . " WHERE code='$code'"
                . " AND subcode='$methodCode'"
        );

        return $method;
    } // }}}

    /**
     * Get transportation part of rate
     * 
     * @param float     $subtotal
     * @param string    $country
     * @param mixed     $orderJsonObject
     * 
     * @return float
     */
    public static function getTransportationPart($subtotal, $country, $orderJsonObject, $savedConfig = false)
    { // {{{
        $config = $savedConfig ?: static::$config;

        $value = $orderJsonObject->totalTransportation->total->value;

        $tresholdTrigger = $config->free_transportation
            && $subtotal > floatval($config->free_transportation_threshold);

        $countryTrigger = $config->free_transportation_countries
            && in_array($country, array_keys($config->free_transportation_countries));

        if ($tresholdTrigger || $countryTrigger) {
            $value = 0;
        }

        return $value;
    } // }}}

    /**
     * Get transportation part of rate
     * 
     * @param float     $subtotal
     * @param string    $country
     * @param mixed     $orderJsonObject
     * 
     * @return float
     */
    public static function getImportationPart($subtotal, $country, $orderJsonObject, $savedConfig = false)
    { // {{{
        $config = $savedConfig ?: static::$config;

        $value = $orderJsonObject->totalImportation->total->value;

        $tresholdTrigger = $config->free_importation
            && $subtotal > floatval($config->free_importation_threshold);

        $countryTrigger = $config->free_importation_countries
            && in_array($country, array_keys($config->free_importation_countries));

        if ($tresholdTrigger || $countryTrigger) {
            $value = 0;
        }

        return $value;
    } // }}}

    /**
     * Get transportation part of rate
     * 
     * @param float     $subtotal
     * @param string    $country
     * @param mixed     $orderJsonObject
     * 
     * @return float
     */
    public static function getFullTransportationCost($subtotal, $country, $orderJsonObject)
    { // {{{
        return static::getTransportationPart($subtotal, $country, $orderJsonObject)
             + static::getImportationPart($subtotal, $country, $orderJsonObject);
    } // }}}

    // }}} Static methods for calculations

    /**
     * This method must return the URL to the detailed tracking information about the package.
     * Tracking number is provided.
     *
     * @param string $trackingNumber Tracking number
     *
     * @return string
     */
    public static function getTrackingInformationURL($trackingNumber)
    { // {{{
        return 'https://parceltracking.pb.com/app/#/dashboard/'.$trackingNumber;
    } // }}}

    /**
     * Get array of required config options
     */
    protected function getRequiredConfigOptions()
    { // {{{
        return array(
            \XCPitneyBowesDefs::CONFIG_VAR_API_USERNAME,
            \XCPitneyBowesDefs::CONFIG_VAR_API_PASSWORD,
            \XCPitneyBowesDefs::CONFIG_VAR_MERCHANT_CODE,
        );
    } // }}}

    protected function arrayAny($array, Callable $condition)
    { // {{{
        $result = false;

        foreach ($array as $key => $value) {
            if ($condition($key, $value)) {
                $result = true;
                break;
            }
        }

        return $result;
    } // }}}

    /**
     * Returns true if PitneyBowes module is configured
     *
     * @return boolean
     */
    protected function isConfigured()
    { // {{{
        $config = static::$config;
        return !$this->arrayAny(
            $this->getRequiredConfigOptions(),
            function($key, $option) use ($config) {
                return !$config->{$option};
            }
        );
    } // }}}

    /**
     * 
     * @param array $inputData  Shipping array data for request
     * 
     * @return string
     */
    protected function getNonGuaranteedCacheKey($inputData)
    { // {{{
        return $this->getProcessorId() . $this->api->getQuoteCacheKey($inputData);
    } // }}}

    /**
     * 
     * @param array $inputData  Shipping array data for request
     * 
     * @return string
     */
    protected function getGuaranteedCacheKey($inputData)
    { // {{{
        return $this->getProcessorId() . $this->api->getOrderCacheKey($inputData);
    } // }}}

    /**
     * Internal unconditional getRates() part
     * 
     * @param array $inputData      Shipping array data for request
     * @param boolean $ignoreCache  Flag: if true then do not get rates from cache OPTIONAL
     *
     * @return array
     */
    protected function getNonGuaranteedRates($inputData, $ignoreCache = false)
    { // {{{
        $rates = array();

        $cachedRate = null;

        $cacheKey = $this->getNonGuaranteedCacheKey($inputData);

        if (!$ignoreCache) {
            $cachedRate = $this->getPitneyBowesProcessorDataFromCache($cacheKey);
        }

        if (null !== $cachedRate) {
            $rates = $cachedRate;

        } else {
            $rates = $this->getNonGuaranteedRatesFromServer($inputData);
            $this->saveDataInCache($cacheKey, $rates);
        }

        return $rates;
    } // }}}

    /**
     * Internal unconditional getRates() part
     * 
     * @param array $inputData  Shipping array data for request
     * 
     * @return array
     */
    protected function getNonGuaranteedRatesFromServer($inputData)
    { // {{{
        $rates = array();
        if ($inputData && !empty($inputData)) {
            $rates = $this->api->getQuote($inputData);
        }
        return $rates;
    } // }}}

    /**
     * Internal unconditional getRates() part
     * 
     * @param $inputData            Shipping array data for request
     * @param boolean $ignoreCache  Flag: if true then do not get rates from cache OPTIONAL
     * @param string $responsePart  Response part to return as the result OPTIONAL
     *
     * @return array
     */
    protected function getGuaranteedRates($inputData, $ignoreCache = false, $responsePart = 'rates')
    { // {{{
        $rates = array();

        $cachedRate = null;

        $cacheKey = $this->getGuaranteedCacheKey($inputData);

        if (!$ignoreCache) {
            $cachedRate = $this->getPitneyBowesProcessorDataFromCache($cacheKey);
        }

        if (null !== $cachedRate) {
            $rates = $cachedRate[$responsePart];

        } else {
            $orders = $this->getGuaranteedRatesFromServer($inputData);
            if ($orders) {
                $this->saveDataInCache($cacheKey, $orders);
                $rates = $orders[$responsePart];
            }
        }

        return $rates;
    } // }}}

    /**
     * Internal unconditional getRates() part
     * 
     * @param array $inputData  Shipping array data for request
     * 
     * @return array
     */
    protected function getGuaranteedRatesFromServer($inputData)
    { // {{{
        $orders = array();

        if ($inputData && !empty($inputData)) {
            $orders = $this->api->createOrder($inputData);
        }

        return $orders;
    } // }}}

    /**
     * Get data from file cache
     *
     * @param type $cache_key
     */
    protected function getPitneyBowesProcessorDataFromCache($cache_key)
    { // {{{
        if (($cacheData = func_get_cache_func($cache_key, 'getPitneyBowesProcessorDataFromCache'))) {
            return $cacheData['data'];
        }
    } // }}}

    /**
     * Save data in cache file
     *
     * @param type $cache_key
     * @param type $data
     *
     */
    protected function saveDataInCache($cache_key, $data)
    { // {{{
        func_save_cache_func($data, $cache_key, 'getPitneyBowesProcessorDataFromCache');
    } // }}}

} // }}}
