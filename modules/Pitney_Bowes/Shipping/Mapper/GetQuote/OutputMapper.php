<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/* * ***************************************************************************\
  +-----------------------------------------------------------------------------+
  | X-Cart Software license agreement                                           |
  | Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
  | All rights reserved.                                                        |
  +-----------------------------------------------------------------------------+
  | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
  | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
  | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
  |                                                                             |
  | THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
  | SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
  | (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
  | FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
  | THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
  | LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
  | INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
  | (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
  | LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
  | NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
  | PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
  | THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
  | SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
  | GRANTED BY THIS AGREEMENT.                                                  |
  +-----------------------------------------------------------------------------+
  \**************************************************************************** */

/**
 * GetQuote - OutputMapper
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Qualiteam software Ltd <info@x-cart.com>
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v2 (xcart_4_7_5), 2016-02-18 13:44:28, OutputMapper.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace XCart\Modules\PitneyBowes\Shipping\Mapper\GetQuote;

use \XCart\Modules\PitneyBowes\Shipping\API;
use \XCart\Modules\PitneyBowes\Shipping\Processor;

/**
 * Get quote output mapper
 */
class OutputMapper extends API\Mapper\AMapper
{
    /**
     * Is mapper able to map?
     * 
     * @return boolean
     */
    protected function isApplicable()
    {
        return $this->inputData && $this->inputData instanceof \PEAR2\HTTP\Request\Response;
    }

    /**
     * Perform actual mapping
     * 
     * @return mixed
     */
    protected function performMap()
    {
        $response = json_decode($this->inputData->body);

        $rates = array();
        if (isset($response->quote)) {
            foreach ($response->quote as $quote) {
                $rates[] = $this->mapSingleQuote($quote);
            }
        } elseif (isset($response->error)) {
            // Log error
            x_log_add(
                PITNEY_BOWES,
                $response->error . ':' . $response->message
            );
        }

        return $rates;
    }

    /**
     * Get total cost for base rate
     * 
     * @param mixed $quote Single quote line
     * 
     * @return float
     */
    protected function getBaseRate($quote)
    {
        $baseRate = $quote->total->value;

        $requestedData = $this->getAdditionalData('requested');

        if (
            $requestedData
        ) {
            $baseRate = Processor\PitneyBowes::getFullTransportationCost(
                $requestedData['cartSubTotal'],
                $requestedData['billAddress']['country'],
                $quote
            );
        }

        return $baseRate;
    }

    /**
     * Get parts for base rate
     *
     * @param mixed $quote Single quote line
     *
     * @return float
     */
    protected function getBaseRateParts($quote)
    {
        $baseRateParts = array (
            'transportation' => $quote->totalTransportation->total,
            'importation' => $quote->totalImportation->total
        );

        $requestedData = $this->getAdditionalData('requested');

        if (
            $requestedData
        ) {
            $baseRateParts['transportation'] = Processor\PitneyBowes::getTransportationPart(
                $requestedData['cartSubTotal'],
                $requestedData['billAddress']['country'],
                $quote
            );
            $baseRateParts['importation'] = Processor\PitneyBowes::getImportationPart(
                $requestedData['cartSubTotal'],
                $requestedData['billAddress']['country'],
                $quote
            );
        }

        return $baseRateParts;
    }

    /**
     * Map single quote
     * 
     * @param mixed $quote Single quote line
     * 
     * @return \XCPitneyBowesRate
     */
    protected function mapSingleQuote($quote)
    {
        $rate = null;
        if (isset($quote->total->value)) {
            $rate = new \XCPitneyBowesRate();

            $rate->setBaseRate(
                $this->getBaseRate($quote)
            );
            $rate->setBaseRateParts(
                $this->getBaseRateParts($quote)
            );
            $rate->setMarkupRate(
                $this->getMarkupRate($quote)
            );

            $extraData = new \stdClass();

            if (isset($quote->totalTransportation->minDays) && isset($quote->totalTransportation->maxDays)) {

                $extraData->deliveryMinDays = $quote->totalTransportation->minDays + intval($this->config->min_delivery_adjustment);
                $extraData->deliveryMaxDays = $quote->totalTransportation->maxDays + intval($this->config->max_delivery_adjustment);

                $rate->setExtraData($extraData);
            }

            unset($extraData);
        }
        if (isset($quote->errors)) {
            foreach ($quote->errors as $error) {
                x_log_add(
                    PITNEY_BOWES,
                    $error->error
                );
            }
        }
        return $rate;
    }

    /**
     * 
     * @param mixed $quote Single quote line
     * 
     * @return float
     */
    protected function getMarkupRate($quote)
    {
        $handlingFee = Processor\PitneyBowes::getHandlingFeeMarkup(
            count($quote->quoteLines)
        );

        $shippingFee = Processor\PitneyBowes::getShippingFeeMarkup(
            count($quote->quoteLines)
        );

        return $handlingFee + $shippingFee;
    }

    /**
     * Postprocess mapped data
     * 
     * @param mixed $mapped mapped data to postprocess
     * 
     * @return mixed
     */
    protected function postProcessMapped($mapped)
    {
        return array_filter($mapped);
    }

}
