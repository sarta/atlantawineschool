<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/* * ***************************************************************************\
  +-----------------------------------------------------------------------------+
  | X-Cart Software license agreement                                           |
  | Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
  | All rights reserved.                                                        |
  +-----------------------------------------------------------------------------+
  | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
  | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
  | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
  |                                                                             |
  | THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
  | SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
  | (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
  | FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
  | THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
  | LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
  | INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
  | (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
  | LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
  | NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
  | PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
  | THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
  | SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
  | GRANTED BY THIS AGREEMENT.                                                  |
  +-----------------------------------------------------------------------------+
  \**************************************************************************** */

/**
 * GetQuote - OrderInputMapper
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Qualiteam software Ltd <info@x-cart.com>
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v2 (xcart_4_7_5), 2016-02-18 13:44:28, OrderInputMapper.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace XCart\Modules\PitneyBowes\Shipping\Mapper\GetQuote;

use \XCart\Modules\PitneyBowes\Shipping\API;
use \XCart\Modules\PitneyBowes\Shipping\Processor;

/**
 * Get quote input mapper
 */
class OrderInputMapper extends API\Mapper\JsonPostProcessedMapper
{
    /**
     * Is mapper able to map?
     * 
     * @return boolean
     */
    protected function isApplicable()
    {
        return $this->inputData;
    }

    /**
     * Perform actual mapping
     * 
     * @return mixed
     */
    protected function performMap()
    {
        $basket = new API\DataTypes\Basket();

        $basket->merchantId             = $this->config->merchant_code;
        $basket->basketCurrency         = $this->config->currency;
        $basket->quoteCurrency          = $this->config->currency;

        $consignee = $this->getConsignee();
        if (null !== $consignee) {
            $basket->consignee = $consignee;
        } else {
            unset($basket->consignee);
        }

        $basket->shippingAddress        = $this->getShippingAdress();

        foreach ($this->inputData['package']['items_list'] as $cid => $cartItem) {
            $basket->basketLines[] = $this->getBasketLine($cid, $cartItem);
        }

        $basket->toHubTransportations[] = $this->getToHubTransportation();

        $basket->parcels = $this->getParcels();

        return $basket;
    }

    /**
     * toHubTransportation part of basket
     *
     * @return array
     */
    protected function getToHubTransportation()
    {
        $handlingFee = Processor\PitneyBowes::getHandlingFeeMarkup(
            $this->inputData['package']['items_count']
        );

        $shippingFee = Processor\PitneyBowes::getShippingFeeMarkup(
            $this->inputData['package']['items_count']
        );

        return array(
            'merchantShippingIdentifier'            => 'STANDARD',
            'speed'                                 => 'STANDARD',
            'shipping'           => array('value' => $handlingFee),
            'handling'           => array('value' => $shippingFee),
            'total'              => array('value' => $shippingFee + $handlingFee),
            'minDays'            => intval($this->config->min_delivery_adjustment),
            'maxDays'            => intval($this->config->max_delivery_adjustment),
        );
    }

    /**
     * Returns array of data for request
     *
     * @return array
     */
    protected function getParcels()
    {
        $packages = array();
        // TODO Implement it maybe
        return $packages;
    }

    /**
     * @param string $cartItem Product info
     *
     * @return array
     */
    protected function getBasketLine($cid, $cartItem)
    {
        return array(
            'lineId'        => "$cid-$cartItem[cartid]-$cartItem[productcode]",
            'currency'      => $this->config->currency,
            'quantity'      => $cartItem['amount'],
            'unitPrice'     => array(
                'price'         => array('value' => $cartItem['price']),
                'codPrice'      => array(),
                'dutiableValue' => array('value' => $cartItem['price']),
            ),
            'commodity'     => array(
                'merchantComRefId'      => $cartItem['productid'],
                'descriptions'          => array(),
                'hsCodes'               => array(),
                'imageUrls'             => array(),
                'identifiers'           => array(),
                'attributes'            => array(),
                'hazmats'               => array(),
                'categories'            => array(),
            ),
            'kitContents'   => array(),
        );
    }

    /**
     * Retrieve shipping adress from modifier
     * 
     * @return array
     */
    protected function getShippingAdress()
    {
        $dstAddress = $this->inputData['destAddress'];

        // TODO add error checking

        return array(
            'street1'           => @$dstAddress['address'],
            'city'              => $dstAddress['city'],
            'provinceOrState'   => $dstAddress['state'],
            'country'           => $dstAddress['country'],
            'postalOrZipCode'   => $dstAddress['zipcode'],
        );
    }

    /**
     * Retrieve consignee from modifier
     * 
     * @return array
     */
    protected function getConsignee()
    {
        $address = $this->inputData['billAddress'] ?: $this->inputData['destAddress'];

        // TODO add error checking

        $consignee = array(
            'familyName'    => $address['lastname'],
            'givenName'     => $address['firstname'],
            'email'         => $address['email'],
            'phoneNumbers'  => array(
                array(
                    'number' => $address['phone'],
                    'type' => 'other'
                ),
            ),
        );

        return $consignee;
    }
}
