<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart Software license agreement                                           |
| Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
| SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
| (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
| FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
| THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
| LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
| INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
| (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
| LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
| NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
| PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
| THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
| SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
| GRANTED BY THIS AGREEMENT.                                                  |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

/**
 * Product country restrictions
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage
 * @author     Michael Bugrov
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    09d46970de4b3605245381ed8ecd8b55fffd1d3e, v2 (xcart_4_7_5), 2016-02-12 18:28:36, product_details.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

if ( !defined("XCART_SESSION_START") ) { header("Location: ../"); die("Access denied"); }

if ($REQUEST_METHOD == 'POST') {
    if (!empty($product[XCAmazonFeedsDefs::FEED_PRODUCT_TYPE])) {

        $product[XCAmazonFeedsDefs::FEED_PRODUCT_TYPE]
            = addslashes($product[XCAmazonFeedsDefs::FEED_PRODUCT_TYPE]);

        func_array2insert($sql_tbl['amazon_feeds_catalog'], array(
            'productid' => intval($productid),
            'product_type' => $product[XCAmazonFeedsDefs::FEED_PRODUCT_TYPE]
        ), true);

        if($geid && $fields[XCAmazonFeedsDefs::FEED_PRODUCT_TYPE] == 'Y') {
            $amazon_feeds_delim = '(';
            while ($pids = func_ge_each($geid, 100, $productid)) {

                $amazon_feeds_cat_values = '';
                while (($pid = array_shift($pids)) && !empty($pid)) {
                    $amazon_feeds_cat_values
                        .= "$amazon_feeds_delim'{$pid}', '{$product[XCAmazonFeedsDefs::FEED_PRODUCT_TYPE]}')";

                    $amazon_feeds_delim = ', (';
                }

                db_query(
                    "REPLACE INTO $sql_tbl[amazon_feeds_catalog]"
                    . " VALUES $amazon_feeds_cat_values"
                );
            }
        }

        $top_message['content'] = func_get_langvar_by_name('msg_adm_amazon_feeds_applied');
        $top_message['type']    = 'I';

    } else {

        $top_message['content'] = func_get_langvar_by_name('err_amazon_feeds_type_required');
        $top_message['type']    = 'E';
    }

    func_refresh(XCAmazonFeedsDefs::PRODUCT_SECTION);
}

if ($section == XCAmazonFeedsDefs::PRODUCT_SECTION) {
    $product_info['amazon_feeds_product_type'] = func_amazon_feeds_get_product_type($productid);
    $product_info['amazon_feeds_results'] = func_amazon_feeds_get_feeds_results($productid);

    $smarty->assign('amazon_feeds_catalog', XCAmazonFeedsConfig::getInstance()->getProductTypes());
}
