<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2010 Ruslan R. Fazlyev <rrf@x-cart.com>                  |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLYEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazlyev             |
| Portions created by Ruslan R. Fazlyev are Copyright (C) 2001-2010           |
| Ruslan R. Fazlyev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: func.php,v 1.0 lehach Exp $
#
if ( !defined('XCART_START') ) { header("Location: ../"); die("Access denied"); }



#
# add new link
#
function func_add_new_link($link,$parent) {
	global $sql_tbl;

	if (!empty($link["link"])) {

		if (!empty($parent))
			$parent = intval($parent);		

		# create new empty link 
		$linkid = func_array2insert(
			"multilevel_menu", 
			array(
				"parentid" => $parent,
				"link" => ''
			),
			false,
			true
		);

		if ($parent == 0) {
			$parent_linkid_path = $linkid;
		} else {
			$parent_linkid_path = func_query_first_cell("SELECT linkid_path FROM $sql_tbl[multilevel_menu] WHERE linkid='$parent'")."/".$linkid;
		}

		$data = array (
			"parentid" => $parent,
			"linkid_path" => $parent_linkid_path,
			"link" => $link["link"],
			"url" => $link["url"],
			"avail" => $link["avail"],
			"order_by" => $link["order_by"]
		);
		func_array2update("multilevel_menu", $data, "linkid = '$linkid'");

        func_languages_alt_insert('multilevel_menu_'.$linkid, $link["link"], $shop_language);

	}

	return $linkid;
}

#
# get link data
#
function func_get_link_data($linkid) {
	global $sql_tbl, $shop_language;

	$link = func_query_first("SELECT * FROM $sql_tbl[multilevel_menu] WHERE linkid = '$linkid'");
	
	if (!empty($link)) {

		# Get the array of all parent links
		$_link_sequense = explode("/", $link["linkid_path"]);

		# Generate link sequence 
		if(!empty($_link_sequense)) {

            $_link_names = func_query_hash("SELECT $sql_tbl[multilevel_menu].linkid, $sql_tbl[multilevel_menu].link FROM $sql_tbl[multilevel_menu] WHERE $sql_tbl[multilevel_menu].linkid IN ('".implode("','", $_link_sequense)."')","linkid", false);

			if(count($_link_names) != count($_link_sequense))
				return false;	

            # get alt links titles
            foreach ($_link_sequense as $k => $v) {
                $tmp_labels[] = 'multilevel_menu_' . $v;
            }
            $tmp = func_get_languages_alt($tmp_labels);

			foreach ($_link_sequense as $_linkid) {

                if (isset($tmp['multilevel_menu_' . $_linkid])) {
                    $_link_name = $tmp['multilevel_menu_' . $_linkid];
                } else {
				    $_link_name = $_link_names[$_linkid]["link"];
                }
				$link["link_location"][] = array($_link_name, "multilevel_menu.php?linkid=$_linkid");
			
			}

		}


		return $link;	
	}
	return false;
}


#
# delete link data
#
function func_delete_link($linkid) {
	global $sql_tbl;

	$linkpair = func_query_first("SELECT linkid_path, parentid FROM $sql_tbl[multilevel_menu] WHERE linkid='$linkid'");
	if ($linkpair === false) # link is missing
		return 0;

	$linkid_path = $linkpair["linkid_path"];
	$parent_linkid = $linkpair["parentid"];
	
	$deleted_links = func_query_column("SELECT linkid FROM $sql_tbl[multilevel_menu] WHERE linkid='$linkid' OR linkid_path LIKE '$linkid_path/%'");

	if (is_array($deleted_links) && !empty($deleted_links)) {
		db_exec("DELETE FROM $sql_tbl[multilevel_menu] WHERE linkid IN (?)", array($deleted_links));

        foreach ($deleted_links as $k=>$linkid) {
            $label_name = 'multilevel_menu_' .  $linkid;
            db_query("DELETE FROM $sql_tbl[languages_alt] WHERE name='$label_name'");
        }

	}

	
	return $parent_linkid;
}

function func_get_links_list($linkid) {
	global $sql_tbl, $shop_language;
	
	$multilevel_menu = func_query_hash("SELECT * FROM $sql_tbl[multilevel_menu] WHERE parentid = '$linkid' ORDER BY order_by", "linkid", false);

    if (!empty($multilevel_menu)) {

        foreach ($multilevel_menu as $k => $v) {
             $tmp_labels[] = 'multilevel_menu_' . $k;
        }
        $tmp = func_get_languages_alt($tmp_labels);

        foreach ($multilevel_menu as $k => $v) {
            if (isset($tmp['multilevel_menu_' . $k])) {
                $multilevel_menu[$k]['link'] = $tmp['multilevel_menu_' . $k];
            }
        }

    }
	return $multilevel_menu;
}

function func_prepare_multilevel_menu($all_links, $links) {

	foreach ($all_links as $k => $v) {
		if (empty($v['parentid']))
			continue;

	    if (!is_array($all_links[$v['parentid']]['childs'])) {
    	    $all_links[$v['parentid']]['childs'] = array($k => &$all_links[$k]);
	    } else { 
    	    $all_links[$v['parentid']]['childs'][$k] = &$all_links[$k];
	    }

		if (isset($links[$v['parentid']]))
    	    $links[$v['parentid']]['childs'] = $all_links[$v['parentid']]['childs'];

	}

	func_mark_last_links($links);

	return array($all_links, $links);
}

function func_mark_last_links(&$links, $columns = array()) {
	if (empty($links) || !is_array($links))
		return false;

	end($links);
	$last = key($links);
	reset($links);
	$links[$last]['last'] = true;

	foreach ($links as $k => $v) {
		if (!empty($v['childs']) && is_array($v['childs'])) {
			$c = $columns;
			$c[] = !$v['last'];
			func_mark_last_links($links[$k]['childs'], $c);
		}

		$links[$k]['columns'] = $columns;
	}
}

function func_get_full_links_list($linkid = 0) {
	global $sql_tbl;

	$linkid = intval($linkid);

	$all_links = array();
	$links = array();
	$search_condition = array();
	$sort_condition = "";
	$to_search = array();
	
	$search_condition[] = "$sql_tbl[multilevel_menu].avail = 'Y'";

	$sort_condition = " ORDER BY $sql_tbl[multilevel_menu].order_by, link";

	$to_search = "$sql_tbl[multilevel_menu].linkid as lid, $sql_tbl[multilevel_menu].linkid, $sql_tbl[multilevel_menu].parentid, $sql_tbl[multilevel_menu].linkid_path, $sql_tbl[multilevel_menu].link, $sql_tbl[multilevel_menu].avail, $sql_tbl[multilevel_menu].url, $sql_tbl[multilevel_menu].order_by";

	$all_links = func_query_hash("SELECT $to_search FROM $sql_tbl[multilevel_menu] $join_tbl ".(!empty($search_condition) ? "WHERE ".implode(" AND ", $search_condition) : "").(strlen($join_tbl) > 0 ? " GROUP BY $sql_tbl[multilevel_menu].categoryid" : "")." ".$sort_condition, "lid", false);

	if (!is_array($all_links) || empty($all_links))
		return array("all_links" => array(), "links" => array());

    # 
    # GET language label
    #
    foreach ($all_links as $k => $v) {
         $tmp_labels[] = 'multilevel_menu_' . $k;
    }
    
    $tmp = func_get_languages_alt($tmp_labels);

    foreach ($all_links as $k => $v) {
        if (isset($tmp['multilevel_menu_' . $k])) {
            $all_links[$k]['link'] = $tmp['multilevel_menu_' . $k];
        }
    }

	foreach ($all_links as $k => $link) {
		
		# Get the full path for link name
		$link["link_path"] = array();
		foreach (explode("/", $link["linkid_path"]) as $i => $link_id) {
			if (isset($all_links[$link_id]))
				$link["link_path"][] = $all_links[$link_id]['link'];
		}
		if (count($link["link_path"]) - 1 != $i) {
			unset($all_links[$k]);
			continue;
		}
		$link["link_path"] = implode("/", $link["link_path"]);

		if($link["parentid"] == 0)		
			$links[$k] = $link;

		$all_links[$k] = $link;
	}

	return array("all_links" => $all_links, "links" => $links);
}


?>
