<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart Software license agreement                                           |
| Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
| SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
| (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
| FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
| THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
| LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
| INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
| (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
| LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
| NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
| PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
| THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
| SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
| GRANTED BY THIS AGREEMENT.                                                  |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

/**
 * Configuration options processing.
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Ruslan R. Fazlyev <rrf@x-cart.com>
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v8 (xcart_4_7_5), 2016-02-18 13:44:28, configuration.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

if ( !defined('XCART_START') ) { header('Location: ../'); die('Access denied'); }

assert('count($config["Adv_Mailchimp_Subscription"]) === 3 /*Adv_Mailchimp_Subscription count settings are changed Adv_Mailchimp_Subscription. Update the modules/Adv_Mailchimp_Subscription/configuration.php file*/');

if ($REQUEST_METHOD == 'POST') {

    if (empty($adv_mailchimp_apikey)) {

    	$top_message['content'] = func_get_langvar_by_name('txt_please_enter_all_required_info');
        func_header_location('configuration.php?option=Adv_Mailchimp_Subscription');

    } else {

       $adv_mailchimp_lists = func_mailchimp_get_lists(0, $adv_mailchimp_apikey);
       if (!empty($adv_mailchimp_lists['Error_message'])) {
           $top_message['content'] = func_get_langvar_by_name(
                "txt_mailchimp_error_txt",
                array(
                    "error_txt" => $adv_mailchimp_lists['Error_message'],
                )
            );
           func_header_location("configuration.php?option=Adv_Mailchimp_Subscription");

        } else {

            if (!empty($adv_mailchimp_lists)) {
                $top_message['content'] = func_get_langvar_by_name('msg_adm_mailchimp_connection_configured');
            }

            func_array2update(
                'config',
                array(
                    'value' => $adv_mailchimp_apikey,
                ),
                "name = 'adv_mailchimp_apikey' AND category = 'Adv_Mailchimp_Subscription'"
            );

            func_array2update(
                'config',
                array(
                    'value' => !empty($adv_mailchimp_analytics) && $adv_mailchimp_analytics == 'on' ? 'Y' : '',
                ),
                "name = 'adv_mailchimp_analytics' AND category = 'Adv_Mailchimp_Subscription'"
            );

            func_array2update(
                'config',
                array(
                    'value' => !empty($adv_mailchimp_register_opt) && $adv_mailchimp_register_opt == 'on' ? 'Y' : '',
                ),
                "name = 'adv_mailchimp_register_opt' AND category = 'Adv_Mailchimp_Subscription'"
            );

        }

    }

}

?>
