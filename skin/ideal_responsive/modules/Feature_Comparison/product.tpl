{*
ef73dfc708dc8113b87c8e77f2cb6fa8f5ff7a3c, v1 (xcart_4_6_0), 2013-05-27 15:54:49, product.tpl, random
vim: set ts=2 sw=2 sts=2 et:
*}
{foreach from=$product.features.options item=v}
  <div class="property-name" nowrap="nowrap">
  {include file="modules/Feature_Comparison/option_hint.tpl" opt=$v}
  </div>
  <div class="property-value" valign="top" colspan="2">

    {if $v.option_type eq 'S'}

      {$v.variants[$v.value].variant_name}

    {elseif $v.option_type eq 'M'}

      {foreach from=$v.variants item=o}
        {if $o.selected ne ''}
          {$o.variant_name}<br />
        {/if}
      {/foreach}

    {elseif $v.option_type eq 'B'}

      {if $v.value eq 'Y'}
        {$lng.lbl_yes}
      {else}
        {$lng.lbl_no}
      {/if}

    {elseif ($v.option_type eq 'N' or $v.option_type eq 'D') and $v.value ne ''}

      {$v.formated_value}

    {else}

      {$v.value|replace:"\n":"<br />"}

    {/if}

  </div>
  <div class="separator"></div>
{/foreach}
