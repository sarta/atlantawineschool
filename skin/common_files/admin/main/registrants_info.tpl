{* 
$Id: registrants_info.tpl,v 1 2016/05/05 kolbasa Exp $
vim: set ts=2 sw=2 sts=2 et:
*}
{include file="page_title.tpl" title=$lng.lbl_registrants_info}
<br />

{$lng.txt_registrants_info_top_text}

<br /><br />

<script type="text/javascript" src="{$SkinDir}/js/popup_product_with_variants.js"></script>

{capture name=dialog}

<form action="registrants_info.php" method="post" name="registrantsinfoform">
<input type="hidden" name="mode" value="make_report" />

<table width="100%">
<tr>
  <td class="OptionLabel" align="left" width="10"><label>{$lng.lbl_product}:</label></td>
  <td align="left">
    <input type="hidden" name="productcode" value="" />
    <input type="text" readonly="readonly" size="25" name="productname" value="{$coupon_data.productname|escape}" />
    <input type="button" onclick="javascript: popup_product_with_variants('registrantsinfoform.productcode','registrantsinfoform.productname');" value="{$lng.lbl_browse_|strip_tags:false|escape}" />
  </td>
</tr>

<tr>
  <td colspan="2" class="SubmitBox">
  <input type="submit" value="{$lng.lbl_make_report|strip_tags:false|escape}" />
  </td>
</tr>
</table>
</form>

{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog extra='width="100%"'}
