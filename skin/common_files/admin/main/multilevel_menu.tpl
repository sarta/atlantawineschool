{include file="page_title.tpl" title=$lng.lbl_multilevel_menu}

{capture name=dialog}

{include file="main/language_selector.tpl" script="multilevel_menu.php?"}

{include file="admin/main/multilevel_menu_location.tpl"}

<form action="multilevel_menu.php" method="post" name="multilevelmenuform">
<input type="hidden" name="linkid" value="{$smarty.get.linkid}" />


<table cellpadding="2" cellspacing="1" width="100%">

<tr class="TableHead">
  <td>&nbsp;</td>
  <td>{$lng.lbl_pos}</td>
  <td>{$lng.lbl_link_title}</td>
  <td align="center">{$lng.lbl_url}</td>
  <td align="center">{$lng.lbl_sublinks}</td>
  <td align="center">{$lng.lbl_enabled}</td>
</tr>

{assign var="menuid_selected" value=0}

{if $multilevel_menu}

{foreach from=$multilevel_menu item=l key=link_id}

<tr{cycle values=', class="TableSubHead"'}>
  <td width="1%"><input type="checkbox" name="to_delete[{$link_id}]" value="Y" /></td>
  <td width="1%"><input type="text" size="3" name="posted_data[{$link_id}][order_by]" maxlength="3" value="{$l.order_by}" /></td>
  <td><input type="text" style="width:95%;" name="posted_data[{$link_id}][link]" maxlength="255" value="{$l.link}" /></td>
  <td align="center">
    <input type="text" style="width:95%;" name="posted_data[{$link_id}][url]" maxlength="255" value="{$l.url}" /><a href="multilevel_menu.php?linkid={$link_id}">
  </td>
  <td align="center"><a href="multilevel_menu.php?linkid={$link_id}">{$lng.lbl_add}/{$lng.lbl_modify}</a></td>
  <td align="center">
  <select name="posted_data[{$link_id}][avail]">
    <option value="Y"{if $l.avail eq "Y"} selected="selected"{/if}>{$lng.lbl_yes}</option>
    <option value="N"{if $l.avail eq "N"} selected="selected"{/if}>{$lng.lbl_no}</option>
  </select>
  </td>
</tr>

{/foreach}

<tr>
  <td colspan="6" class="SubmitBox">
      <input type="submit" value="{$lng.lbl_update|strip_tags:false|escape}" onclick="javascript: submitForm(this, 'update');" />
      <input type="button" value="{$lng.lbl_delete_selected|strip_tags:false|escape}" onclick="javascript: submitForm(this, 'delete');" />
  </td>
</tr>

{else}

<tr>
  <td colspan="6" align="center"><br />{$lng.txt_no_links}</td>
</tr>

{/if}


<tr>
  <td colspan="6"><br /><br />{include file="main/subheader.tpl" title=$lng.txt_add_new_link}</td>
<tr>

<tr>
  <td>&nbsp;</td>
  <td><input type="text" size="3" maxlength="5" name="new_link[order_by]" value="{$new_order_by}" /></td>
  <td><input type="text" size="45" style="width:95%;" name="new_link[link]" /></td>
  <td><input type="text" size="45" style="width:95%;" name="new_link[url]" value="{$http_location}/" /></td>
  <td align="center">{$lng.txt_not_available}</td>
  <td align="center">
    <select name="new_link[avail]">
      <option value="Y" selected="selected">{$lng.lbl_yes}</option>
      <option value="N">{$lng.lbl_no}</option>
    </select>
  </td>
</tr>
<tr>
  <td colspan="6">
    <input type="button" value="{$lng.lbl_add}" onclick="javascript: submitForm(this, 'add')" />
  </td>
</tr>

</table>

<input type="hidden" name="mode" value="update" />
</form>



{/capture}
{include file="dialog.tpl" title=$lng.lbl_links content=$smarty.capture.dialog extra='width="100%"'}