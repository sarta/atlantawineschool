{*
$Id: location.tpl,v 1.5 2008/08/21 09:52:42 max Exp $
vim: set ts=2 sw=2 sts=2 et:
*}
{literal}
    <style>
        span.separator:last-of-type{display: none}
    </style> {/literal}
{if $link_location and $linkid ne ""}
<div class="NavigationPath">
{strip}
{section name=position loop=$link_location}
{if $link_location[position].1 ne "" }<a href="{$link_location[position].1|amp}" class="NavigationPath">{/if}
{$link_location[position].0}
{if $link_location[position].1 ne "" }</a>{/if}
<span class="separator">&nbsp;::&nbsp;</span>
{/section}
</div>
{/strip}
<br />
{/if}
<br />