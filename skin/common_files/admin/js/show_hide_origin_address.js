/* vim: set ts=2 sw=2 sts=2 et: */
/**
 * Show/Hide Company shipfrom address section on configuration.php?option=Company page
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage JS Library
 * @author     Ruslan R. Fazlyev <rrf@x-cart.com>
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @version    22207578af238b5c789d9591b49b9ceecc36b118, v1 (xcart_4_7_5), 2016-01-28 13:31:49, show_hide_origin_address.js, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */
function func_toggle_shipfrom_address(duration, is_hide) {
  if (is_hide) {
    $("tr[id^='tr_shipfrom_']").not("#tr_shipfrom_address_separator,#tr_shipfrom_address_as_company_one").fadeOut( duration );
  } else {
    $("tr[id^='tr_shipfrom_']").not("#tr_shipfrom_address_separator,#tr_shipfrom_address_as_company_one").fadeIn( duration );
  }
}

$(document).ready(
  function() {

    $('#opt_shipfrom_address_as_company_one').change(function() {
      func_toggle_shipfrom_address(200, this.checked);
    });

    func_toggle_shipfrom_address(0, $('#opt_shipfrom_address_as_company_one').is(':checked'));
  }
);
