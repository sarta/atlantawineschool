{*
$Id: back_in_stock.tpl,v 1 2013/10/02 11:31:16 kolbasa Exp $
vim: set ts=2 sw=2 sts=2 et:
*}
{config_load file="$skin_config"}
{*include file="mail/html/mail_header.tpl"*}

<br />The <a href="{$http_location}/product.php?productid={$product.productid}">{$product.product|escape}</a> is back in stock.
<br />

{include file="mail/html/signature.tpl"}
