/* vim: set ts=2 sw=2 sts=2 et: */
/**
 * 
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage JS Library
 * @author     Ruslan R. Fazlyev <rrf@x-cart.com> 
 * @version    $Id: popup_products_with_variants.js,v 1 2013/10/02 kolbasa Exp $
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

function popup_product_with_variants(field_productid, field_product, only_regular) {
  return popupOpen(
    'popup_product_with_variants.php?field_productid=' + field_productid + '&field_product=' + field_product + '&only_regular=' + only_regular,
    '',
    { 
      width: 800,
      height: 600,
      draggable: true
    }
  );
}
