{*
5ecadf95fd8040867b954dc580dae7d18e4e8c6e, v4 (xcart_4_7_4), 2015-10-27 19:43:25, accounting_adv.tpl, aim

vim: set ts=2 sw=2 sts=2 et:
*}

{capture name=dialog}

  <h1 class="title" id="page-title" >Accounting</h1>
  <div class="accounting-page-wrapper">
    <div class="description">
      <p>Choose your accounting system below and configure it.</p>
      <p>Not seeing your accounting software here? <a href="http://ideas.x-cart.com/forums/32109-x-cart-classic-4-x?utm_source=xcart&amp;utm_medium=help_menu_link&amp;utm_campaign=help_menu" target="_blank">Let us know</a></p>
    </div>

    <div class="available-block">
      <ul class="available-block-list">
        <li>
          <a href="http://marketplace.x-cart.com/addons-modules/store-management/orders/Unify-by-Webgility?utm_source=xcart&amp;utm_medium=help_menu_link&amp;utm_campaign=help_menu" class="marketplace-search" target="_blank">
            <img src="{$ImagesDir}/adv/accounting/qb_logo.jpg" alt="QuickBooks" />
            <span class="link-wrapper">QuickBooks</span>
          </a>
        </li>
        <li>
          <a href="http://marketplace.x-cart.com/addons-modules/store-management/orders/Unify-by-Webgility?utm_source=xcart&amp;utm_medium=help_menu_link&amp;utm_campaign=help_menu" class="marketplace-search" target="_blank">
            <img src="{$ImagesDir}/adv/accounting/xero_logo.jpg" alt="Xero integration" />
            <span class="link-wrapper">Xero integration</span>
          </a>
        </li>
        <li>
          <a href="orders.php" class="marketplace-search">
            <img src="{$ImagesDir}/adv/accounting/export.jpg" alt="Export orders" />
            <span class="link-wrapper">Export orders</span>
          </a>
        </li>
      </ul>
    </div>
  </div>

{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog extra='width="100%"'}
