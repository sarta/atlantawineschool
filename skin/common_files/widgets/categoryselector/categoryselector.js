/* vim: set ts=2 sw=2 sts=2 et: */
/**
 * Javascript code for categoryselector widget
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Skin
 * @author     Michael Bugrov
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v4 (xcart_4_7_5), 2016-02-18 13:44:28, categoryselector.js, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */
$(document).ready(function () {
    var elements = $('select.categoryselector');

    $.each(elements, function () {
        var element = $(this);
        var config = JSON.parse(element.attr('data-categoryselectorconfig').trim()) || {};

        element.select2(config);
    });
});
