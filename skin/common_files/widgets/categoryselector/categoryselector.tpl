{*
09d46970de4b3605245381ed8ecd8b55fffd1d3e, v4 (xcart_4_7_5), 2016-02-12 18:28:36, categoryselector.tpl, aim

vim: set ts=2 sw=2 sts=2 et:
*}

{*
  Supported params:

    $name - select control name
    $class - class to add to select element
    $extra - extra attributes to add to select element

    $display_empty - epmpty option selector
    $selected_id - selected option
    $selected_ids - selected options list
*}

{include file="widgets/css_loader.tpl" css="lib/select2/css/select2.min.css"}
{load_defer file="lib/select2/js/select2.min.js" type="js"}
{load_defer file="lib/select2/js/i18n/$shop_language.js" type="js"}

{include file="widgets/categoryselector/config.tpl" assign="categoryselectorconfig"}

{include file="widgets/css_loader.tpl" css="widgets/categoryselector/categoryselector.css"}
{load_defer file="widgets/categoryselector/categoryselector.js" type="js"}

<select name="{$name|default:"categoryid"}" class="categoryselector{if $class} {$class}{/if}" data-categoryselectorconfig="{$categoryselectorconfig|escape}" {$extra}>
  {if $display_empty eq 'P'}
    <option value="">{$lng.lbl_please_select_category}</option>
  {elseif $display_empty eq 'E'}
    <option value="">&nbsp;</option>
  {/if}
  {foreach from=$allcategories item=c key=catid}
    {if $selected_ids}
      <option value="{$catid}"{if $selected_ids[$catid]} selected="selected"{/if}>{$c}</option>
    {else}
      <option value="{$catid}"{if $selected_id eq $catid} selected="selected"{/if} title="{$c|cat:' (id:'|cat:$catid|strip_tags:false|escape})">{$c|amp}</option>
    {/if}
  {/foreach}
</select>
