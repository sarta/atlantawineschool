{*
09d46970de4b3605245381ed8ecd8b55fffd1d3e, v4 (xcart_4_7_5), 2016-02-12 18:28:36, css_loader.tpl, aim

vim: set ts=2 sw=2 sts=2 et:
*}

{*
  Supported params:

    $css - css file name
    $type - { link, style } default link
*}

{load_defer file="widgets/css_loader.js" type="js"}

<script type="text/javascript">//<![CDATA[
  if (typeof xc_load_css !== 'undefined') {
    xc_load_css('{$SkinDir}/{$css}', '{$type|default:"link"}');
  } else {
    $(document).ready(function () {
      xc_load_css('{$SkinDir}/{$css}', '{$type|default:"link"}');
    });
  }
//]]></script>
