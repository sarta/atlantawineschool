/* vim: set ts=2 sw=2 sts=2 et: */
/**
 * Javascript widget css loader
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Skin
 * @author     Michael Bugrov
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @version    09d46970de4b3605245381ed8ecd8b55fffd1d3e, v3 (xcart_4_7_5), 2016-02-12 18:28:36, css_loader.js, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

/**
 * Load CSS file
 *
 * @param {string} file
 * @param {string} type
 *
 * @returns void
 */
function xc_load_css(file, type) {
    var element;

    appendElement = function(element, file) {
        if (
            $('link[href="' + file + '"]').length === 0
            && $('style[file="' + file + '"]').length === 0
        ) {
            $('head').append(element);
        }
    };

    switch (type) {
        case 'link':
            element = '<link rel="stylesheet" type="text/css" href="' + file + '"></link>';
            appendElement(element, file);
            break;
        case 'style':
            $.get(file).done(function (content) {
                element = '<style file="' + file + '">' + content + '</style>';
                appendElement(element, file);
            });
            break;
    }
};
