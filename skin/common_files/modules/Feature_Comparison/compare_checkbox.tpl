{*
bbd5b07dba7e5be66f64bc1fe0308ec979c6b8d5, v2 (xcart_4_6_1), 2013-09-11 18:21:42, compare_checkbox.tpl, aim
vim: set ts=2 sw=2 sts=2 et:
*}
{if $product.fclassid gt 0 and $products_has_fclasses}

<div class="fcomp-checkbox-box">

  <label for="fe_pid_{$product.productid}">
    <input type="checkbox" id="fe_pid_{$product.productid}" value="Y" />
    {$lng.lbl_check_to_compare}
  </label>

</div>

{/if}
