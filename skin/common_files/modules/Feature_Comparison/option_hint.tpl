{*
545ef28db2b8c109d4f91b0a1e7d2bb6a7a1bb39, v3 (xcart_4_4_1), 2010-08-17 13:03:39, option_hint.tpl, igoryan
vim: set ts=2 sw=2 sts=2 et:
*}
{if $opt.option_hint}
  {include file="main/tooltip_js.tpl" title=$opt.option_name text=$opt.option_hint id="fc_opt_help_`$opt.foptionid`" class="help-link"}
{else}
  {$opt.option_name}
{/if}
