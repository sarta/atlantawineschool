{*
cbd5b33537a292f5a3d487cfe98638e2e7f94a89, v5 (xcart_4_7_5), 2016-02-08 20:36:40, choosing_options_list.tpl, aim

vim: set ts=2 sw=2 sts=2 et:
*}

<h1>{$lng.lbl_X_features|substitute:product_type:$current_class}</h1>

<p class="text-block">{$lng.txt_choosing_by_features_note_step2}</p>
 
{capture name=dialog} 

  <p>
    <strong>{$lng.lbl_note}:</strong>&nbsp;{$lng.txt_not_necessary_fill_all_fields}
  </p>

  <form action="choosing.php" method="post" name="choosingoptionsform">
    <input type="hidden" name="mode" value="select_options" />

    <table cellspacing="0" class="data-table width-100 fcomp-options-table" summary="{$lng.lbl_select_features|escape}">

      {* for html_select_date below *}
      {$_month_names = ['1'=>$lng.lbl_month_fullname_1,'2'=>$lng.lbl_month_fullname_2, '3'=>$lng.lbl_month_fullname_3, '4'=>$lng.lbl_month_fullname_4, '5'=>$lng.lbl_month_fullname_5, '6'=>$lng.lbl_month_fullname_6, '7'=>$lng.lbl_month_fullname_7, '8'=>$lng.lbl_month_fullname_8, '9'=>$lng.lbl_month_fullname_9, '10'=>$lng.lbl_month_fullname_10, '11'=>$lng.lbl_month_fullname_11, '12'=>$lng.lbl_month_fullname_12]}
      {foreach from=$options item=v}
        <tr>
          <td class="data-name">
          {include file="modules/Feature_Comparison/option_hint.tpl" opt=$v}
          </td>
          <td class="data-width-100">

            {if $v.option_type eq 'S' or  $v.option_type eq 'M'}

              {if $v.option_type eq 'S'}
                <select name="options[{$v.foptionid}]">
                  <option value="">{$lng.lbl_ignored}</option>
              {else}
                <select name="options[{$v.foptionid}][]" multiple="multiple" size="5">
              {/if}

              {foreach from=$v.variants item=vv}
                <option value="{$vv.fvariantid}"{if $v.option_type eq 'M'}{foreach from=$choosing.options[$v.foptionid] item=s}{if $s eq $vv.fvariantid} selected="selected"{/if}{/foreach}{else}{if $choosing.options[$v.foptionid] eq $vv.fvariantid and $choosing.options[$v.foptionid]|cat:"|" ne '|'} selected="selected"{/if}{/if}>{$vv.variant_name}</option>
              {/foreach}

              </select>

              {if $v.option_type eq 'M'}
                <div class="fcomp-options-label-including">
                  <label>
                  {$lng.lbl_fcomp_search_for_products}
                  <input type="radio" name="including[{$v.foptionid}]" value="any"{if $choosing.including[$v.foptionid] eq "any" or $choosing.including[$v.foptionid] eq ""} checked="checked"{/if} />
                  {$lng.lbl_fcomp_any_selected_option}
                  </label>
&nbsp;
                  <label>
                  <input type="radio" name="including[{$v.foptionid}]" value="all"{if $choosing.including[$v.foptionid] eq "all"} checked="checked"{/if}  />
                  {$lng.lbl_fcomp_all_selected_options}
                  </label>
  &nbsp;
                  <br />
                  {$lng.lbl_hold_ctrl_key}
                </div>
              {/if}

            {elseif $v.option_type eq 'N'}

              <input type="text" name="options[{$v.foptionid}][0]" maxlength="11" size="11" value="{$choosing.options[$v.foptionid].0}"{if not $choosing.options[$v.foptionid].0} class="default-value"{/if} placeholder="{$lng.lbl_from|strip_tags:false|escape}" />
              &nbsp;-&nbsp;
              <input type="text" name="options[{$v.foptionid}][1]" maxlength="11" size="11" value="{$choosing.options[$v.foptionid].1}"{if not $choosing.options[$v.foptionid].1} class="default-value"{/if} placeholder="{$lng.lbl_to|strip_tags:false|escape}" />

            {elseif $v.option_type eq 'D'}

              <table cellspacing="0" cellpadding="0" summary="{$v.option_name|escape}">
                {if $choosing.options[$v.foptionid].0 ne 0}
                  {assign var="cur_time" value=$choosing.options[$v.foptionid].0}
                {else}
                  {assign var="cur_time" value=null}
                {/if} 
                <tr>
                  <td>{$lng.lbl_from}:</td>
                  <td>{html_select_date field_array="options[`$v.foptionid`][0]" start_year="-5" year_empty=$lng.lbl_ignored|wm_remove month_empty=$lng.lbl_ignored|wm_remove day_empty=$lng.lbl_ignored|wm_remove time=$cur_time month_names=$_month_names}</td>
                </tr>

                {if $choosing.options[$v.foptionid].1 ne 0}
                  {assign var="cur_time" value=$choosing.options[$v.foptionid].1}
                {else}
                  {assign var="cur_time" value=null}
                {/if} 
                <tr>
                  <td>{$lng.lbl_through}:</td>
                  <td>{html_select_date field_array="options[`$v.foptionid`][1]" start_year="+5" year_empty=$lng.lbl_ignored|wm_remove month_empty=$lng.lbl_ignored|wm_remove day_empty=$lng.lbl_ignored|wm_remove time=$cur_time month_names=$_month_names}</td>
                </tr>
              </table>

            {elseif $v.option_type eq 'B'}
              <select name="options[{$v.foptionid}]">
                <option value=""{if $choosing.options[$v.foptionid] eq ""} selected="selected"{/if}>{$lng.lbl_ignored}</option>
                <option value="Y"{if $choosing.options[$v.foptionid] eq "Y"} selected="selected"{/if}>{$lng.lbl_yes}</option>
                <option value="N"{if $choosing.options[$v.foptionid] eq "N"} selected="selected"{/if}>{$lng.lbl_no}</option>
               </select>

            {else}
              <input type="text" size="40" name="options[{$v.foptionid}]" value="{$choosing.options[$v.foptionid]}" /><br />
                <div class="fcomp-options-label-including">
                <label>
                  <input type="radio" name="including[{$v.foptionid}]" value="all"{if $choosing.including[$v.foptionid] eq "all" or $choosing.including[$v.foptionid] eq ""} checked="checked"{/if} />
                  {$lng.lbl_all_word}
                </label>
&nbsp;
                <label>
                  <input type="radio" name="including[{$v.foptionid}]" value="any"{if $choosing.including[$v.foptionid] eq "any"} checked="checked"{/if}  />
                  {$lng.lbl_any_word}
                </label>
&nbsp;
                <label>
                  <input type="radio" name="including[{$v.foptionid}]" value="phrase" {if $choosing.including[$v.foptionid] eq "phrase"} checked="checked"{/if} />
                  {$lng.lbl_exact_phrase}
                </label>
                </div>
            {/if}

           </td>
        </tr>
      {/foreach}

      <tr>
        <td>&nbsp;</td>
        <td class="button-row">
          {include file="customer/buttons/search.tpl" type="input"}
        </td>
      </tr>
    </table>
  </form>

{/capture} 
{include file="customer/dialog.tpl" title=$section_head|default:$lng.lbl_select_features content=$smarty.capture.dialog noborder=true}
