{*
626362abdced0086c1c116748026db88b0984243, v1 (xcart_4_7_4), 2015-10-01 14:20:13, order_info.tpl, mixon

vim: set ts=2 sw=2 sts=2 et:
*}
{if $order.{XCPitneyBowesDefs::CART_ORDERS}}
<tr>
  <td colspan="2" valign="top" height="10">&nbsp;</td>
</tr>
<tr valign="top">
  <td colspan="2"><i>{$order.shipping|trademark}:</i></td>
</tr>
  {foreach from=$order.{XCPitneyBowesDefs::CART_ORDERS} item=pbOrder}
<tr>
  <td valign="top">&nbsp;&nbsp;{$lng.lbl_pitney_bowes_orderid}</td>
  <td valign="top">{$pbOrder->orderId}</td>
</tr>
<tr>
  <td valign="top">&nbsp;&nbsp;{$lng.lbl_pitney_bowes_transportation_part}</td>
  <td valign="top">{currency value=$pbOrder->order->totalTransportation->total->value}</td>
</tr>
<tr>
  <td valign="top">&nbsp;&nbsp;{$lng.lbl_pitney_bowes_importation_part}</td>
  <td valign="top">{currency value=$pbOrder->order->totalImportation->total->value}</td>
</tr>
<tr>
  <td colspan="2" valign="top" height="10">&nbsp;</td>
</tr>
<tr>
  <td valign="top">&nbsp;&nbsp;{$lng.lbl_pitney_bowes_hub_id}</td>
  <td valign="top">{$pbOrder->shipToHub->hubId}</td>
</tr>
<tr>
  <td valign="top">&nbsp;&nbsp;{$lng.lbl_pitney_bowes_hub_address}</td>
  <td valign="top"></td>
</tr>
<tr>
  <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;{$lng.lbl_address}</td>
  <td valign="top">
    {$pbOrder->shipToHub->hubAddress->street1}
  </td>
</tr>
<tr>
  <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;{$lng.lbl_address_2}</td>
  <td valign="top">
    {$pbOrder->shipToHub->hubAddress->street2}
  </td>
</tr>
<tr>
  <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;{$lng.lbl_city}</td>
  <td valign="top">
    {$pbOrder->shipToHub->hubAddress->city}
  </td>
</tr>
<tr>
  <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;{$lng.lbl_state}</td>
  <td valign="top">
    {$pbOrder->shipToHub->hubAddress->provinceOrState}
  </td>
</tr>
<tr>
  <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;{$lng.lbl_zip_code}</td>
  <td valign="top">
    {$pbOrder->shipToHub->hubAddress->postalOrZipCode}
  </td>
</tr>
<tr>
  <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;{$lng.lbl_country}</td>
  <td valign="top">
    {$pbOrder->shipToHub->hubAddress->country}
  </td>
</tr>
<tr>
  <td colspan="2" valign="top" height="10">&nbsp;</td>
</tr>
  {/foreach}
{/if}
