{*
9b4c5a1d6dc9993562ed55f6d968d2168665ac49, v1 (xcart_4_7_4), 2015-10-07 09:22:24, product_restrictions.tpl, mixon

vim: set ts=2 sw=2 sts=2 et:
*}

{capture name=restrictions}

  <table width="100%">
    <tr>
      <td colspan="4"></td>
    </tr>

    <tr class="TableHead">
      <th></th>
      <th>{$lng.lbl_pitney_bowes_country}</th>
      <th>{$lng.lbl_pitney_bowes_restriction_code}</th>
      <th></th>
    </tr>

    {foreach from=$restrictions item=r}
      <tr>
        <td></td>
        <td>{$lng.{"country_`$r.country`"}}</td>
        <td>{$r.restriction_code}</td>
        <td></td>
      </tr>
    {foreachelse}
      <tr>
        <td colspan="4" align="center">{$lng.lbl_pitney_bowes_no_restrictions}</td>
      </tr>
    {/foreach}

    <tr>
      <td colspan="4"></td>
    </tr>
  </table>

{/capture}

{include file="customer/menu_dialog.tpl" title=$lng.lbl_pitney_bowes_product_restrictions content=$smarty.capture.restrictions}
