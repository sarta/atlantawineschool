{*
626362abdced0086c1c116748026db88b0984243, v1 (xcart_4_7_4), 2015-10-01 14:20:13, catalog.tpl, mixon

vim: set ts=2 sw=2 sts=2 et:
*}
<form action="configuration.php?option={$option|escape}&amp;{XCPitneyBowesDefs::CONTROLLER}={$controller}" method="post" name="{$controller}form">

  <table width="100%">

    <tr>
      <td>
        {include file="modules/Pitney_Bowes/catalog_logo.tpl"}
      </td>
    </tr>

    <tr>
      <td><h2>{$lng.lbl_pitney_bowes_manual_catalog_extraction}</h2></td>
    </tr>

    <tr>
      <td><p>{$lng.txt_pitney_bowes_catalog_sync_note}</p></td>
    </tr>

    <tr>
      <td>
        <ul class="pb-export-type">
          <li>
            <label><input value="{XCPitneyBowesDefs::EXPORT_TYPE_FULL}" type="radio" name="{XCPitneyBowesDefs::EXPORT_TYPE}"{if $export_type eq XCPitneyBowesDefs::EXPORT_TYPE_FULL or $export_type eq ''}checked="checked"{/if}>{$lng.lbl_pitney_bowes_full}</label>
          </li>
          <li>
            <label><input value="{XCPitneyBowesDefs::EXPORT_TYPE_DIFF}" type="radio" name="{XCPitneyBowesDefs::EXPORT_TYPE}"{if $export_type eq XCPitneyBowesDefs::EXPORT_TYPE_DIFF}checked="checked"{/if}>{$lng.lbl_pitney_bowes_diff}</label>
          </li>
        </ul>
      </td>
    </tr>

    <tr>
      <td>
        <div class="main-button">
          <input type="submit" class="big-main-button configure-style" value="{$lng.lbl_pitney_bowes_start_export}" />
        </div>
      </td>
    </tr>

  </table>
</form>

<table width="100%">

  <tr>
    <td colspan="4"><br />{include file="main/subheader.tpl" title=$lng.lbl_pitney_bowes_last_exports}</td>
  </tr>

  <tr class="TableHead">
    <td width="5%">&nbsp;</td>
    <td width="50%">{$lng.lbl_pitney_bowes_type}</td>
    <td width="30%">{$lng.lbl_pitney_bowes_date}</td>
    <td width="15%">{$lng.lbl_pitney_bowes_status}</td>
  </tr>

  {foreach from=$export_items item=item}
    <tr{cycle name=$type values=", class='TableSubHead'"}>
      <td>&nbsp;</td>
      <td align="center">{$item.type|escape}</td>
      <td align="center">{$item.export_date|escape|date_format:$config.Appearance.date_format}</td>
      <td align="center">{$item.status|escape}</td>
    </tr>
  {foreachelse}
    <tr>
      <td colspan="4" align="center">{$lng.lbl_pitney_bowes_no_exports}</td>
    </tr>
  {/foreach}

  <tr>
    <td colspan="4"><hr /></td>
  </tr>
  <tr>
    <td colspan="4" align="right">
      <form action="configuration.php?option={$option|escape}&amp;{XCPitneyBowesDefs::CONTROLLER}=Check" method="post" name="Checkform">
      <div class="main-button">
        <input type="submit" class="big-main-button configure-style" value="{$lng.lbl_pitney_bowes_check_status}" />
      </div>
      </form>
    </td>
  </tr>

</table>

{include file="main/navigation.tpl"}
