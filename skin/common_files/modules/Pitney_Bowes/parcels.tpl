{*
626362abdced0086c1c116748026db88b0984243, v1 (xcart_4_7_4), 2015-10-01 14:20:13, parcels.tpl, mixon

vim: set ts=2 sw=2 sts=2 et:
*}

{load_defer file="modules/Pitney_Bowes/controller.js" type="js"}

{capture name=dialog}

  <table width="100%">
    {if $order.{XCPitneyBowesDefs::CART_ORDERS}}
      <tr>
        <td colspan="6" height="10">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="6">
          <b>{$lng.lbl_pitney_bowes_order_number}:</b><br/><br/>
          <select class="pb-order-number">
            {foreach from=${XCPitneyBowesDefs::ORDERS_LIST} item=order name=orders}
              <option {if ${XCPitneyBowesDefs::PARCEL_ORDER_NUMBER} eq $order.{XCPitneyBowesDefs::PARCEL_ORDER_NUMBER}}selected="selected"{/if} value="{$order.{XCPitneyBowesDefs::PARCEL_HUB_ADDRESS}|escape}">{$order.{XCPitneyBowesDefs::PARCEL_ORDER_NUMBER}|escape}</option>
            {/foreach}
          </select>
        </td>
      </tr>
      <tr>
        <td colspan="6" height="10">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="6">
          <b>{$lng.lbl_pitney_bowes_hub_address}:</b><br/><br/>
          <textarea class="pb-hub-address">{$hub_address}</textarea>
        </td>
      </tr>
      <tr>
        <td colspan="6" height="10">&nbsp;</td>
      </tr>
    {/if}
    <tr>
      <td colspan="6"></td>
    </tr>
    <tr class="TableHead">
      <th></th>
      <th>{$lng.lbl_pitney_bowes_parcel_number}</th>
      <th>{$lng.lbl_items}</th>
      <th>{$lng.lbl_pitney_bowes_asn}</th>
      <th></th>
      <th></th>
    </tr>
    {foreach from=$parcels item=parcel name=parcels}
      <tr{cycle values=', class="TableSubHead"'}>
        <td></td>
        <td>{$parcel.parcel_number}</td>
        <td align="center">
          <form action="{$navigation_script}" method="post" name="{$controller}form1">
            <input type="hidden" name="action" value="{XCPitneyBowesDefs::PARCEL_ACTION_EDIT_ITEMS}"/>
            <input type="hidden" name="parcelid" value="{$parcel.id}"/>
            {if $createParcelAllowed and $parcel.createAsnCalled eq XCPitneyBowesDefs::ASN_STATUS_PENDING}
              <input type="submit" value="{$lng.lbl_pitney_bowes_edit_parcel}"/>
            {else}
              <input type="hidden" name="action" value="{XCPitneyBowesDefs::PARCEL_ACTION_VIEW_ITEMS}"/>
              <input type="hidden" name="parcelid" value="{$parcel.id}"/>
              <input type="submit" value="{$lng.lbl_pitney_bowes_view_parcel}"/>
            {/if}
          </form>
        </td>
        <td align="center">
          {if $parcel.createAsnCalled eq XCPitneyBowesDefs::ASN_STATUS_PENDING}
            {if $parcel.items|count gt 0}
              <form action="{$navigation_script}" method="post" name="{$controller}form2">
                <input type="hidden" name="action" value="{XCPitneyBowesDefs::PARCEL_ACTION_SEND_ASN}"/>
                <input type="hidden" name="parcelid" value="{$parcel.id}"/>
                <input type="hidden" name="order_number" value=""/>
                <input type="submit" value="{$lng.lbl_pitney_bowes_send_asn}" />
              </form>
            {else}
              <span>{$lng.lbl_pitney_bowes_no_parcel_items}</span>
            {/if}
          {else}
            <a href="{pb_track_url tracking_number=$parcel.asn_number}" target="_blank" title="{$lng.lbl_pitney_bowes_asn_tracking}">{$parcel.asn_number}</a>
          {/if}
        </td>
        <td align="center">
          {if $parcel.items|count gt 0}
            <form action="{$navigation_script}" method="post" name="{$controller}form3" target="_blank">
              <input type="hidden" name="action" value="{XCPitneyBowesDefs::PARCEL_ACTION_PRINT_LABEL}"/>
              <input type="hidden" name="parcelid" value="{$parcel.id}"/>
              <input type="hidden" name="order_number" value=""/>
              <input type="hidden" name="hub_address" value=""/>
              <input type="submit" value="{$lng.lbl_pitney_bowes_print_label}" class="pb-print-label-btn" />
            </form>
          {else}
            <span>{$lng.lbl_pitney_bowes_no_parcel_items}</span>
          {/if}
        </td>
        <td align="center">
          {if $parcel.createAsnCalled eq XCPitneyBowesDefs::ASN_STATUS_PENDING}
            <form action="{$navigation_script}" method="post" name="{$controller}form4">
              <input type="hidden" name="action" value="{XCPitneyBowesDefs::PARCEL_ACTION_DELETE_PARCEL}"/>
              <input type="hidden" name="parcelid" value="{$parcel.id}"/>
              <input type="submit" value="&nbsp;" title="{$lng.lbl_pitney_bowes_delete_parcel}" data-question="{$lng.msg_pitney_bowes_q_delete_parcel}" class="pb-delete-parcel-btn" />
            </form>
          {/if}
        </td>
      </tr>
    {foreachelse}
      <tr>
        <td colspan="6" align="center">{$lng.lbl_pitney_bowes_no_parcels}</td>
      </tr>
    {/foreach}
    <tr>
      <td colspan="6"></td>
    </tr>
    <tr>
      <td colspan="6"><hr /></td>
    </tr>
    <tr>
      <td colspan="6">
        <div style="float: left;">
          {if $createParcelAllowed}
            <form action="{$navigation_script}" method="post" name="{$controller}form5">
              <input type="hidden" name="action" value="{XCPitneyBowesDefs::PARCEL_ACTION_CREATE_PARCEL}"/>
              <input type="submit" value="{$lng.lbl_pitney_bowes_create_parcel}" />
            </form>
          {/if}
        </div>
        <div style="float: right;">
          <a href="order.php?orderid={$orderid}">{$lng.lbl_pitney_bowes_back_to_order_details}</a>
        </div>
      </td>
    </tr>

  </table>

{/capture}
{include file="dialog.tpl" title=$lng.lbl_pitney_bowes_parcels content=$smarty.capture.dialog extra='width="100%"'}
