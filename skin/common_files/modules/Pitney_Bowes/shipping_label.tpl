{*
626362abdced0086c1c116748026db88b0984243, v1 (xcart_4_7_4), 2015-10-01 14:20:13, shipping_label.tpl, mixon

vim: set ts=2 sw=2 sts=2 et:
*}
<html>
  <body>
    {load_defer file="modules/Pitney_Bowes/shipping_label.css" type="css"}
    <div class="shipping-label">
      <p>To: {$recipient}</p>
      <p class="address">{$hub_address}</p>
      <p class="order">{$lng.lbl_pitney_bowes_order_number}: <span class="order-number">{$order_number}</span></p>
      <div class="barcode">
        <img src="data:image/png;base64,{$barcode_img}">
        <p class="text">{$barcode_text}</p>
      </div>
    </div>
  </body>
</html>
