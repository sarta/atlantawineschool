{*
0ff443df92716337eeaf9ce093e389f1850c39e7, v2 (xcart_4_7_4), 2015-10-20 22:58:09, order_tracking.tpl, mixon

vim: set ts=2 sw=2 sts=2 et:
*}
{if $order.{XCPitneyBowesDefs::ORDER_PARCELS}}
{if $nohtml eq 'Y'}
{$order.shipping|trademark}:
{foreach from=$order.{XCPitneyBowesDefs::ORDER_PARCELS} item=parcel name=parcels}
  {$lng.lbl_tracking_number}: {$parcel.asn_number} ({pb_track_url tracking_number=$parcel.asn_number})
{/foreach}
{else}
    <br />
    <br />

    {include file="customer/subheader.tpl" title=$order.shipping|trademark}

    <ul {if $is_nomail eq 'Y'}class="pb-tracking-numbers"{else}style="list-style: none;"{/if}>
      {foreach from=$order.{XCPitneyBowesDefs::ORDER_PARCELS} item=parcel name=parcels}
        <li>
          {$lng.lbl_tracking_number}: <a href="{pb_track_url tracking_number=$parcel.asn_number}" target="_blank" title="{$lng.lbl_pitney_bowes_asn_tracking}">{$parcel.asn_number}</a>
        </li>
      {/foreach}
    </ul>
{/if}
{/if}
