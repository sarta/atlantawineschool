{*
67814c9aec19d6cf932988e3b949b16094b3dab9, v1 (xcart_4_7_4), 2015-10-28 10:53:25, mod_PB.tpl, aim

vim: set ts=2 sw=2 sts=2 et:
*}
{if $carrier eq "PB"}
{*************************************************************
 *  Pitney Bowes OPTIONS                                     *
 *************************************************************}

  <script type="text/javascript" src="{$SkinDir}/js/two_select_boxes.js"></script>

  {capture name=dialog}

    <div align="right"><a href="shipping.php?carrier=PB#rt">{$lng.lbl_X_shipping_methods|substitute:"service":"Pitney Bowes"}</a></div>

    <form method="post" action="shipping_options.php" onsubmit="saveSelects(new Array('free_transportation_countries','free_importation_countries'))">
      <input type="hidden" name="carrier" value="PB" />

      <table class="pb-shipping-configuration">

        {include file="admin/main/shipping_package_limits.tpl" shipper_options=$shipping_options.pb shipper="Pitney Bowes"}

        <tr>
          <td colspan="2"><hr /></td>
        </tr>
        <tr>
          <td><b>{$lng.lbl_pitney_bowes_currency}:</b></td>
          <td>
            <select name="currency">
              {foreach key=code item=currency from=$pb_currencies}
                <option value="{$code}"{if $code eq $shipping_options.pb.currency}selected="selected"{/if}>{$code} ({$currency})</option>
              {/foreach}
            </select>
          </td>
        </tr>
        <tr>
          <td><b>{$lng.lbl_shipping_cost_convertion_rate}:</b><br />
            <span class="SmallText">{$lng.txt_shipping_cost_convertion_rate_currency}</span>
          </td>
          <td><input type="text" name="currency_rate" size="15" value="{$shipping_options.pb.currency_rate|escape}" /></td>
        </tr>

      </table>

      <table class="pb-shipping-configuration">
        <tr>
          <td colspan="2"><hr /></td>
        </tr>
        <tr>
          <td><b>{$lng.opt_free_transportation}</b>:</td>
          <td>
            <input type="checkbox" name="free_transportation" value="{XCPitneyBowesDefs::YES}" {if $shipping_options.pb.free_transportation eq XCPitneyBowesDefs::YES}checked="checked"{/if}/>
          </td>
        </tr>
        <tr>
          <td><b>{$lng.opt_free_transportation_threshold}</b>:</td>
          <td>
            <input type="text" size="15" name="free_transportation_threshold" value="{$shipping_options.pb.free_transportation_threshold}"/>
            {include file="main/tooltip_js.tpl" id='free_transportation_threshold' type='img' sticky='Y' text=$lng.opt_descr_free_transportation_threshold}
          </td>
        </tr>
        <tr>
          <td><b>{$lng.opt_free_transportation_countries}</b>:</td>
          <td class="pb-countries-multiselect">
            <input type="hidden" id="free_transportation_countries_store" name="free_transportation_countries_store"/>
            <table>
              <tr>
                <td>
                  <b>{$lng.lbl_unset_val}</b>
                </td>
                <td>&nbsp;</td>
                <td>
                  <b>{$lng.lbl_set_val}</b>
                  {include file="main/tooltip_js.tpl" id='free_transportation_countries_tip' type='img' sticky='Y' text=$lng.opt_descr_free_transportation_countries}
                </td>
              </tr>
              <tr>
                <td>
                  <select multiple="multiple" size="10" id="ftu_countries">
                    {foreach key=code item=country from=$pb_countries}
                      {if $shipping_options.pb.free_transportation_countries.$code ne XCPitneyBowesDefs::YES}
                      {assign var="country_name" value="country_`$code`"}
                      <option value="{$code}">{$lng.$country_name}</option>
                      {/if}
                    {/foreach}
                  </select>
                </td>
                <td align="center">
                  <input type="button" value="&lt;&lt;" onclick="javascript: moveSelect(document.getElementById('ftu_countries'), document.getElementById('free_transportation_countries'), 'R');" />
                  <br /><br />
                  <input type="button" value="&gt;&gt;" onclick="javascript: moveSelect(document.getElementById('ftu_countries'), document.getElementById('free_transportation_countries'), 'L');" />
                </td>
                <td>
                  <select multiple="multiple" size="10" id="free_transportation_countries">
                    {foreach key=code item=country from=$pb_countries}
                      {if $shipping_options.pb.free_transportation_countries.$code eq XCPitneyBowesDefs::YES}
                      {assign var="country_name" value="country_`$code`"}
                      <option value="{$code}">{$lng.$country_name}</option>
                      {/if}
                    {/foreach}
                  </select>
                </td>
              </tr>
            </table>
          </td>
        </tr>

        <tr>
          <td colspan="2"><hr /></td>
        </tr>
        <tr>
          <td><b>{$lng.opt_free_importation}</b>:</td>
          <td>
            <input type="checkbox" name="free_importation" value="{XCPitneyBowesDefs::YES}" {if $shipping_options.pb.free_importation eq XCPitneyBowesDefs::YES}checked="checked"{/if}/>
          </td>
        </tr>
        <tr>
          <td><b>{$lng.opt_free_importation_threshold}</b>:</td>
          <td>
            <input type="text" size="15" name="free_importation_threshold" value="{$shipping_options.pb.free_importation_threshold}"/>
            {include file="main/tooltip_js.tpl" id='free_importation_threshold' type='img' sticky='Y' text=$lng.opt_descr_free_importation_threshold}
          </td>
        </tr>
        <tr>
          <td><b>{$lng.opt_free_importation_countries}</b>:</td>
          <td class="pb-countries-multiselect">
            <input type="hidden" id="free_importation_countries_store" name="free_importation_countries_store"/>
            <table>
              <tr>
                <td>
                  <b>{$lng.lbl_unset_val}</b>
                </td>
                <td>&nbsp;</td>
                <td>
                  <b>{$lng.lbl_set_val}</b>
                  {include file="main/tooltip_js.tpl" id='free_importation_countries_tip' type='img' sticky='Y' text=$lng.opt_descr_free_importation_countries}
                </td>
              </tr>
              <tr>
                <td>
                  <select multiple="multiple" size="10" id="fiu_countries">
                    {foreach key=code item=country from=$pb_countries}
                      {if $shipping_options.pb.free_importation_countries.$code ne XCPitneyBowesDefs::YES}
                      {assign var="country_name" value="country_`$code`"}
                      <option value="{$code}">{$lng.$country_name}</option>
                      {/if}
                    {/foreach}
                  </select>
                </td>
                <td align="center">
                  <input type="button" value="&lt;&lt;" onclick="javascript: moveSelect(document.getElementById('fiu_countries'), document.getElementById('free_importation_countries'), 'R');" />
                  <br /><br />
                  <input type="button" value="&gt;&gt;" onclick="javascript: moveSelect(document.getElementById('fiu_countries'), document.getElementById('free_importation_countries'), 'L');" />
                </td>
                <td>
                  <select multiple="multiple" size="10" id="free_importation_countries">
                    {foreach key=code item=country from=$pb_countries}
                      {if $shipping_options.pb.free_importation_countries.$code eq XCPitneyBowesDefs::YES}
                      {assign var="country_name" value="country_`$code`"}
                      <option value="{$code}">{$lng.$country_name}</option>
                      {/if}
                    {/foreach}
                  </select>
                </td>
              </tr>
            </table>
          </td>
        </tr>

        <tr>
          <td colspan="2"><hr /></td>
        </tr>
        <tr>
          <td><b>{$lng.opt_shipping_fee_markup}</b>:</td>
          <td>
            <input type="text" size="15" name="shipping_fee_markup" value="{$shipping_options.pb.shipping_fee_markup}"/>
            {include file="main/tooltip_js.tpl" id='shipping_fee_markup' type='img' sticky='Y' text=$lng.opt_descr_shipping_fee_markup}
          </td>
        </tr>
        <tr>
          <td><b>{$lng.opt_shipping_fee_markup_basis}</b>:</td>
          <td>
            <select name="shipping_fee_markup_basis">
              <option value="{XCPitneyBowesDefs::SHIPPING_MARKUP_BASIS_ITEMS}"{if XCPitneyBowesDefs::SHIPPING_MARKUP_BASIS_ITEMS eq $shipping_options.pb.shipping_fee_markup_basis}selected="selected"{/if}>{$lng.lbl_pitney_bowes_per_item}</option>
              <option value="{XCPitneyBowesDefs::SHIPPING_MARKUP_BASIS_CART}"{if XCPitneyBowesDefs::SHIPPING_MARKUP_BASIS_CART eq $shipping_options.pb.shipping_fee_markup_basis}selected="selected"{/if}>{$lng.lbl_pitney_bowes_per_cart}</option>
            </select>
            {include file="main/tooltip_js.tpl" id='shipping_fee_markup_basis' type='img' sticky='Y' text=$lng.opt_descr_shipping_fee_markup_basis}
          </td>
        </tr>

        <tr>
          <td colspan="2"><hr /></td>
        </tr>
        <tr>
          <td><b>{$lng.opt_handling_fee_markup}</b>:</td>
          <td>
            <input type="text" size="15" name="handling_fee_markup" value="{$shipping_options.pb.handling_fee_markup}"/>
            {include file="main/tooltip_js.tpl" id='handling_fee_markup' type='img' sticky='Y' text=$lng.opt_descr_handling_fee_markup}
          </td>
        </tr>
        <tr>
          <td><b>{$lng.opt_handling_fee_markup_basis}</b>:</td>
          <td>
            <select name="handling_fee_markup_basis">
              <option value="{XCPitneyBowesDefs::SHIPPING_MARKUP_BASIS_ITEMS}"{if XCPitneyBowesDefs::SHIPPING_MARKUP_BASIS_ITEMS eq $shipping_options.pb.handling_fee_markup_basis}selected="selected"{/if}>{$lng.lbl_pitney_bowes_per_item}</option>
              <option value="{XCPitneyBowesDefs::SHIPPING_MARKUP_BASIS_CART}"{if XCPitneyBowesDefs::SHIPPING_MARKUP_BASIS_CART eq $shipping_options.pb.handling_fee_markup_basis}selected="selected"{/if}>{$lng.lbl_pitney_bowes_per_cart}</option>
            </select>
            {include file="main/tooltip_js.tpl" id='handling_fee_markup_basis' type='img' sticky='Y' text=$lng.opt_descr_handling_fee_markup_basis}
          </td>
        </tr>

        <tr>
          <td colspan="2"><hr /></td>
        </tr>
        <tr>
          <td><b>{$lng.opt_min_delivery_adjustment}</b>:</td>
          <td>
            <input type="text" size="15" name="min_delivery_adjustment" value="{$shipping_options.pb.min_delivery_adjustment}"/>
            {include file="main/tooltip_js.tpl" id='min_delivery_adjustment' type='img' sticky='Y' text=$lng.opt_descr_min_delivery_adjustment}
          </td>
        </tr>
        <tr>
          <td><b>{$lng.opt_max_delivery_adjustment}</b>:</td>
          <td>
            <input type="text" size="15" name="max_delivery_adjustment" value="{$shipping_options.pb.max_delivery_adjustment}"/>
            {include file="main/tooltip_js.tpl" id='max_delivery_adjustment' type='img' sticky='Y' text=$lng.opt_descr_max_delivery_adjustment}
          </td>
        </tr>

        <tr>
          <td colspan="2"><hr /></td>
        </tr>
        <tr>
          <td><b>{$lng.opt_suppress_leg_tracking}</b>:</td>
          <td>
            <input type="checkbox" name="suppress_leg_tracking" value="{XCPitneyBowesDefs::YES}" {if $shipping_options.pb.suppress_leg_tracking eq XCPitneyBowesDefs::YES}checked="checked"{/if}/>
            {include file="main/tooltip_js.tpl" id='suppress_leg_tracking' type='img' sticky='Y' text=$lng.opt_descr_suppress_leg_tracking}
          </td>
        </tr>
        <tr>
          <td></td>
          <td></td>
        </tr>

        <tr>
          <td colspan="2" class="SubmitBox"><input type="submit" value="{$lng.lbl_apply|strip_tags:false|escape}" /></td>
        </tr>

      </table>
    </form>
  {/capture}
  {assign var="section_title" value=$lng.lbl_X_shipping_options|substitute:"service":"Pitney Bowes"}
  {include file="dialog.tpl" content=$smarty.capture.dialog title=$section_title extra='width="100%"'}

{/if}
