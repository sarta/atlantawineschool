{*
0ff443df92716337eeaf9ce093e389f1850c39e7, v3 (xcart_4_7_4), 2015-10-20 22:58:09, shipping_details.tpl, mixon

vim: set ts=2 sw=2 sts=2 et:
*}
{if $order.{XCPitneyBowesDefs::CART_TRANSACTIONID}}
  {* Show details section only for Pitney Bowes*}
  {if $nohtml eq 'Y'}{$lng.lbl_pitney_bowes_transportation_part|mail_truncate} {currency value={pb_transportation_part subtotal=$order.subtotal country=$order.s_country cart=$order} plain_text_message="Y"}
  {$lng.lbl_pitney_bowes_importation_part|mail_truncate} {currency value={pb_importation_part subtotal=$order.subtotal country=$order.s_country cart=$order} plain_text_message="Y"}
  {else}
  <tr>
    <td {if $is_nomail eq 'Y'}class="invoice-total-name pb-invoice-name"{else}style="padding-right: 3px; height: 20px; width: 100%; text-align: right; font-size: smaller;"{/if}><strong>{$lng.lbl_pitney_bowes_transportation_part}:</strong></td>
    <td {if $is_nomail eq 'Y'}class="invoice-total-value pb-invoice-value"{else}style="white-space: nowrap; padding-right: 5px; height: 20px; text-align: right; font-size: smaller;"{/if}>{currency value={pb_transportation_part subtotal=$order.subtotal country=$order.s_country cart=$order}}</td>
  </tr>
  <tr>
    <td {if $is_nomail eq 'Y'}class="invoice-total-name pb-invoice-name"{else}style="padding-right: 3px; height: 20px; width: 100%; text-align: right; font-size: smaller;"{/if}><strong>{$lng.lbl_pitney_bowes_importation_part}:</strong></td>
    <td {if $is_nomail eq 'Y'}class="invoice-total-value pb-invoice-value"{else}style="white-space: nowrap; padding-right: 5px; height: 20px; text-align: right; font-size: smaller;"{/if}>{currency value={pb_importation_part subtotal=$order.subtotal country=$order.s_country cart=$order}}</td>
  </tr>
  {/if}
{/if}
