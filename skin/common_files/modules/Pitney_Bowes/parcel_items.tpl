{*
b82ff367d5669b4cc06c5831ea30868bb2f3cb1a, v2 (xcart_4_7_4), 2015-10-10 05:30:02, parcel_items.tpl, mixon

vim: set ts=2 sw=2 sts=2 et:
*}

{load_defer file="modules/Pitney_Bowes/controller.js" type="js"}

{capture name=dialog}

  <table width="100%">

    <tr>
      <td colspan="5"></td>
    </tr>
    <tr class="TableHead">
      <th></th>
      <th>{$lng.lbl_sku}</th>
      <th>{$lng.lbl_product_name}</th>
      <th>{$lng.lbl_quantity}</th>
      <th></th>
    </tr>
    {foreach from=$parcels.$parcelid.items item=pi name=items}
      <tr{cycle values=', class="TableSubHead"'}>
        <td></td>
        <td>{$pi.sku}</td>
        <td>{$pi.name}</td>
        <td align="center">
          {if $parcels.$parcelid.createAsnCalled eq XCPitneyBowesDefs::ASN_STATUS_PENDING and $pi.max gt 1}
            <form action="{$navigation_script}" method="post" name="{$controller}form1">
              <input type="hidden" name="action" value="{XCPitneyBowesDefs::PARCEL_ACTION_UPDATE_PARCEL_ITEM}"/>
              <input type="hidden" name="parcelid" value="{$parcelid}"/>
              <input type="hidden" name="itemid" value="{$pi.itemid}"/>
              <input type="number" name="quantity" max="{if $pi.max gt 0}{$pi.max}{else}{$pi.quantity}{/if}" value="{$pi.quantity}"/>
              <input type="submit" value="&nbsp;" title="{$lng.lbl_pitney_bowes_update_parcel_item}" class="pb-update-parcel-item-btn" />
            </form>
          {else}
            <span>{$pi.quantity}</span>
          {/if}
        </td>
        <td align="center">
          {if $parcels.$parcelid.createAsnCalled eq XCPitneyBowesDefs::ASN_STATUS_PENDING}
          <form action="{$navigation_script}" method="post" name="{$controller}form2">
            <input type="hidden" name="action" value="{XCPitneyBowesDefs::PARCEL_ACTION_DELETE_PARCEL_ITEM}"/>
            <input type="hidden" name="parcelid" value="{$parcelid}"/>
            <input type="hidden" name="itemid" value="{$pi.itemid}"/>
            <input type="submit" value="&nbsp;" title="{$lng.lbl_pitney_bowes_delete_parcel_item}" data-question="{$lng.msg_pitney_bowes_q_delete_parcel_item}" class="pb-delete-parcel-item-btn" />
          </form>
          {/if}
        </td>
      </tr>
    {foreachelse}
      <tr>
        <td colspan="5" align="center">{$lng.lbl_pitney_bowes_no_parcel_items}</td>
      </tr>
    {/foreach}
    <tr>
      <td colspan="5"></td>
    </tr>
    <tr>
      <td colspan="5"><hr /></td>
    </tr>
  </table>

  <form action="{$navigation_script}" method="post" name="{$controller}form3">
    <table width="100%">
      {if $parcels.$parcelid.createAsnCalled eq XCPitneyBowesDefs::ASN_STATUS_PENDING and $available_items}
        <tr>
          <td colspan="5"></td>
        </tr>
        <tr>
          <td class="SubHeader">{$lng.lbl_pitney_bowes_available_items}</td>
        </tr>
        <tr>
          <td colspan="5">
            {include file="main/check_all_row.tpl" form="{$controller}form3" prefix="itemsToAdd" style="line-height: 200%"}
          </td>
        </tr>
        <tr class="TableHead">
          <th></th>
          <th>{$lng.lbl_sku}</th>
          <th>{$lng.lbl_product_name}</th>
          <th>{$lng.lbl_quantity}</th>
          <th></th>
        </tr>
        {foreach from=$available_items item=ai name=available_items}
          <tr{cycle values=', class="TableSubHead"'}>
            <td><input type="checkbox" name="itemsToAdd[{$ai.itemid}][itemid]" value="{$ai.itemid}"></td>
            <td>{$ai.sku}</td>
            <td>{$ai.name}</td>
            <td><input type="number" name="itemsToAdd[{$ai.itemid}][quantity]" max="{$ai.max}" value="{$ai.max}"></td>
          </tr>
        {/foreach}
      {/if}
      <tr>
        <td colspan="5">
          <div style="float: left;">
            {if $available_items}
              <input type="hidden" name="action" value="{XCPitneyBowesDefs::PARCEL_ACTION_ADD_PARCEL_ITEMS}"/>
              <input type="hidden" name="parcelid" value="{$parcelid}"/>
              <input type="submit" value="{$lng.lbl_pitney_bowes_add_parcel_items}" />
            {/if}
          </div>
          <div style="float: right;">
            <a href="order.php?orderid={$orderid}&controller={XCPitneyBowesDefs::CONTROLLER_PARCELS}">{$lng.lbl_pitney_bowes_back_to_order_parcels}</a>
          </div>
        </td>
      </tr>

    </table>
  </form>

{/capture}
{include file="dialog.tpl" title=$lng.lbl_pitney_bowes_parcel_items content=$smarty.capture.dialog extra='width="100%"'}
