{*
f7d1aa9f262169407020bdc488c600bd8b92db43, v3 (xcart_4_7_5), 2015-11-25 16:22:36, product_restricted.tpl, aim

vim: set ts=2 sw=2 sts=2 et:
*}
<div class="pb-restricted">
  <span>{$lng.lbl_pitney_bowes_restricted}</span>
  {include file="main/tooltip_js.tpl" title=$lng.lbl_pitney_bowes_restricted text=$lng.txt_pitney_bowes_restricted_in_your_country|escape|replace:'{{RC}}':$lng.{"country_`$product.country`"} type="img" sticky=true id="`$product.productid`_`$product.add_date`_`$featured`"}
</div>
