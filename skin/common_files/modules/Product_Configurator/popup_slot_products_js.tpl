{*
a55134f3cc0c742f1c1ba4109a711c4feba898fd, v4 (xcart_4_7_4), 2015-09-02 18:57:54, popup_slot_products_js.tpl, aim 

vim: set ts=2 sw=2 sts=2 et:
*}
<script type="text/javascript">
//<![CDATA[
var lbl_pconf_default_product_not_defined = '{$lng.lbl_pconf_default_product_not_defined|wm_remove|escape:javascript}';
var lbl_pconf_save_msg = '{$lng.lbl_pconf_save_msg|wm_remove|escape:javascript}';
var catalogs_customer = '{$catalogs.customer|wm_remove|escape:javascript}';
//]]>
</script>
<script type="text/javascript" src="{$SkinDir}/modules/Product_Configurator/popup_slot_products.js"></script>

