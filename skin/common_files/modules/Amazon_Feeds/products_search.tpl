{*
09d46970de4b3605245381ed8ecd8b55fffd1d3e, v2 (xcart_4_7_5), 2016-02-12 18:28:36, products_search.tpl, aim

vim: set ts=2 sw=2 sts=2 et:
*}

<tr>
  <td height="10" width="20%" class="FormButton" nowrap="nowrap" valign="top">{$lng.lbl_amazon_feeds_export_status}:</td>
  <td>

    <select id="amazon_feeds_exported" name="posted_data[amazon_feeds_exported]">
      <option value=""{if $search_prefilled.amazon_feeds_exported eq ""} selected="selected"{/if}>{$lng.lbl_amazon_feeds_export_no_status}</option>
      <option value="exported"{if $search_prefilled.amazon_feeds_exported eq "exported"} selected="selected"{/if}>{$lng.lbl_amazon_feeds_export_exported}</option>
      <option value="has_warnings"{if $search_prefilled.amazon_feeds_exported eq "has_warnings"} selected="selected"{/if}>{$lng.lbl_amazon_feeds_export_warnings}</option>
      <option value="has_errors"{if $search_prefilled.amazon_feeds_exported eq "has_errors"} selected="selected"{/if}>{$lng.lbl_amazon_feeds_export_errors}</option>
    </select>

  </td>
</tr>
