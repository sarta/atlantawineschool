{*
09d46970de4b3605245381ed8ecd8b55fffd1d3e, v3 (xcart_4_7_5), 2016-02-12 18:28:36, product_type_selector.tpl, aim

vim: set ts=2 sw=2 sts=2 et:
*}

{include file="widgets/css_loader.tpl" css="lib/select2/css/select2.min.css"}
{load_defer file="lib/select2/js/select2.min.js" type="js"}
{load_defer file="lib/select2/js/i18n/$shop_language.js" type="js"}

{include file="widgets/categoryselector/config.tpl" assign="categoryselectorconfig"}

{include file="widgets/css_loader.tpl" css="widgets/categoryselector/categoryselector.css"}
{load_defer file="widgets/categoryselector/categoryselector.js" type="js"}

<div class="afds-product-type">
  <select name="{$name}" class="categoryselector InputWidth" data-categoryselectorconfig="{$categoryselectorconfig|escape}">
    <option value=""{if $value eq ""} selected="selected"{/if} title="{$lng.lbl_amazon_feeds_no_type}">{$lng.lbl_amazon_feeds_no_type}</option>
    {foreach from=$amazon_feeds_catalog item=grp key=name}
      <optgroup label="{$name}">
        {foreach from=$grp item=pt key=ptid}
          <option value="{$ptid}" title="{$pt}"{if $value eq $ptid} selected="selected"{/if}>{$pt}</option>
        {/foreach}
      </optgroup>
    {/foreach}
  </select>
  <div class="error-label">{$lng.err_amazon_feeds_type_required|escape}</div>
</div>
