{*
194626627da00f55901e393daa481ee1a5d33cfb, v4 (xcart_4_7_4), 2015-10-26 13:19:01, saved_cards.tpl, aim

vim: set ts=2 sw=2 sts=2 et:
*}

<h1>{$lng.lbl_saved_cards}</h1>

{$lng.lbl_saved_cards_top_note}

<br /><br />

{if $saved_cards}

  {include file="modules/XPayments_Connector/card_list_customer.tpl"}

{else}

  {$lng.lbl_no_saved_cards}

  {if $allow_add_new_card}
    <br /><br />
    {include file="customer/buttons/button.tpl" button_title=$lng.lbl_saved_card_add_new additional_button_class="xpc-add-new-card-button" href="javascript: showXPCFrame(this);"}
    {include file="modules/XPayments_Connector/saved_cards_add_new.tpl"}
  {/if}

{/if}

<br /><br />
