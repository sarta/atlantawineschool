{foreach from=$multilevel_menu item=menu key=menuid name="footer_menus"}
    <ul {interline name=footer_menus additional_class="footer-menu"}>
        <li class="title"><a href="{$menu.url|default:"#"}">{$menu.link}</a>
            {if $menu.childs}
                <ul>
                    {foreach from=$menu.childs item=l key=linkid name="menu_links"}
                        <li><a href="{$l.url|default:"#"}">{$l.link}</a></li>
                    {/foreach}
                </ul>
            {/if}
        </li>
    </ul>
{/foreach}

