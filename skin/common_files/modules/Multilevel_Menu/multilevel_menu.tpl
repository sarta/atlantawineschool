{if $multilevel_menu}

<div id="multilevelmenu" class="multilevel-menu">
  {include file="modules/Multilevel_Menu/multilevel_menu_links.tpl" level=0 links=$multilevel_menu}
</div>

{/if}