{if $mode eq 'simple'}
    <ul>

        {assign var="loop_name" value="mm_`$parentid`"}
        {foreach from=$links item=l key=linkid name=$loop_name}

            <li {interline name=$loop_name}>
                <a href="{$l.url|default:"#"}">{$l.link}</a>
                {if $l.childs}
                    <span class="toggle-button"></span>
                {/if}
                {if $l.childs}
                    {include file="modules/Multilevel_Menu/multilevel_menu_links.tpl" links=$l.childs parentid=$linkid level=$level+1 mode='simple'}
                {/if}
            </li>

        {/foreach}

    </ul>

    {else}

    <ul class="multilevel-menu-level-{$level}">

        {assign var="loop_name" value="mm_`$parentid`"}
        {foreach from=$links item=l key=linkid name=$loop_name}

            <li {interline name=$loop_name}>
                <a {if $l.childs}class="has-sublevel"{/if} href="{$l.url|default:"#"}">{$l.link}</a>
                {if $l.childs}
                    {include file="modules/Multilevel_Menu/multilevel_menu_links.tpl" links=$l.childs parentid=$linkid level=$level+1}
                {/if}
            </li>

        {/foreach}

    </ul>

{/if}