{*
a74fe6885509c651f2ee37e8b41267a193293cc7, v1 (xcart_4_7_0), 2015-02-27 17:35:59, head.tpl, aim

vim: set ts=2 sw=2 sts=2 et:
*}
<div class="wrapper-box">
<div class="inner-container">
  <div class="logo">
    <a href="//www.vinovenue.com/"><img src="{$AltImagesDir}/redesign/logo.png" alt="{$config.Company.company_name}" /></a>
  </div>

  <div id="multilevelmenu" class="multilevel-menu">

    {if $active_modules.Multilevel_Menu && $multilevel_menu ne ""}
      {include file="modules/Multilevel_Menu/multilevel_menu_links.tpl" level=0 links=$multilevel_menu}
    {/if}

    <ul class="nav nav-pills">

      <li class="dropdown desktop-hidden">
        <a id="main-menu-toggle" class="dropdown-toggle" href="#">
          <span class="fa fa-bars"></span>
        </a>
        <div id="main-menu-box" class="dropdown-menu">

          {if $active_modules.Multilevel_Menu && $multilevel_menu ne ""}
            {include file="modules/Multilevel_Menu/multilevel_menu_links.tpl" level=0 links=$multilevel_menu mode='simple'}
          {/if}

        </div>
      </li>

      <li class="dropdown">
        <a id="account-toggle" class="dropdown-toggle" href="#">
          <span class="fa fa-user"></span>
        </a>
        <div id="account-box" class="dropdown-menu">

          {include file="customer/header_links.tpl" mode="plain_list"}

        </div>
      </li>

      <li>
        {include file="customer/menu_cart.tpl"}
      </li>

    </ul>

  </div></div>

  {include file="customer/noscript.tpl"}

</div><!--/wrapper-box-->




