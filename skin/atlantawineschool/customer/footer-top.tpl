
<div class="footer-top">
    <div class="footer-top-1 col-xs-12 col-sm-6 col-md-3 col-lg-3"><div id="text-44" class="footer-widget-col widget_text">			<div class="textwidget"><div class="footer_headings">WINE BAR &amp; RETAIL:</div>
                MONDAY THROUGH FRIDAY<br>
                <em>10:00am - CLOSE</em><br>
                SATURDAY<br>
                <em>12:00PM - CLOSE</em>
                <br></div>
            <div style="clear:both;"></div></div></div><div class="footer-top-2 col-xs-12 col-sm-6 col-md-3 col-lg-3"><div id="text-49" class="footer-widget-col widget_text">			<div class="textwidget"><div class="footer_headings">RESTAURANT:</div>
                MONDAY THROUGH FRIDAY<br>
                AFTERNOON BITES<br>
                <em>2:00PM - 5:00PM</em><br>
                DINNER NIGHTLY<br>
                <em>5:00pm - CLOSE</em><br>
                FULL MENU SATURDAY <br>
                <em>12:00pm – Close</em><br></div>
            <div style="clear:both;"></div></div></div><div class="footer-top-3 col-xs-12 col-sm-6 col-md-3 col-lg-3"><div id="text-45" class="footer-widget-col widget_text">			<div class="textwidget"><div class="footer_headings">CONTACT INFORMATION</div>
                4478 Chamblee Dunwoody Road<br> Dunwoody, GA 30338<br>
                Georgetown Shopping Center<br>
                Phone: <a href="tel:770-668-0435">770-668-0435</a><br>
                <a href="https://goo.gl/maps/WKl0T" target="_blank">View a map of our location</a><br></div>
            <div style="clear:both;"></div></div></div><div class="footer-top-4 col-xs-12 col-sm-6 col-md-3 col-lg-3"><div id="text-51" class="footer-widget-col widget_text">			<div class="textwidget"><div class="footer_headings">STAY INFORMED:</div>
                <div class="stay-informed">
                    Sign up for our complimentary newsletter.<form id="radCCSubscribe" action="https://www.radiantretailapps.com/CustomerConnect/OptIn.aspx" method="post" target="_blank"><div class="news-signup"> <input type="text" class="news-field" value="Enter Email" name="email" id="email" onfocus="this.value=''"> <button class="btn btn-default button-small" type="submit" value="Subscribe" name="submit"> Go</button><input id="a" type="hidden" name="a" value="hVbVTR3Ztku6IBFEYxZ5ww"></div></form></div>
            </div>
        </div>
        <div style="clear:both;"></div></div><div id="cs_social_widget-6" class="right_media footer-widget-col widget_cs_social_widget"><h3 class="wg-title"><span><span class="title-line">FOLLOW</span> US:</span></h3><ul class="cs-social"><li><a target="_blank" data-rel="tooltip" data-placement="bottom" data-original-title="Facebook" href="https://www.facebook.com/pages/Vino-Venue/220458168074358?fref=ts"><i class="fa fa-facebook"></i></a></li><li><a target="_blank" data-rel="tooltip" data-placement="bottom" data-original-title="Twitter" href="https://twitter.com/VinoVenueAtl?lang=en"><i class="fa fa-twitter"></i></a></li><li><a target="_blank" data-rel="tooltip" data-placement="bottom" data-original-title="Instagram" href="https://instagram.com/vinovenue/"><i class="fa fa-instagram"></i></a></li></ul><div style="clear:both;"></div></div></div>
