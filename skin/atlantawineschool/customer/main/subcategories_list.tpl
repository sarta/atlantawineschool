{*
16fd72c950254a8d4861815dac0d6147af93e43f, v3 (xcart_4_7_0), 2014-12-23 10:13:26, subcategories_list.tpl, aim

vim: set ts=2 sw=2 sts=2 et:
*}

      <div class="cats">

        {foreach from=$categories item=subcat name=subcategories}
          <div{interline name=subcategories foreach_iteration="`$smarty.foreach.subcategories.iteration`" foreach_total="`$smarty.foreach.subcategories.total`"}>

            <div class="cat-image">
              {if $subcat.image_path}
                <a href="home.php?cat={$subcat.categoryid}" title="{$subcat.category|escape}">
                  <img class="subcat-img" src="{$subcat.image_path}" alt="{$subcat.category|escape}"{if $subcat.image_x} width="{$subcat.image_x}"{/if}{if $subcat.image_y} height="{$subcat.image_y}"{/if} />
                </a>
              {/if}
            </div>

            <div class="cat-info">
              <a href="home.php?cat={$subcat.categoryid}" title="{$subcat.category|escape}" class="cat-title">
                {$subcat.category|amp}
                {if $config.Appearance.count_products eq "Y"}
                  {if $subcat.product_count}
                    ({$subcat.product_count} {$lng.lbl_products})
                  {elseif $subcat.subcategory_count}
                    ({$lng.lbl_N_categories|substitute:count:$subcat.subcategory_count})
                  {/if}
                {/if}
              </a>

              {if $subcat.description}
                {$subcat.description}
              {/if}
              <div class="clearfix"></div>

              <a href="home.php?cat={$subcat.categoryid}" title="{$subcat.category|escape}" class="cat-button">
                {$lng.lbl_learn_more}
              </a>
            </div>

          </div>
        {/foreach}

      </div>
