{*
a74fe6885509c651f2ee37e8b41267a193293cc7, v1 (xcart_4_7_0), 2015-02-27 17:35:59, welcome.tpl, aim

vim: set ts=2 sw=2 sts=2 et:
*}

{include file="customer/main/home_page_banner.tpl"}

{if $lng.txt_welcome}
    <div class="welcome">
        {$lng.txt_welcome}
    </div>
{/if}

{if $categories_menu_list ne '' or $fancy_use_cache}

    <div class="cats">
        {foreach from=$categories_menu_list item=c name=categories}
            <div{interline name=categories foreach_iteration="`$smarty.foreach.categories.iteration`" foreach_total="`$smarty.foreach.categories.total`"}>
                <div class="cat-image">
                    {if $c.image_path}
                        <a href="home.php?cat={$c.categoryid}" title="{$c.category|escape}">
                            <img src="{$c.image_path}" alt="{$c.category|escape}"{if $c.image_x} width="{$c.image_x}"{/if}{if $c.image_y} height="{$c.image_y}"{/if} />
                        </a>
                    {/if}
                </div>
                <div class="cat-info">
                    <a href="home.php?cat={$c.categoryid}" title="{$c.category|escape}" class="cat-title">
                        {$c.category|amp}
                    </a>

                    {if $c.description}
                        {$c.description}
                    {/if}
                    <div class="clearfix"></div>

                    <a href="home.php?cat={$c.categoryid}" title="{$c.category|escape}" class="cat-button">
                        {$lng.lbl_learn_more}
                    </a>
                </div>
            </div>
        {/foreach}
        {$lng.txt_gift_cert_additional_category}
{/if}


{if $active_modules.Bestsellers and $config.Bestsellers.bestsellers_menu ne "Y"}
    {include file="modules/Bestsellers/bestsellers.tpl"}
{/if}

{if $active_modules.New_Arrivals}
    {include file="modules/New_Arrivals/new_arrivals.tpl" is_home_page="Y"}
{/if}

{if $active_modules.Refine_Filters}
    {include file="modules/Refine_Filters/home_products.tpl"}
{/if}

{if $active_modules.On_Sale}
    {include file="modules/On_Sale/on_sale.tpl" is_home_page="Y"}
{/if}

{include file="customer/main/featured.tpl"}

