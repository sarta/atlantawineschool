{*
a74fe6885509c651f2ee37e8b41267a193293cc7, v1 (xcart_4_7_0), 2015-02-27 17:35:59, bottom.tpl, aim

vim: set ts=2 sw=2 sts=2 et:
*}
<div class="box">

  <div class="wrapper-box">

    {include file="customer/footer-top.tpl"}

  </div><!--/wrapper-box-->

</div>

<div class="box">
  <div class="wrapper-box">
    <div class="copyright">
      {$lng.lbl_footer_copyright}
    </div>

    {if $active_modules.Multilevel_Menu && $multilevel_menu ne ""}
      {include file="modules/Multilevel_Menu/multilevel_menu_links.tpl" level=0 links=$multilevel_menu}
    {/if}
  </div>
</div>
