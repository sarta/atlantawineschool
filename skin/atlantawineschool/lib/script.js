/**
 * Created by sarta on 20.04.16.
 */



function stickyNav (){

    //init
    var pageWidth = $('#center').outerWidth();
    var windowWidth = $(window).width();
    var outer_container = $('#header > .wrapper-box');
    var inner_container = outer_container.find('.inner-container');
    var scroll_top = window.pageYOffset;
    var breakPoint = $('#header').outerHeight(true);
    var leftOffset = (windowWidth - pageWidth)/2;


    var makeFixed = function () {
        outer_container.addClass('fixed');
        inner_container.addClass('fixed').width(pageWidth).css('left', leftOffset);
    };

    var makeUnfixed = function () {
        outer_container.removeClass('fixed');
        inner_container.removeClass('fixed').width(pageWidth);

    };

    var shouldBeFixed = function () {
        return scroll_top >= breakPoint;
    };


    if (shouldBeFixed()) {
        makeFixed();
    } else if (!shouldBeFixed()) {
        makeUnfixed();
    }



}

$(window).on('load',stickyNav);
$(window).on('scroll',stickyNav);
$(window).on('resize',stickyNav);
$(window).on('touchmove',stickyNav);

jqmenu = new Object();

jqmenu.items = new Array();
jqmenu.items_selector = '#main-menu-box ul li';
jqmenu.button_css_class = 'toggle-button';
jqmenu.item_state1_css_class = "expanded";
jqmenu.item_state2_css_class = "collapsed";
jqmenu.duration = 0;
jqmenu.collapsed_opacity = 1;

jqmenu.itemCollapse = function(el, duration){
    if (!duration)
        duration = jqmenu.duration;
    if (el.hasClass(jqmenu.item_state1_css_class))
        el.removeClass(jqmenu.item_state1_css_class);
    el.addClass(jqmenu.item_state2_css_class)
        .find('> ul')
        .animate({"height": "toggle", "opacity": "toggle"}, { duration: duration })
        .end()
        .find('> span.'+jqmenu.button_css_class)
        .end()
        .animate({"opacity": jqmenu.collapsed_opacity}, { duration: duration });
}
jqmenu.itemExpand = function(el, duration){
    if (!duration)
        duration = jqmenu.duration;
    el.removeClass(jqmenu.item_state2_css_class)
        .find('> ul')
        .animate({"height": "toggle", "opacity": "toggle"}, { duration: duration })
        .end()
        .find('> span.'+jqmenu.button_css_class)
        .end()
        .animate({"opacity": "1"}, { duration: duration });
    el.addClass(jqmenu.item_state1_css_class);
}

function pageTitle(){
    var title = $('#center').find('> h1');
    var title_text = title.text();

    $('.main-title > h1').text(title_text);
    title.remove();
}


$(document).ready(
    function() {
        pageTitle();
        jqmenu.items = $(jqmenu.items_selector);

        jqmenu.items.each(function() {

            $(this).addClass('collapsed');

            $(this)
                .find('> span.'+jqmenu.button_css_class)
                .click(function() {
                    if ($(this).parent().hasClass(jqmenu.item_state2_css_class)) {
                        jqmenu.itemExpand($(this).parent());
                    } else {
                        jqmenu.itemCollapse($(this).parent());
                    }
                });
        });
    }
);


