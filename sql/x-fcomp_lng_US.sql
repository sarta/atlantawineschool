INSERT INTO xcart_languages VALUES ('en','lbl_X_features','{{product_type}} features','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_add_comparison_list','Add to comparison list','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_add_feature_class','Add product class','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_add_new_option','Add new option','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_add_option_variant','Add variant','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_assign_feature_class','Assign product class','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_check_to_compare','Check to compare','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_choosing_products_features','Feature-based search','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_class_options_are_deleted','Class options are deleted.','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_clear_list','Clear list','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_compare_selected','Compare selected','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_comparison_list','Comparison list','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_comparison_list_empty','Comparison list is empty','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_delete_selected_options','Delete selected options','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_delete_selected_variants','Delete selected variants','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_fcomp_add_to','To comparison list','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_fcomp_all_selected_options','all selected options','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_fcomp_any_selected_option','any selected options','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_fcomp_compare_product_with','Compare product with:','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_fcomp_search_for_products','Search for products matching:','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_feature_class','Product class','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_feature_class_is_added','Product class is added.','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_feature_class_is_updated','Product class is updated.','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_feature_class_not_found','Product classes are not defined','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_feature_class_options','Product class options','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_feature_classes_are_deleted','Product classes are deleted.','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_feature_classes_are_updated','Product classes are updated.','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_feature_image_is_deleted','Product class image is deleted.','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_feature_option_type_B','Boolean field','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_feature_option_type_D','Date field','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_feature_option_type_M','Multiple option selector','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_feature_option_type_N','Numeric field','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_feature_option_type_S','Single option selector','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_feature_option_type_T','Text field','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_ignored','ANY','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_image_title_F','Product class images','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_input_box','Input box','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_modify_feature_class','Modify product class','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_multilanguage_product_class_options_checking_','Checking product class options\' multilanguage data...','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_multilanguage_product_class_options_exporting_','Exporting product class options\' multilanguage data...','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_multilanguage_product_class_options_importing_','Importing product class options\' multilanguage data...','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_multilanguage_product_classes_checking_','Checking product classes\' multilanguage data...','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_multilanguage_product_classes_exporting_','Exporting product classes\' multilanguage data...','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_multilanguage_product_classes_importing_','Importing product classes\' multilanguage data...','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_no_features_selected','No features are selected','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_open_in_popup_window','Open in popup window','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_option_hint','Tooltip','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_option_type_format','Option type / Format','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_option_variants','Variants setup','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_option_variants_are_deleted','Option variants are deleted.','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_options_not_found','Options are not defined','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_product_class_options_checking_','Checking product class options...','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_product_class_options_exporting_','Exporting product class options...','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_product_class_options_importing_','Importing product class options...','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_product_classes_checking_','Checking product classes...','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_product_classes_exporting_','Exporting product classes...','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_product_classes_importing_','Importing product classes...','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_product_comparison','Product comparison','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_product_feature','Product features','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_product_feature_classes','Product classes','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_product_feature_values_checking_','Checking product feature values...','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_product_feature_values_exporting_','Exporting product feature values...','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_product_feature_values_importing_','Importing product feature values...','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_product_features_chart','Product features chart','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_product_type_list','Product classes list','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_product_types_not_defined','Product classes are not defined','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_remove_selected_features','Remove selected features','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_remove_selected_products','Remove selected products','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_removed_features','Removed features','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_reverse_axis','Reverse axis','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_select_features','Select features','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_show_all_features','Show all features','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_show_differ_feature','Show only the features that differ','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_show_in_search','Show in search','Labels');
INSERT INTO xcart_languages VALUES ('en','lbl_what_are_you_looking_for','What are you looking for?','Labels');
INSERT INTO xcart_languages VALUES ('en','module_descr_Feature_Comparison','This module allows customers to compare products by specific features.','Modules');
INSERT INTO xcart_languages VALUES ('en','module_name_Feature_Comparison','Feature Comparison','Modules');
INSERT INTO xcart_languages VALUES ('en','msg_err_import_log_message_26','Unknown product feature class','Labels');
INSERT INTO xcart_languages VALUES ('en','msg_err_import_log_message_27','Unknown product feature class option','Labels');
INSERT INTO xcart_languages VALUES ('en','opt_fcomparison_comp_product_limit','Maximum number of products which can be compared','Options');
INSERT INTO xcart_languages VALUES ('en','opt_fcomparison_disp_product_limit','Maximum number of products which can be shown on the feature-based comparisons page and on the Product details page in the form of a select box. If the quantity of products for comparison exceeds the number specified here, product selection will be done in a popup window.','Options');
INSERT INTO xcart_languages VALUES ('en','opt_fcomparison_max_product_list','The maximum number of products in the comparison list in the menu column','Options');
INSERT INTO xcart_languages VALUES ('en','opt_fcomparison_show_product_list','Enable adding products to comparison lists for further comparing','Options');
INSERT INTO xcart_languages VALUES ('en','opt_fcomparison_show_search','Enable feature-based search','Options');
INSERT INTO xcart_languages VALUES ('en','opt_feature_image_width','The product class image width in the product classes list','Options');
INSERT INTO xcart_languages VALUES ('en','option_title_Feature_Comparison','Feature Comparison','Options');
INSERT INTO xcart_languages VALUES ('en','txt_choosing_by_features_note_step1','On this page you can select the type of products you would like to find.','Text');
INSERT INTO xcart_languages VALUES ('en','txt_choosing_by_features_note_step2','On this page you can provide information about the features by which you would like the feature-based search to be conducted.','Text');
INSERT INTO xcart_languages VALUES ('en','txt_fc_differ_classes','The products selected for comparison are of different classes and cannot be compared. Please select the product class within which you would like to compare products.','Text');
INSERT INTO xcart_languages VALUES ('en','txt_fc_empty_comparison_list','You have not selected any products. Please select one or more products for comparison.','Text');
INSERT INTO xcart_languages VALUES ('en','txt_max_number_of_products_exceeded','The maximum number of products which can be compared is exceeded! Some of the selected products were not added to the comparison list! If you want to compare other products, please remove some products from the comparison list and add new ones.','Text');
INSERT INTO xcart_languages VALUES ('en','txt_max_number_of_products_exceeded_in_chart','The maximum number of products which can be compared is exceeded! Some of the selected products were not added to the product features chart! If you want to compare other products, please remove some products from the chart and add new ones.','Text');
INSERT INTO xcart_languages VALUES ('en','txt_multilanguage_product_class_options_import_result','Product class options languages import details:<br />&nbsp;&nbsp;&nbsp;- added: {{added}}<br />&nbsp;&nbsp;&nbsp;- updated: {{updated}}','Text');
INSERT INTO xcart_languages VALUES ('en','txt_multilanguage_product_classes_import_result','Multilanguage product classes import details:<br />&nbsp;&nbsp;&nbsp;- added: {{added}}<br />&nbsp;&nbsp;&nbsp;- updated: {{updated}}','Text');
INSERT INTO xcart_languages VALUES ('en','txt_not_allowed_edit_product_class','You are not allowed to edit this product class','Text');
INSERT INTO xcart_languages VALUES ('en','txt_not_necessary_fill_all_fields','It is not necessary to fill out all the fields. You can specify values only for the features relevant to you.','Text');
INSERT INTO xcart_languages VALUES ('en','txt_product_class_options_import_result','Product class options import details:<br />&nbsp;&nbsp;&nbsp;- added: {{added}}<br />&nbsp;&nbsp;&nbsp;- updated: {{updated}}','Text');
INSERT INTO xcart_languages VALUES ('en','txt_product_classes_import_result','Product classes import details:<br />&nbsp;&nbsp;&nbsp;- added: {{added}}<br />&nbsp;&nbsp;&nbsp;- updated: {{updated}}','Text');
INSERT INTO xcart_languages VALUES ('en','txt_product_classes_note','This page allows you to manage product classes. Here you can create, modify and delete product classes and their features.','Text');
INSERT INTO xcart_languages VALUES ('en','txt_product_feature_values_import_result','Product feature values import details:<br />&nbsp;&nbsp;&nbsp;- added: {{added}}<br />&nbsp;&nbsp;&nbsp;- updated: {{updated}}','Text');
