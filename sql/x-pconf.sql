







CREATE TABLE xcart_pconf_class_requirements (
  classid int(11) NOT NULL DEFAULT '0',
  ptypeid int(11) NOT NULL DEFAULT '0',
  specid int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (classid,ptypeid,specid)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;










CREATE TABLE xcart_pconf_class_specifications (
  classid int(11) NOT NULL DEFAULT '0',
  specid int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (classid,specid)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;










CREATE TABLE xcart_pconf_products_classes (
  classid int(11) NOT NULL AUTO_INCREMENT,
  productid int(11) NOT NULL DEFAULT '0',
  ptypeid int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (classid),
  UNIQUE KEY product_type (productid,ptypeid)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;










CREATE TABLE xcart_pconf_product_types (
  ptypeid int(11) NOT NULL AUTO_INCREMENT,
  provider int(11) NOT NULL DEFAULT '0',
  ptype_name varchar(255) NOT NULL DEFAULT '',
  orderby int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (ptypeid),
  UNIQUE KEY provider (provider,ptype_name)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;










CREATE TABLE xcart_pconf_slot_markups (
  markupid int(11) NOT NULL AUTO_INCREMENT,
  slotid int(11) NOT NULL DEFAULT '0',
  markup decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'Use func_decimal_empty',
  markup_type char(1) NOT NULL DEFAULT '%',
  membershipid int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (markupid),
  UNIQUE KEY slotid (slotid,membershipid)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;










CREATE TABLE xcart_pconf_slot_rules (
  slotid int(11) NOT NULL DEFAULT '0',
  ptypeid int(11) NOT NULL DEFAULT '0',
  index_by_and int(11) NOT NULL DEFAULT '0',
  KEY slotid (slotid),
  KEY ptypeid (ptypeid),
  KEY index_by_and (index_by_and)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;










CREATE TABLE xcart_pconf_slots (
  slotid int(11) NOT NULL AUTO_INCREMENT,
  stepid int(11) NOT NULL DEFAULT '0',
  slot_name varchar(255) NOT NULL DEFAULT '',
  slot_descr text NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'O',
  multiple char(1) NOT NULL DEFAULT '',
  amount_min int(11) NOT NULL DEFAULT '1',
  amount_max int(11) NOT NULL DEFAULT '1',
  default_amount int(11) NOT NULL DEFAULT '1',
  default_productid int(11) NOT NULL DEFAULT '0',
  orderby int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (slotid),
  KEY product (stepid,orderby,slotid)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;










CREATE TABLE xcart_pconf_specifications (
  specid int(11) NOT NULL AUTO_INCREMENT,
  ptypeid int(11) NOT NULL DEFAULT '0',
  spec_name varchar(255) NOT NULL DEFAULT '',
  orderby int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (specid),
  UNIQUE KEY `name` (ptypeid,spec_name)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;










CREATE TABLE xcart_pconf_wizards (
  stepid int(11) NOT NULL AUTO_INCREMENT,
  productid int(11) NOT NULL DEFAULT '0',
  step_name varchar(255) NOT NULL DEFAULT '',
  step_descr text NOT NULL,
  orderby int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (stepid),
  KEY product (productid,orderby,stepid)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;








INSERT INTO xcart_modules VALUES (85,'Product_Configurator','This module allows you to create and sell composite products or systems that can be assembled from various components.','Y','0','qualiteam','','products,userexp');


