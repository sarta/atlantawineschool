







CREATE TABLE xcart_feature_classes (
  fclassid int(11) NOT NULL AUTO_INCREMENT,
  class varchar(128) NOT NULL DEFAULT '',
  avail char(1) NOT NULL DEFAULT 'Y',
  orderby int(11) NOT NULL DEFAULT '0',
  provider int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (fclassid),
  UNIQUE KEY fao (fclassid,avail,orderby)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;





CREATE TABLE xcart_feature_classes_lng (
  fclassid int(11) NOT NULL DEFAULT '0',
  `code` char(2) NOT NULL DEFAULT 'en',
  class varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (fclassid,`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;





CREATE TABLE xcart_feature_options (
  foptionid int(11) NOT NULL AUTO_INCREMENT,
  fclassid int(11) NOT NULL DEFAULT '0',
  option_name varchar(128) NOT NULL DEFAULT '',
  option_hint varchar(128) NOT NULL DEFAULT '',
  option_type char(1) NOT NULL DEFAULT '',
  `format` varchar(32) NOT NULL DEFAULT '',
  avail char(1) NOT NULL DEFAULT 'Y',
  show_in_search char(1) NOT NULL DEFAULT 'Y',
  orderby int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (foptionid),
  KEY cao (fclassid,avail,orderby)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;





CREATE TABLE xcart_feature_options_lng (
  foptionid int(11) NOT NULL DEFAULT '0',
  `code` char(2) NOT NULL DEFAULT 'en',
  option_name varchar(128) NOT NULL DEFAULT '',
  option_hint varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (foptionid,`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;





CREATE TABLE xcart_feature_variants (
  fvariantid int(11) NOT NULL AUTO_INCREMENT,
  foptionid int(11) NOT NULL DEFAULT '0',
  orderby int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (fvariantid),
  KEY vo (fvariantid,foptionid),
  KEY fo (foptionid,orderby)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;





CREATE TABLE xcart_feature_variants_lng_de (
  fvariantid int(11) NOT NULL DEFAULT '0',
  variant_name varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (fvariantid)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;





CREATE TABLE xcart_feature_variants_lng_en (
  fvariantid int(11) NOT NULL DEFAULT '0',
  variant_name varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (fvariantid)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;





CREATE TABLE xcart_feature_variants_lng_fr (
  fvariantid int(11) NOT NULL DEFAULT '0',
  variant_name varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (fvariantid)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;





CREATE TABLE xcart_feature_variants_lng_sv (
  fvariantid int(11) NOT NULL DEFAULT '0',
  variant_name varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (fvariantid)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;





CREATE TABLE xcart_images_F (
  imageid int(11) NOT NULL AUTO_INCREMENT,
  id int(11) NOT NULL DEFAULT '0',
  image mediumblob NOT NULL,
  image_path varchar(255) NOT NULL DEFAULT '',
  image_type varchar(64) NOT NULL DEFAULT 'image/jpeg',
  image_x int(11) NOT NULL DEFAULT '0',
  image_y int(11) NOT NULL DEFAULT '0',
  image_size int(11) NOT NULL DEFAULT '0',
  filename varchar(255) NOT NULL DEFAULT '',
  `date` int(11) NOT NULL DEFAULT '0',
  alt varchar(255) NOT NULL DEFAULT '',
  avail char(1) NOT NULL DEFAULT 'Y',
  orderby int(11) NOT NULL DEFAULT '0',
  md5 char(32) NOT NULL DEFAULT '',
  PRIMARY KEY (imageid),
  UNIQUE KEY id (id),
  KEY image_path (image_path),
  KEY func_image_filename_is_unique_n_func_get_next_unique_id (filename(64))
) ENGINE=MyISAM DEFAULT CHARSET=utf8;





CREATE TABLE xcart_product_features (
  productid int(11) NOT NULL DEFAULT '0',
  fclassid int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (productid,fclassid)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;





CREATE TABLE xcart_product_foptions (
  foptionid int(11) NOT NULL DEFAULT '0',
  productid int(11) NOT NULL DEFAULT '0',
  `value` text NOT NULL,
  PRIMARY KEY (foptionid,productid)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



REPLACE INTO xcart_config VALUES ('fcomparison_show_search','Enable feature-based search','Y','Feature_Comparison',2305,'checkbox','Y','','','');
REPLACE INTO xcart_config VALUES ('feature_image_width','The product class image width in the product classes list','100','Feature_Comparison',160,'numeric','100','','uintz','');
REPLACE INTO xcart_config VALUES ('fcomparison_comp_product_limit','Maximum number of products which can be compared','20','Feature_Comparison',2330,'numeric','20','','uintz','');
REPLACE INTO xcart_config VALUES ('fcomparison_disp_product_limit','Maximum number of products which can be shown on the feature-based comparisons page and on the Product details page in the form of a select box. If the quantity of products for comparison exceeds the number specified here, product selection will be done i','20','Feature_Comparison',2320,'numeric','20','','uintz','');
REPLACE INTO xcart_config VALUES ('fcomparison_max_product_list','The maximum number of products in the comparison list in the menu column','10','Feature_Comparison',2310,'numeric','10','','uintz','');
REPLACE INTO xcart_config VALUES ('fcomparison_show_product_list','Enable adding products to comparison lists for further comparing','Y','Feature_Comparison',2300,'checkbox','N','','','');



INSERT INTO xcart_modules VALUES (82,'Feature_Comparison','This module allows customers to compare products by specific features.','Y','0','qualiteam','','products,userexp,marketing');



REPLACE INTO xcart_setup_images VALUES ('F','FS','',0,'','./default_image.gif');


